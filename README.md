# Noir d'encre #

*Nous étions poursuivis, mon frère et moi. Par ma faute, nous courions au cœur de la forêt pour* lui *échapper. Je devais le protéger, il n'avait que huit ans. Il s'agissait de mon devoir de grande sœur. Mais où aller, quand tout se ressemblait ?
Soudain, mon frère me montra quelque chose, derrière les arbres : « Regarde, Nevena, là-bas ! » Nous étions sauvés. Cette vieille bâtisse allait nous abriter jusqu'au matin. Jusqu'au matin… Vraiment ?*

## Jouer ##

*Noir d'encre* est une fiction interactive, qui a fini deuxième au [concours de fiction interactive 2013](http://ifiction.free.fr/index.php?id=concours&date=2013). Elle se joue en tapant des verbes à l'infinitif afin de guider l'héroïne et son frère.

Si vous voulez essayer, ce n'est pas ici qu'il faut regarder ! Vous pouvez jouer en ligne à la version du concours [à cet endroit](http://ifiction.free.fr/index.php?id=jeu&j=054), et une nouvelle version verra le jour prochainement (au moins).

## La Source ##

La source de ce jeu est visualisable dans ce dépôt (dans le fichier « NoirEncre.inform/Source/story.ni ») afin de satisfaire votre curiosité. Elle est compilable avec la version 6L38 d'Inform 7.

Le répertoire en « -6G » contient la source de la première version de *Noir d'encre,* écrite avec la version 6G60 d'Inform 7, qui est gardée à titre historique.

## Pour m'aider ##

Rien de plus simple : jouez ! Cela me ferait également plaisir si vous me faites part de vos commentaires/impressions/remarques. Vous pouvez me contacter via [mon site](http://ulukos.com/contact/) ou sur [le forum de la communauté francophone dédié aux fictions interactives](http://ifiction.free.fr/taverne/index.php).

Si vous trouvez une erreur dans le jeu, vous pouvez utiliser les mêmes liens que ci-dessus, ou bien l'outil de signalement de bug (menu ci-contre).