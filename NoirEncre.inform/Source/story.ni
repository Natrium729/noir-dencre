"Noir d'encre" by Nathanaël Marion (in French)

[
Pour faire bref :
Ce code source est libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la « GNU General Public License » telle que publiée par la Free Software Foundation : soit la version 3 de cette licence, soit toute version ultérieure.

Ce programme est distribué dans l'espoir qu'il vous sera utile, mais SANS AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ ni d'ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.

Pour faire long, consultez : http://www.gnu.org/licenses
]

Volume 1 - La destination de l'histoire

[L'extension ci-dessous n'est pas disponible publiquement car elle contient les fonctionnalités additionnelles de la version commerciale de l'histoire.
La ligne doit être commentée pour pouvoir compiler l'histoire pour interpréteur standard.]
[Include Vorplisation by Noir d'encre]

Volume 2 - Initialisation

Use MAX_VERBSPACE of 8192.
Use MAX_NUM_STATIC_STRINGS of 30000.

Book 1 - Extensions

Include Experimental French Features by Nathanael Marion.

Include (- 
	language French
	
	<control-structure-phrase> ::=
		/a/ si ... est début |
		/b/ si ... est |
		/c/ si ... |
		/c/ à moins que |
		/d/ répéter/parcourir ... |
		/e/ tant que ... |
		/f/ sinon |
		/g/ sinon si ... |
		/g/ sinon à moins que |
		/h/ -- sinon |
		/i/ -- ... |
	
-)  in the Preform grammar.

[Include French Imperative Commands by Nathanael Marion.]
Include Basic Screen Effects by Emily Short.
[Include Glulx Image Centering by Emily Short.] [À FAIRE]
Include Music by Daniel Stelzer.
Include Senses by Nathanael Marion.

Figure map is the file "plan.png". [À FAIRE : déplacer]

Book 2 - Temps

Le time of day est 10:30 PM.

Part 1 - Gestion du temps

Chapter 1 - Faire passer le temps en prenant implicitement

For implicitly taking something: 
	suivre l' advance time rule;
	continuer l'activité.

Chapter 2 - Faire passer le temps

En attente is a truth state that varies. [À FAIRE : changer le nom de la variable.]

To faire passer (T - une heure):
	let TC be the turn count;
	let l' heure-cible be T après le time of day;
	diminuer l' heure-cible de une minute;
	maintenant en attente est vrai;
	tant que le time of day n'est pas l' heure-cible:
		[dire "[time of day] - [heure cible]";]
		suivre les turn sequence rules;
		si le time of day est l' heure fatidique ou le time of day est 5:00 AM, break;
	maintenant le turn count est TC;
	maintenant en attente est faux.

Part 2 - L'heure fatidique

To decide what time is the heure fatidique:
	let H be the 12 AM;
	[Si on porte une chose en fer.]
	si le player englobe une chose ferreux (called C):
		si C n'est pas sanctifié, maintenant H est 20 minutes après H;
		sinon maintenant H est 25 minutes après H;
	si le frère englobe une chose ferreux (called C):
		si C n'est pas sanctifié, maintenant H est 20 minutes après H;
		sinon maintenant H est 25 minutes après H;
	[Si on porte le crucifix.]
	si le player englobe le crucifix ou le frère englobe le crucifix:
		maintenant H est 35 minutes après H;
	[Si on porte le fusil chargé.]
	si le player englobe le fusil chargé ou le frère englobe le fusil chargé:
		maintenant H est 20 minutes après H;
	[Si on a dessiné.]
	si le player est dessiné:
		maintenant H est 30 minutes après H;
	si le frère est dessiné:
		maintenant H est 30 minutes après H;
	[Si on a allumé la cheminée.]
	si les branches sont brûlés:
		maintenant H est 25 minutes après H;
	[Si on a fermé le portail.]
	si le portail est fermé et la location n'est pas la forêt:
		maintenant H est 5 minutes après H;
	si le portail est verrouillé et la location n'est pas la forêt:
		maintenant H est 10 minutes après H;
	[Si on a creusé dans la forêt.]
	si la forêt est creusé:
		maintenant H est 25 minutes après H;
	[Si on a répandu du sang dans la forêt.]
	si la forêt est sanglant:
		maintenant H est 30 minutes après H;
	[Si on posé les pièges dans la forêt.]
	si les pièges sont hors scène:
		si les pièges ne sont pas sanctifié, maintenant H est 25 minutes après H;
		sinon maintenant H est 35 minutes après H;
	[Si on a mis le feu à la forêt.]
	si les arbres sont brûlés:
		maintenant H est 60 minutes après H;
	se décider pour H.

Part 3 - Fin du jeu

La fin est une scène.
La fin begins when play begins.
La fin ends sadly when le time of day est l' heure fatidique.
La fin ends happily when le time of day est 5 AM.

When fin ends sadly:
	stop l' ambiance de la location;
	dire "Nous nous immobilisâmes soudain. Nous [italique]l[romain][']avions entendu. [italique]Il[romain] était venu nous chercher. Il était trop tard maintenant.";
	attendre une touche;
	fin de l'histoire en disant "J'avais échoué…".

When fin ends happily:
	stop l' ambiance de la location;
	dire "Les premiers rayons du soleil teintèrent le ciel d'or. Enfin[_]! Nous étions exténués par cette nuit blanche, mais nous nous en étions sortis. Nous pouvions quitter cet endroit et partir le plus loin possible. Il nous restait encore un foyer à trouver.";
	fin définitive de l'histoire en disant "Nous avions réussi[_]!".

When play ends when le player porte la montre et l'histoire n'est pas finie définitivement:
	dire "Tout semblait perdu quand, soudain, la montre luit comme si elle avait été chauffée à blanc. Elle s'ouvrit et les aiguilles tournèrent telle une tempête. Tout s'arrêta aussi subitement que cela avait commencé, et le temps sembla suspendu. Une étrange impression s'insinua en moi[_]: je pouvais continuer d'explorer la demeure.";
	attendre une touche;
	reprendre l'histoire;
	essayer de looking.

Book 3 - Objets

Part 1 - Backdrops

Chapter 1 - Fenêtre

La fenêtre est une privately-named toile de fond.
La fenêtre est dans la chambre, le hall, l' escalier, le salon, la salle à manger, la cuisine, la bibliothèque et le couloir.
La description est "La fenêtre était [si la location est le salon]brisée, et le vent qui filtrait était malsain[sinon]dans un piteux état, et la vitre ne tenait que par miracle[fin si]. On pouvait apercevoir la lune au travers, inquiétante.".
Le sound est "Je me mis à la fenêtre et écoutai, à l'affût du moindre bruit à l'extérieur. Je n'entendis rien[si la location est le couloir], si ce n'était ces hululements exaspérants[fin si].".

Comprendre "vitre", "fenetre" ou "carreaux" comme la fenêtre.

Instead of opening or attacking la fenêtre, dire "[si la location est le salon]Le vent glacial se permettait déjà d'entrer, et je ne voulais pas l'inviter à continuer[sinon]Je ne voulais pas la briser, le vent qui se serait alors engouffré n'aurait rien arrangé[fin si]."

Chapter 2 - Lune

La lune est une toile de fond.
La lune est dans la chambre, le hall, le salon, la salle à manger, la cuisine, la bibliothèque, le couloir, le jardin, le jardin-est et la forêt.
La description est "Une lune gibbeuse éclairait du mieux qu'elle le pouvait, mais sa triste lumière n'avait rien de rassurant.".

Chapter 3 - Marches

Les marches sont une toile de fond.
Les marches sont dans l' escalier et le hall.
La description est "Des marches grinçantes [si la location est le hall]montaient à l'étage[sinon]descendaient vers le hall d'entrée[fin si]. Visiblement, elles allaient s'effondrer d'un instant à l'autre. Nous ne devrions pas nous attarder dans notre [si la location est le hall]ascension[sinon]descente[fin si]."

Comprendre "escalier" comme les marches.

Chapter 4 - Robinet

Le robinet est une toile de fond.
Le robinet est dans la cuisine et la salle de bain.
The description is "[si la location est la cuisine]Un antique évier et son robinet. Il n'avait plus l'air utilisable[sinon]Un vieux lavabo sale et son robinet. Il ne semblait plus en état de marche[fin si].".

[On spécifie aussi la chapelle pour qu'on puisse prier pour le lavabo et l'évier.]
Understand "evier" as le robinet when la location est la cuisine ou la location est la chapelle.
Understand "lavabo" as le robinet when la location est la salle de bain ou la location est la chapelle.

Instead of opening le robinet:
	play the sound gurgle on foreground;
	dire "[parmi]J'ouvris le robinet[_]; un lointain gargouillis se fit entendre depuis les entrailles de la demeure. Puis tout s'arrêta, sans la moindre petite goutte[ou]J'essayais encore d'obtenir de l'eau, pourtant je savais déjà qu'elle ne coulerait pas[stoppant].".
	
Instead of turning le robinet, try opening le robinet.

Part 2 - Propriétés

Chapter 1 - Mouillé / Sec

Une chose peut être mouillé ou sec. Une chose est généralement sec.

To dire message mouillé:
	dire "Il était détrempé et ne servait maintenant plus à rien.".

Chapter 2 - Sanctifié

Une chose peut être sanctifié. Une chose est généralement non sanctifié.

Chapter 3 - En fer

Une chose peut être ferreux. Une chose est généralement non ferreux.		

Book 4 - Actions

In French plonger is a verb.

Part 1 - Compréhension

Comprendre "dechirer [quelque chose]" comme cutting.
Comprendre "arracher [quelque chose]" comme pulling.
Understand the commands "piler" or "broyer" as "squeeze".
Understand the commands "affuter" or "aiguiser" as "rub".
Comprendre "fondre [quelque chose]" comme burning.
Comprendre "verser [quelque chose] dans/sur [quelque chose]" comme inserting it into.

Comprendre "montre [quelque chose]" comme showing it to.
Comprendre "donne [quelque chose]" comme giving it to.
Comprendre "prends [quelque chose]" comme taking.

Part 2 - Parler

[À FAIRE]
Talking to is an action applying to one visible thing.
Understand "talk to [something]", "speak to [something]", "parler a/au/aux/avec/-- [something]" or "questionner [something]" as talking to.
Understand the command "discuter" as "parler".

Does the player mean talking to a person:
	it is very likely.

Check an actor talking to (this is the can't talk to a non-person rule):
	if the noun is not a person:
		if the actor is the player, say "[Tu] [ne peux pas] parler [au noun][_]!" (A) instead;
		else stop the action.

Check an actor talking to (this is the talking to yourself rule):
	if the actor is the player and the noun is the player:
		say "[Tu] [te-se] [parles] à [toi]-même[if the player is plural-named]s[end if] pendant un moment." (A) instead;
	else if the actor is not the player and the noun is the actor:
		stop the action.

Unsuccessful attempt by someone talking to while the reason the action failed is the talking to yourself rule (this is the other people talking to themselves rule):
	say "[The actor] [marmonnes] quelques mots à [lui]-même[if the actor is plural-named]s[end if] pendant un moment." (A).

Report an actor talking to (this is the standard report talking to rule):
	say "[if the actor is the player][Tu][else][The actor][end if] n['][as] rien à dire." (A).

For clarifying the parser's choice of something (called item) while talking to:
	say "([au item])[command clarification break]".

Part 3 - Lire

[À FAIRE]
Understand the command "read" as something new.
Understand the command "lire" as something new.
Reading is an action applying to one visible thing and requiring light.
Understand "read [something]" or "lire [something]" as reading.

Report reading (this is the standard report reading rule):
	say "Il n'y [as] rien de spécial à lire [ici]." (A).

Report someone reading (this is the report other people reading rule):
	say "[The actor] [essayes] de lire quelque chose sur [the noun]." (A).

Part 4 - Attendre

Le standard report waiting rule response (A) est "Je me figeai soudain de désespoir, et attendit. Puis, après quelque temps, je me secouai et repris lentement mes esprits. Il n'était pas question d'abandonner.".

Waiting more is an action applying to a number.
Comprendre "attendre [a time period]" ou "z [a time period]" comme waiting more.

Carry out waiting more:
	let T be le time understood;
	si T est plus grand que une hour, maintenant T est une hour;
	faire passer T.

Report waiting more:
	dire "Je ne savais vraiment plus quoi faire, et nous attendîmes, seuls dans la pénombre. Enfin, quand l'attente ne fut plus supportable, je fis signe à mon frère que nous devions continuer. Il ne fallait pas baisser les bras.".

Part 5 - Sortir

Instead of exiting when le player est dans la location et le player est dans l' intérieur:
	let way be le meilleur chemin de la location au jardin, en utilisant les portes;
	essayer de going way.

Instead of going dehors when le player est dans la location et le player est dans l' intérieur et la porte-ou-endroit au dehors de la location est rien:
	essayer d' exiting.

Part 6 - Écouter

Le sound d' un endroit est généralement "Un inquiétant silence régnait.".

Part 7 - Prendre

The can't take people's possessions rule is not listed in the check taking rulebook.

Check le frère taking quelque chose:
	si le player porte le noun, essayer de giving le noun to le frère instead.

Unsuccessful attempt by le frère taking:
	si la reason the action failed est la can't exceed carrying capacity rule, dire "Il secoua la tête[_]; il ne pouvait rien porter de plus.";
	sinon dire "Mon frère essaya, mais n'y arriva pas.".

After le frère taking quelque chose:
	dire "J'indiquai à mon frère de ramasser [le noun], ce qu'il fit.".

Part 8 - Donner

Check le player giving quelque chose to le frère (this is the brother can't be given more than the carrying capacity rule):
	si le nombre de choses carried by the frère >= le carrying capacity du frère, dire "Il secoua la tête[_]; il ne pouvait rien porter de plus." instead.

After giving quelque chose to le frère:
	dire "Je demandai à mon frère de garder [le noun]. Il acquiesça.".

Check le frère giving quelque chose to le player (this is the player can't be given more than the carrying capacity rule):
	si le nombre de choses carried by le player >= le carrying capacity du player, arrêter l'action.

Unsuccessful attempt by the frère giving:
	si la reason the action failed est la player can't be given more than the carrying capacity rule, dire "Il secoua la tête[_]; j'en portais déjà trop.";
	sinon dire "Il ne put me le donner.".

After le frère giving quelque chose to le player:
	dire "Je redemandai [le noun] à mon frère, qui me [le] donna.".

For supplying a missing second noun while an actor giving:
	maintenant le second noun est le player.

Part 9 - Montrer

Instead of le frère showing quelque chose (called C) to le player:
	essayer de examining C;
	la règle réussit.

For supplying a missing second noun while an actor showing:
	maintenant le second noun est le player.

Part 10 - Attacher

Comprendre "nouer [quelque chose]" comme tying it to.

For supplying a missing second noun while tying:
	si le noun est la serviette, maintenant le second noun est le player;
	sinon maintenant le second noun est le noun.

Part 11 - Verrouiller

Carry out locking quelque chose with quelque chose:
	play the sound lock on foreground.

Carry out unlocking quelque chose with quelque chose:
	play the sound lock on foreground.

Part 12 - Dessiner

Drawing on is an action applying to one thing.
Comprendre "dessiner sur [quelque chose]", "dessiner le/la/un/une/-- symbole/figure sur [quelque chose]" comme drawing on.
Understand the command "tracer" as "dessiner".

Check drawing on:
	dire "Il n'était pas temps de dessiner." instead.

Part 13 - Creuser

Digging is an action applying to nothing.
Comprendre "creuser" comme digging.

Check digging:
	dire "[si le player ne porte pas la pelle]Je n'avas pas de pelle et, de toute façon, c[sinon]C[fin si][']était inutile ici." instead.

La forêt peut être creusé. La forêt est non creusé.

Instead of digging when la location est la forêt:
	si la forêt n'est pas creusé:
		si le player porte la pelle:
			play the sound shovel on foreground;
			dire "Je priai mon frère de rester près du portail et commençait à m'éloigner. Je creusai de toutes mes forces, autour de l'entrée, une fosse, qui [italique]l[romain][']empêcherait [--] peut-être [--] de venir.";
			maintenant la forêt est creusé;
			faire passer 20 minutes;
		sinon:
			 dire "Oui, peut-être que creuser un fossé ici [italique]l[romain][']aurait ralenti[_]? Mais je n'avais pas de pelle.";
	sinon:
		dire "J'avais déjà creusé un fossé autour de l'entrée. Je ne savais pas s'il allait être utile, mais que pouvions-nous faire d'autre[_]?".
			

Part 14 - Brûler

The block burning rule is not listed in the check burning rulebook.

Check burning quelque chose (this is the can't burn without fire rule):
	si le player ne porte pas l' allumette, dire "Je n'avais rien pour allumer un feu." instead.

Check burning quelqu'un (this is the block burning people rule):
	si le noun est le player, dire "Je n'allais pas me brûler[_]!" instead;
	si le noun est le frère, dire "Je ne pouvais pas lui faire de mal. Pas après tout ce qu'il avait subi par ma faute." instead.

Report burning:
	dire "Il aurait été bête de brûler ceci.".

Book 5 - Phrases

To découvrir (découverte - une chose):
	si le nombre de choses carried by le player >= la carrying capacity du player:
		maintenant la découverte est dans le porteur du player;
		dire "[à la ligne]Malheureusement, je ne pouvais rien porter de plus, et dut [à propos de la découverte][le] laisser ici.";
	sinon:
		maintenant le player porte la découverte;

Book 6 - Sons

Chapter 1 - Liste des sons

Section 1 - Effets

[Introduction]
Sound are you sleeping is the file "tu_dors.ogg".
Sound wolf is the file "loup.ogg".

[Salon]
Sound falling is the file "chute.ogg".
Sound item falling is the file "chute_objet.ogg".

[Salle de bain]
Sound gurgle is the file "gargouillis.ogg".
Sound pharmacy is the file "grincement_pharmacie.ogg".

[Cuisine]
Sound sharpening is the file "affutage.ogg".
Sound creaking cupboard is the file "grincement_armoire.ogg".
Sound pot is the file "pot.ogg".

[Chambre]
Sound match is the file "allumette.ogg".
Sound gas is the file "gaz.ogg".
Sound tear cloth is the file "dechirement_tissu.ogg".

[Bibliothèque]
Sound books is the file "livres.ogg".
Sound book is the file "livre.ogg".

[Jardin]
Sound splashing is the file "eclaboussure.ogg".
Sound closing gate is the file "fermer_portail.ogg".
Sound opening gate is the file "ouvrir_portail.ogg".
Sound shovel is the file "pelle.ogg".

[Remise]
Sound jaws is the file "machoires.ogg".

[Endroits multiples]
Sound draw is the file "tiroir.ogg".
Sound lock is the file "serrure.ogg".

[Autres]
Sound clock is the file "horloge.ogg".
Sound steps1 is the file "pas_plancher1.ogg".
Sound steps2 is the file "pas_plancher2.ogg".
Sound stairs is the file "escalier.ogg".
Sound nature steps is the file "pas_feuilles.ogg".

Section 2 - Ambiance

Sound owl is the file "hibou.ogg". [Dans le couloir.]
Sound tick is the file "tictac.ogg". [Dans le hall et l'escalier.]
Sound hearth is the file "foyer.ogg". [Dans le salon après avoir allumé la cheminée]
Sound forest fire is the file "feu_foret.ogg". [Dans la forêt pendant l'incendie]

Chapter 2 - Ambiance des pièces

Un endroit has a sound name called ambiance.
L' ambiance d' un endroit est généralement sound wolf.

After going to a room (called destination):
	si l' ambiance de la destination est sound wolf, stop l' ambiance de la room gone from;
	sinon loop l' ambiance de la destination on background;
	continuer l'action.

Chapter 3 - Coups des heures

Every turn when les minutes dans le time of day est 59:
	si les heures dans le time of day > 11:
		let N be les heures dans le time of day - 11;
		play the sound clock on midground repeating N times;
	sinon:
		let N be les heures dans le time of day + 1;
		play the sound clock on midground repeating N times.

Volume 3 - Jeu

Book 1 - Introduction

When play begins (this is the warn the player about sounds rule):
	si glulx sounds is supported:
		dire "[italique]Pour profiter au mieux de ce jeu, il vaut mieux porter des écouteurs ou un casque, et le jouer dans une pièce sombre…[romain]";
	sinon:
		dire "[italique]Bien qu'il est possible de jouer à ce jeu sans son, son ambiance repose énormément sur les effets sonores. Cependant, votre interpréteur ne les supporte pas. Un bon interpréteur les supportant est Gargoyle.[romain]";
	attendre une touche;

When play begins:
	effacer l'écran;
	attendre une touche;
	maintenant le story viewpoint est la première personne du singulier;
	maintenant le story tense est le passé simple;
	dire "«[_]Tu dors[_]?[_]»[à la ligne]";
	play the sound are you sleeping on foreground;
	attendre une touche;
	dire "Je lui répondis que non. Après tout, comment pouvais-je dormir[_]?";
	attendre une touche;
	dire "J'essayais de trouver des mots qui le rassureraient quand soudain…[à la ligne]";
	attendre une touche;
	play the sound wolf on foreground;
	attendre une touche;
	dire "[à la ligne]Nous nous pétrifiâmes de terreur. [italique]Il[romain] était là. Si l'on ne faisait rien, nous étions perdus. [italique]Il[romain] allait nous retrouver, rapidement. Peut-être y avait-il un moyen de nous sauver, mon frère et moi, dans ce bâtiment en ruines.[saut de paragraphe]";
	attendre une touche;
	dire "Il fallait l'explorer, malgré mon appréhension…[à la ligne]";
	attendre une touche.

Book 2 - Jeu

Part 1 - Rez-de-chaussée

L' intérieur (m) est une région.
Le hall, la salle à manger, la cuisine, le salon, la salle de bain, l' escalier, la bibliothèque, le cabinet, le couloir, la chambre et la cave sont dans l' intérieur.

[À FAIRE : simplifier la condition]
Carry out going from un endroit (called E1) to un endroit (called E2) in intérieur:
	Unless (E1 is escalier and E2 is hall) or (E1 is hall and E2 is escalier):
		si une chance de 1 sur 2 réussit, play the sound steps1 on foreground;
		sinon play the sound steps2 on foreground.

Chapter 1 - Hall

Le hall est un endroit. "Un hall de taille imposante, à l'aspect lugubre. Le mur crasseux laissait entrevoir des morceaux de papier peint décollés. Le décor, dans la faible luminosité, n'avait rien d'accueillant. Pourtant, cette bâtisse était le meilleur moyen de nous abriter. Du moins, je l'espérais…[à la ligne]Nous pouvions aller [si le salon est visité]dans le salon [fin si]au nord, [si la salle à manger est visitée]dans la salle à manger [fin si]à l'ouest, [si la salle de bain est visitée]dans la salle de bain [fin si]à l'est, ou sortir dans le jardin par le sud. Nous pouvions également emprunter l'escalier pour aller à l'étage.".
Le printed name est "Dans le hall d'entrée".
Le sound est "Le tic-tac narquois de l'horloge brisait le silence.".
L' ambiance est le sound tick.

Carry out going from le hall to l' escalier:
	play the sound stairs on foreground.

Section 1 - Horloge

L' horloge (f) est une chose fixée sur place dans le hall. "[si cassée]L'horloge demeurait là, inutile[sinon]Au mur, une imposante horloge affichait environ [time of day + 1 minute arrondi à 5 minutes en lettres][fin si].".
La description est "Une énorme horloge était adossée au mur. [si l' horloge est cassée]Elle n'avait plus d'aiguilles par ma faute, et il était maintenant impossible de savoir l'heure[sinon]Ses aiguilles avançaient trop vite à mon goût. Elles semblaient nous narguer du peu de temps qu'il nous restait[_]; en effet, il était environ [time of day + 1 minute arrondi à 5 minutes en lettres]. Je ne devais pas m'attarder pour la contempler[fin si].".
Le sound est "L'inlassable écoulement du temps égrenait ses tics et ses tacs. Il ne fallait pas s'attarder."
L' horloge peut être cassée. L' horloge est non cassée.

Comprendre "heure" comme l' horloge.

Instead of attacking l' horloge:
	maintenant l' horloge est cassée;
	dire "Le temps pressait et cette horloge ne cessait de nous le rappeler par son tic-tac narquois. Dans un accès de rage, je me jetai sur le cadran et lui donnai un coup. Il vola en éclat, et seule la main de mon frère sur mon épaule m'arrêta. J'étais honteuse maintenant, car j'avais perdu mon sang-froid devant lui et par ma faute, nous ne pouvions plus savoir l'heure. En outre, si les aiguilles étaient brisées, le mécanisme, lui, continuait de fonctionner, et le bruit n'avait pas cessé.".

Section 2 - Papier peint

Le papier peint est une chose décorative dans le hall.
La description est "Un reste de décoration subsistait, tentant tant bien que mal de s'accrocher aux murs couverts de crasse. Depuis quand cet endroit était-il désert[_]?".
Le sound est "Je collai mon oreille au mur. Pas un bruit, comme si la maison retenait sa respiration.".

Comprendre "mur" comme le papier peint.

Instead of taking le papier peint:
	dire "Ce papier peint m'exaspérait, pour je ne savais quelle raison, et je tentais de l'arracher en vain.";
	faire passer 3 minutes.

Instead of pulling le papier peint, essayer de taking le papier peint.
Instead of taking off le papier peint, essayer de taking le papier peint.

Chapter 2 - Salon

Le salon est au nord du hall. "Un riche salon, mais éprouvé par le temps. Il était spacieux, et le peu de mobilier accentuait cet aspect. La vitre cassée laissait passer un vent glacial, qui semblait [si les branches sont brûlés]mener un combat sans merci contre la chaleur du feu[sinon]se moquer de la cheminée éteinte. Il ne fallait pas traîner[fin si].".
Le printed name est "Au milieu du salon".
Le sound est "Le vent nous murmurait de quitter cet endroit au plus vite avant que… Non, j'étais en train de délirer.".

Section 1 - Dessins

Les dessins (f) sont une chose au singulier privately-named dans le salon.
Le printed name est "feuille".
La description est "Le papier abîmé était le support de nombreux dessins, dont la plupart étaient raturés[_]; d'autres avaient été effacés et ne subsistaient que par un pâle tracé. Mais au centre de la feuille, parmi l'amas des crayonnés qui se superposaient, un grand symbole entouré d'un trait épais attirait l[']œil.[si le journal est lu] Était-ce la figure que mentionnait le journal[_]?[fin si]".

Comprendre "feuille" comme les dessins.
Understand "dessins", "dessin", "symbole" or "symboles" as les dessins when we have examined les dessins.

Carry out examining les dessins pour la première fois:
	maintenant le printed name des dessins est "dessins";
	maintenant les dessins sont male;
	maintenant les dessins sont masculine gender;
	maintenant les dessins sont au pluriel.

Instead of examining les mouillé dessins, dire message mouillé.

Instead of burning les dessins when le player porte l' allumette:
	retirer l' allumette du jeu;
	dire "Ces dessins ne m'inspiraient rien qui vaille. Je pris l'allumette et le réduisis en cendre, sous le regard incompréhensif de mon frère.";
	faire passer 2 minutes.

Section 2 - Fusil

Le fusil est une chose dans le salon. "Un fusil était accroché au mur, au-dessus de la cheminée.".
La description est "Une arme antique, qui ne servait maintenant plus que de décoration destinée à prendre la poussière[si le fusil n'est pas chargé][_]; en effet, il n'avait pas l'air chargé[sinon]. Je l'avais chargé, mais je ne savais pas si le fusil était fonctionnel[fin si]. Je détestais les armes depuis ce jour qui paraissait déjà si lointain, mais elle pouvait s'avérer utile.".
Le sound est "Avec un peu de chance, nous n'aurons jamais plus à entendre de coup de feu.".
Le fusil peut être chargé. Le fusil est non chargé.

After taking le fusil pour la première fois, dire "Je décrochai le fusil du mur.".

Section 3 - Moule

Le moule est une chose.
La description est "Ce lourd objet m'avait bien fait mal. On aurait dit… une sorte de moule. Mais j'ignorais à quoi cela pouvait servir[si chaud].[à la ligne]Il contenait de l'argent en fusion[sinon si rempli].[à la ligne]Je pouvais l'ouvrir, maintenant que le métal était froid[fin si].".
Le moule peut être chaud. Le moule est non chaud.
Le moule peut être rempli. Le moule est non rempli.

[Pour découvrir le moule]
Instead of going from le salon to le hall pour la première fois:
	play the sound falling on foreground;
	attendre une touche;
	dire "Mon pied venait de heurter quelque chose, et la chute fut douloureuse. Mon frère accourut et m'aida à m'asseoir. Je pris le temps de souffler cinq minutes, tandis qu'il me montrait sur quoi j'avais trébuché. C'était un objet étrange, une espèce de moule…[à la ligne]";
	découvrir le moule;
	attendre une touche;
	faire passer 5 minutes.

[Pour refroidir le moule]
Instead of inserting l' eau into le chaud moule:
	[À FAIRE : son de sifflement]
	dire "Je versai l'eau sur le moule afin de solidifier le métal. Il y eut un sifflement, puis le calme revint.";
	maintenant le moule n'est pas chaud;
	maintenant le moule est rempli.

[Quand on ouvre le moule]
Instead of opening le moule, dire "On pouvait apparemment l'ouvrir. Je le fis plusieurs fois, sans en voir l'utilité. Je le fermai de dépit.".

Instead of opening le chaud moule, dire "Il s'agissait d'une très mauvaise idée, je me serais brûlée.".

Instead of opening le rempli moule:
	dire "Maintenant que tout paraissait sûr, je pris fermement le moule et l'ouvris.";
	attendre une touche;
	play the sound item falling on foreground;
	dire "Un objet fuselé et brillant tomba à terre. Je me baissai et m'en saisis. Une balle[_]?";
	attendre une touche;
	découvrir la balle.

Section 4 - Balle

La balle est une chose.
La description est "Cette balle était en argent. En argent… peut-être que…[_]?".
Le sound est "Aussi belle fût-elle, j'espérais que l'on ne l'endendrait jamais sortir d'une arme.".

Instead of inserting la balle into le fusil:
	retirer la balle du jeu;
	dire "J'essayai de charger le fusil, mais je ne savais vraiment pas comment faire, et mon frère non plus. Après avoir examiné l'arme dans ses moindres détails, je trouvai pourtant une solution.";
	maintenant le fusil est chargé;
	faire passer 2 minutes.

Instead of burning la balle when le player porte l' allumette:
	retirer l' allumette du jeu;
	retirer la balle du jeu;
	dire "Cette balle… était une balle, une arme, et je voulais oublier… J'approchai la flamme et la fit fondre et disparaître.".

Section 5 - Tournevis

Le tournevis est une chose dans le salon.
La description est "Un tournevis banal, qui possédait un manche en bois. Combien de fois avais-je vu un outil semblable, à l'époque où… Non, il fallait se concentrer sur le présent.".

Section 6 - Cheminée

La cheminée est un contenant décoratif ouvert non ouvrable privately-named dans le salon. 
La description est "[si les branches sont brûlés]Un petit feu brûlait dans la cheminée. Il ne réchauffait pas beaucoup, mais on se rassurait comme on pouvait[sinon]La cheminée était vide et triste. Si seulement[fin si]…[à la ligne]".
Le sound est "[si les branches sont brûlés]Le crépitement du foyer remplissait la pièce. Ce n'était pas beaucoup, mais il rassurait[sinon]Le foyer était triste et silencieux.[fin si].".
Le carrying capacity de la cheminée est 1.

Comprendre "cheminee" ou "foyer" comme la cheminée.
Understand "feu" or "flammes" as la cheminée when les branches sont brûlés.

Instead of switching on la cheminée:
	si les branches ne sont pas dans la cheminée:
		dire "Il n'y avait pas de bois dans la cheminée.";
	sinon:
		essayer de burning les branches.

Chapter 3 - Salle à manger

La salle à manger est à l'ouest du hall. "Ici, seule la poussière témoignait du temps qui s'était écoulé depuis l'abandon de la maison. Tout était en ordre, comme si un spectre du passé tenait à ce qu'un éventuel visiteur puisse encore se restaurer. Je secouai la tête, il fallait chasser ces pensées. Plus personne n'habitait ici, je commençais à divaguer.[à la ligne]La porte au nord menait [si la cuisine n'est pas visitée]sûrement [fin si]à la cuisine.".
Le printed name est "À l'intérieur de la salle à manger".
Le sound est "Des bruits de couverts, des rires, des discussions… On entendait presque les anciens habitants. Maintenant, on ressentait un grand chagrin, un chagrin qui témoignait d'un effroyable événement survenu dans cette maison, il y a longtemps.".
La salle à manger est privately-named

Section 1 - Table à manger

La table à manger est un privately-named support. La table à manger est dans la salle à manger.
La description est "Une grande table destinée à de nombreux convives et qui attendait, depuis leur disparition, que quelqu'un vienne manger avec elle.".

Comprendre "table", "a" ou "manger" comme la table.

Section 2 - Couteau

Le couteau est une chose ferreux sur la table à manger.
La description est "Ce couteau me remémorait de tristes souvenirs. Je voyais défiler des images dans ma tête, je… Je repris soudain mes esprits. Que dirait mon frère s'il me voyait faiblir[_]?[à la ligne][si rouillé]Sa lame rouillée le rendait totalement inutile[sinon]Maintenant qu'il était aiguisé, il devenait un ustensile dangereux[fin si].".

Le couteau peut être rouillé ou aiguisé. Le couteau est rouillé.
Le couteau peut être utile ou inutile. Le couteau est inutile.

After burning le couteau:
	dire "J'approchai l'allumette du couteau. Il chauffa, mais la flamme n'était pas assez chaude pour le faire fondre.";
	faire passer 5 minutes.

Instead of cutting le frère when le player porte le couteau, dire "Jamais je n'aurais levé la main sur mon frère. Pas après tout le mal qu'il avait dû subir à cause de moi.".

Instead of cutting le player when le player porte le couteau et le player n'est pas coupé:
	si le couteau est rouillé:
		dire "La lame était horriblement rouillée et je ne me serais pas risquée à me couper avec elle.";
	sinon:
		maintenant le player est coupé;
		dire "Une folle idée me vint[_]; ma main tremblait, je fermai les yeux. Malgré ce que cet acte ravivait en moi, j'entaillai la paume de ma main. Je tressaillis, et le sang commença à couler[si le couteau est utile].[à la ligne]Du sang… Une des entrées du journal me revint en mémoire[fin si].".

Instead of cutting le player when le player est coupé ou la serviette est une partie du player, dire "Je m'étais déjà coupée, et je ne tenais pas à recommencer.".

Instead of attacking the player when le player porte le couteau, essayer de cutting le player.

Le saignement est une scène.
Le saignement begins when le player est coupé.
Le saignement ends sadly when the time since le saignement began est 15 minutes.
Le saignement ends happily when la serviette est une partie du player.

Every turn when en attente is faux during le saignement:
	dire "[parmi]Ma coupure me faisait mal[ou]Le sang continuait de couler[ou]Je ne la laissais pas paraître ma douleur à mon frère[ou]Je serrais ma main très fort tant la douleur était grande[au hasard].".

When saignement ends sadly:
	dire "Ma tête se mit à tourner… Ma vision se troubla… J'avais perdu trop de sang.";
	attendre une touche;
	fin de l'histoire en disant "C'était la fin…".

Chapter 4 - Cuisine

La cuisine est au nord de la salle à manger. "La cuisine, contrairement à la salle à manger, était véritablement abandonnée, des ustensiles traînant çà et là. Une imposante armoire se dressait dans un coin, un plan de travail au fond de la pièce, à côté de l'évier… J'imaginais de nombreuses domestiques s'affairant à cet endroit, même si je savais qu'il ne fallait plus songer au passé.".
Le printed name est "Dans la cuisine".

Section 1 - Ustensiles

Les ustensiles sont une chose décorative dans la cuisine.
La description est "Des ustensiles jonchaient le sol, mais aucun n'avait l'air utilisable.".

Instead of searching les ustensiles pour la première fois:
	say "Malgré tout, je décidai de faire l'inventaire de ce qu'il y avait ici. Au bout d'un moment, nous trouvâmes une belle cuillère. Elle était terne, mais il s'agissait assurément d'argent.";
	découvrir la cuillère;
	faire passer 7 minutes.

Instead of searching les ustensiles, dire "Il n'y avait plus rien d'intéressant.".

Section 2 - Cuillère

La cuillère est une chose privately-named.
La description est "L'ustensile en argent était vieux et terni. Il avait dû connaître de nombreux repas.".

Comprendre "cuillere" ou "cuiller" comme la cuillère.

Instead of burning la cuillère when le player porte l' allumette:
	retirer l' allumette du jeu;
	si le moule est touchable:
		dire "J'approchai la flamme de la cuillère. Lentement, elle commença à s'amollir. Enfin, quelques gouttes tombèrent dans le moule.";
		maintenant le moule est chaud;
	sinon:
		dire "J'approchai la flamme de la cuillère. Lentement, elle commença à s'amollir. Enfin, les premières gouttes tombèrent…[à la ligne]";
		attendre une touche;
		dire "Soudain, une douleur me fit retenir un cri. Du métal en fusion venait de me brûler la main. Je ne recommencerais plus cela.";
	retirer la cuillère du jeu;
	faire passer 5 minutes.

Section 3 - Armoire

L' armoire (f) est un contenant décoratif fixé sur place fermé ouvrable dans la cuisine.
Le printed name est "lourde armoire".
La description est "[si poussé]Cette armoire dissimulait un passage dans le sol[_]! Il devait sûrement mener à la cave. Était-ce une bonne idée de descendre, je n'aurais su le dire[sinon]Cette imposante armoire occupait un coin de la pièce. Elle n'avait rien de particulier[fin si].".
Le sound est "Elle grinçait quand on manipulait ses portes.".

Comprendre "lourde" comme l' armoire.

Carry out opening l' armoire:
	play the sound creaking cupboard on foreground.

Carry out closing l' armoire:
	play the sound creaking cupboard on foreground.


L' armoire peut être poussé. L' armoire est non poussé.

Instead of pushing la non poussé armoire:
	dire "Je pris appui sur le meuble et poussai de toutes mes forces[_]; en vain. J'appelai alors mon frère et lui expliquai ce que je voulais faire. À deux, nous tentâmes ce que je n'avais pas réussi toute seule.";
	attendre une touche;
	dire "Au début, rien ne se passa, puis, finalement, l'armoire bougea. Il y avait un passage caché sous elle[_]! Allions-nous descendre par là[_]?";
	attendre une touche;
	maintenant l' armoire est poussé;
	faire passer 2 minutes.

Instead of pushing la poussé armoire, dire "Nous n'avions plus besoin de la pousser, et nous n'avions pas de temps à perdre.".

Section 4 - Marmite

La marmite est un contenant ferreux dans l' armoire.
La description est "Une petite marmite esseulée [si la marmite est dans l' armoire]attendait[sinon]avait longtemps attendu[fin si] dans l'armoire[si la marmite est une partie de la corde] et était maintenant attachée à la corde[fin si]. Elle était encore en bon état.".

After printing the name of la marmite while not taking l' eau or examining la marmite:
	si l' eau est dans la marmite, dire " pleine d'eau".

[Quand on attache la marmite à la corde]
Instead of tying la marmite to la corde:
	maintenant la marmite est une partie de la corde;
	dire "J'attachai la marmite à la corde.".

Instead of tying la corde to la marmite, essayer de tying la marmite to la corde.

[Son comportement quand elle est attachée]
Instead of taking la marmite when la marmite est une partie de la corde:
	essayer de taking la corde.

Instead of dropping la marmite when la marmite est une partie de la corde:
	essayer de dropping la corde.

Instead of inserting la marmite into quelque chose (called C):
	essayer inserting la corde into C.

Instead of putting la marmite on quelque chose (called C):
	essayer de putting la corde on C.

Section 5 - Plan de travail

Le plan de travail est un décoratif support. Le plan de travail est dans la cuisine.
La description est "Une surface de travail qui avait beaucoup dû servir autrefois. Maintenant, elle restait là, silencieuse et poussiéreuse.".

Section 6 - Pierre à affûter

La pierre à affûter est une chose privately-named sur le plan de travail.
La description est "Cette pierre rectangulaire permettait d'aiguiser divers ustensiles de cuisine. Elle ne paraissait pas avoir été beaucoup utilisée.".

Comprendre "pierre", "a" ou "affuter" comme la pierre à affûter.

Instead of rubbing le rouillé couteau when la pierre à affûter est touchable:
	play the sound sharpening on foreground;
	maintenant le couteau est aiguisé;
	dire "Je le préférais inoffensif, mais j'aiguisai tout de même le couteau, qui retrouva son tranchant de jadis.".

Instead of rubbing l' aiguisé couteau, dire "Il était déjà affûté.".

Section 7 - Mortier

Le mortier est un contenant sur le plan de travail.
La description est "Un mortier et un pilon qui permettaient d'écraser des choses.".

Comprendre "pilon" comme le mortier.

Instead of inserting quelque chose which is not les pétales into le mortier, dire "Je ne pouvais pas piler ceci.".

Instead of squeezing les pétales when les pétales sont dans le mortier:
	si l' eau est dans le mortier:
		[À FAIRE : son pour piler]
		retirer l' eau du jeu;
		retirer les pétales du jeu;
		maintenant la mixture est dans le mortier;
		dire "J'écrasai les pétales, jusqu'à l'obtention d'une sorte de pâte.";
	sinon:
		dire "Je ne pensais pas qu'écraser les pétales seuls aurait été utile.".

Section 8 - Mixture

La mixture est une chose.
La description est "J'avais préparé cette mixture en écrasant les pétales avec de l'eau. Il ne me restait plus qu'à l'utiliser.".
Le sound est "Elle faisaut un drôle de bruit quand on la touillait.".
Le scent est "Elle sentait bon, à l'image des fleurs d'où elle provenait.".
Le taste est "J'en pris un peu avec mon index et en goûtai. Je ne savais pas si c'était toxique, mais ce n'était pas mauvais.".

Comprendre "preparation" ou "pate" comme la mixture.

Instead of taking la mixture, dire "Il valait mieux la laisser dans le mortier.".

Instead of eating la mixture, essayer de tasting la mixture.

[Quand on dessine]
Instead of drawing on le player when la mixture est touchable et le journal est lu et we have examined les dessins:
	si le player est dessiné:
		dire "J'avais déjà dessiné le symbole sur moi-même.";
	sinon:
		maintenant le player est dessiné;
		dire "Je dessinai le symbole ésotérique sur mon front. Cela serait-il suffisant[_]?".

Instead of drawing on le frère when la mixture est touchable et le journal est lu et we have examined les dessins:
	si le frère est dessiné:
		dire "J'avais déjà dessiné le symbole sur mon frère.";
	sinon:
		maintenant le frère est dessiné;
		dire "Je dessinai le symbole ésotérique sur son front. Cela serait-il suffisant[_]?".

Chapter 5 - Cave

La cave est en dessous de la cuisine. "Un sombre et humide endroit avait été aménagé sous la cuisine. Ici, les murs étaient de pierre nue, et le sol aussi. Autrement, rien d'autre que le bruit de nos pas qui résonnait.".
Le printed name est "Sous terre".
La cave est sombre.
Le sound de la cave est "Le bruit de nos pas qui se réverbérait sur les murs de la cave lorsque nous marchions. Lorsque nous nous arrêtions, le silence.".

For printing the description of a dark room when la location est la cave:
	dire "D'oppressantes ténèbres régnaient ici. Je me sentais comme étouffée et je ne voulais pas rester trop longtemps.".

For printing the announcement of darkness when la location est la cave:
	dire "La noirceur envahit de nouveau la cave.".

Instead of going to la cave when l' armoire n'est pas poussé, dire "[texte de la can't go that way rule response (A)][à la ligne]".

Section 1 - Murs

Les murs sont des choses décoratives dans la cave.
La description est "Ces murs se refermaient sur nous, nous étouffaient. Ils voulaient que la cave soit notre tombeau.".

Comprendre "mur" comme les murs.

Instead of touching les murs, dire "Des pierres d'une humidité glaciale.".

Instead of attacking les murs pour la première fois:
	maintenant la pierre du mur est dans la cave;
	dire "Je cognais ces pierres qui nous voulaient du mal. L'une d'elles sonna creux. Étrange…[à la ligne]";
	établir les pronoms pour la pierre du mur;
	faire passer 2 minutes.

Section 2 - Pierre

La pierre du mur (f) est une chose. "Une pierre sonnait creux.".
La description est "Cette pierre [si manipulé]s'était descellée du mur de la cave[sinon]sonnait creux[fin si].".

Instead of pulling la pierre du mur, essayer de taking la pierre du mur.

After taking la pierre du mur:
	dire "La pierre se descella sans difficulté. Avec appréhension, je mis ma main dans la cavité qu'elle avait libérée[_]; elle rencontra quelque chose, que je pris et examinai. Il s'agissait d’un curieux cylindre.";
	découvrir le cylindre;
	établir les pronoms pour le cylindre.

Section 3 - Cylindre

Le cylindre est un contenant fermé non ouvrable.
La description est "Cet étrange objet absorbait toute la lumière qu'on lui donnait. Il en devenait presque invisible dans la pénombre de la demeure. De plus, il était bien plus lourd que sa taille ne le laissait présager.[à la ligne][italique][crochet]Cet objet est le début d'une énigme optionnelle qui n'a pas été implantée. Ne perds pas ton temps dessus[_]![crochet fermant][romain]".
[Le cylindre can be revealed. The cylindre is not revealed.]

Section 4 - Montre de gousset

La montre de gousset (f) est une chose dans le cylindre.
The description is "J'ouvris la montre et l'examinai. Étrangement, elle possédait cinq aiguilles[_]: les trois premières étaient celles que l'on retrouvait sur toutes les horloges [--] pour les heures, les minutes et les secondes [--], elles indiquaient [time of day + 1 minute en lettres][_]; les deux autres étaient rouge sang et leur pointe en forme de tête de mort montraient [heure fatidique en lettres]. Que cela pouvait-il bien signifier[_]?".

Comprendre "a gousset" comme la montre.

Instead of opening la montre, essayer de examining la montre.

Chapter 6 - Salle de bain

La salle de bain est à l'est du hall. "Une salle de bain complètement fêlée et sale. Pourtant, sous toute cette crasse, on devinait une baignoire ou un lavabo de luxe. Le propriétaire de cette demeure avait dû être aisé. Cependant, cette richesse ne l'avait pas sauvé, et ne nous sauverait pas non plus[_]; il fallait faire vite.".
Le printed name est "Dans la salle de bain".
La salle de bain est sombre.

For printing the description of a dark room when la location est la salle de bain:
	dire "Les volets semblaient fermés ici. Je ne voyais en tout cas pas grand-chose.".

For printing the announcement of darkness when la location est la salle de bain:
	dire "L'obscurité envahit de nouveau tous les recoins de la salle de bain.".

Section 1 - Baignoire

La baignoire est un contenant entrable décoratif dans la salle de bain.
La description est "Cette baignoire était très abîmée, mais on devinait tout de même sa valeur.".
Le sound est "J'imaginais l'eau couler à l'intérieur de la baignoire. Peut-être qu'un jour nous pourrons enfin nous détendre.".

Section 2 - Serviette

La serviette est une chose dans la salle de bain.
La description est "Cette serviette paraissait plus propre que la plupart des autres choses de cette maison[si la serviette est une partie du player]. Je m'en étais servi comme bandage[fin si].".

Instead of tying la serviette to le player during le saignement:
	maintenant la serviette est une partie du player;
	dire "Je nouai la serviette autour de ma main et arrêtai l'hémorragie.".

Instead of tying la serviette to le player when la serviette est une partie du player:
	dire "Je l'avais déjà nouée autour de ma main.".

Instead of taking la serviette when la serviette est une partie du player, dire "Il valait mieux la laisser là où elle était.".

Section 3 - Pharmacie

La pharmacie est un contenant fermé ouvrable fixé sur place dans la salle de bain.
La description est "Un petit meuble de rangement destiné aux médicaments était accroché au-dessus du lavabo. Il n'avait rien de spécial.".

Carry out opening la pharmacie:
	play the sound pharmacy on foreground.

Carry out closing la pharmacie:
	play the sound pharmacy on foreground.

Section 4 - Pot

Le pot est une chose dans la pharmacie.
La description est "Un vieux pot que j'avais découvert dans la pharmacie. Le médicament à l'intérieur, une pâte durcie par le temps, n'était pas engageant et dégageait une forte odeur. Je préférais tenir cela loin de moi.".
Le sound est "Il sonnait assez creux : il ne restait pas tant de médicament que cela.".
Le scent est "Le pot dégageait une forte odeur de médicament.".
Le taste est "Je ne savais pas s'il s'agissait de pommade ou si le médicament était destiné à l'ingestion. Dans le doute, je m'abstins.".

Comprendre "medicament" comme le pot.

Instead of searching le pot when la clef rouillée est hors scène pour la première fois:
	dire "Bravant mon dégoût, je mis la main dans le pot. Étrangement, je rencontrai un objet dur. Je le pris et l'examinai[_]: une énorme clef rouillée. Celui qui l'avait mise là ne voulait pas qu'on la trouve…[à la ligne]";
	découvrir la clef rouillée.

Instead of eating le pot, essayer de tasting le pot.

Instead of attacking le pot:
	play the sound pot on foreground;
	retirer le pot du jeu;
	dire "Je jetais le pot à terre. Il se brisa en mille morceaux[si la clef rouillée est hors scène], révélant une grosse clef. Je me demandais quel homme sensé l'aurait mise là.";
	si la clef rouillée est hors scène et la clef rouillée n'est pas mouillé, maintenant la clef rouillée est dans la location.

Section 5 - Clef rouillée

La clef rouillée est une chose privately-named.
La clef rouillée ouvre le portail.
The description is "Une vieille clef volumineuse trouvée dans un pot de médicament. Elle en gardait des traces[_]: l'odeur, par exemple.".
Le scent est "Elle avait gardé l'odeur du médicament dans lequel elle avait été conservée pendant si longtemps [--] sûrement des années."

Comprendre "clef", "cle" ou "rouillee" comme la clef rouillée.

Part 2 - Extérieur

L' extérieur est une région.
Le jardin, jardin-ouest, jardin-est et la forêt sont dans l' extérieur.

Carry out going to un endroit in extérieur:
	play the sound nature steps on foreground;

Chapter 1 - Jardin

Le jardin est au sud du hall. "La lune nous observait depuis un ciel sans étoiles. Sous son triste regard, les ombres paraissaient encore plus inquiétantes. Les plantes semblaient mortes depuis la nuit des temps, excepté des épineux, qui nous tendaient leurs branches accusatrices. Elles voulaient sûrement que nous quittassions leur domaine.[à la ligne]Le jardin s'étendait plus à l'ouest et à l'est, et la forêt se dressait, menaçante, par-delà [le portail] au sud, mais mon frère me pressait de rentrer."
Le printed name est "Dans le jardin".

Instead of going dedans when la location est le jardin, essayer de going nord.

Section 1 - Puits

Le puits est une chose dans le jardin.
La description est "Un puits de pierre délabré se dressait à mi-chemin entre le portail et le bâtiment. Il était profond[_]: impossible de distinguer quoi que ce soit dans les ténèbres en contrebas.".
Le sound est "Je me penchai et tendis l'oreille vers les ténèbres au fond du puits, qui ne me murmura rien."

Le fond du puits est un endroit.

[Quand on jette quelque chose dans le puits]
Instead of inserting une chose into le puits:
	maintenant le noun est dans le fond du puits;
	maintenant le noun est mouillé;
	let léger be faux;
	si le noun est l' allumette ou le noun est la boîte d'allumettes ou le noun est les dessins ou le noun est les fleurs ou le noun est les lambeaux ou le noun est le plan de la demeure ou le noun est les pétales:
		maintenant léger est vrai;
	si léger est faux:
		play the sound splashing on foreground;
	dire "Je jetai [le noun] dans le puits[si léger est faux], et [il] [plonges] dans une éclaboussure qui déchira le silence[fin si].".

Instead of throwing something (called projectile) at le puits, essayer de inserting le projectile into le puits.

Instead of inserting l' eau into le puits, dire "Il aurait été inutile de rendre l'eau au puits.".

[Quand on jette la marmite dans le puits]
Instead of inserting la marmite into le puits when la marmite est une partie de la corde:
	play the sound splashing on foreground;
	si l' eau est hors scène, maintenant l' eau est dans la marmite;
	dire "Je jetai la marmite dans le puits et la fit descendre le plus possible. Enfin, je la remontai[si l' eau était hors scène]. Je récupérai ainsi de l'eau[fin si].";
	si l' eau était hors scène, maintenant l' eau est dans la marmite;
	si quelque chose est dans le fond du puits:
		let trouvaille be une chose dans le fond du puits aléatoire;
		dire "À ma grande surprise, je retrouvai [la trouvaille], que j'avais fait tomber dans le puits.";
		découvrir la trouvaille.

Before throwing la marmite at le puits:
	si la marmite est une partie de la corde:
		essayer d' inserting la marmite into le puits;
		arrêter l'action.

Section 2 - Eau

L' eau (f) est une chose.
L' indefinite article est "de l[']".
La description est "De l'eau que j'avais remontée du puits.".

Instead of taking l' eau, dire "Il valait mieux la laisser dans [le porteur de l' eau], ou elle se serait répandue par terre.".

Instead of drinking l' eau, dire "Je n'avais aucune idée quant à la salubrité de l'eau, mais j'avais soif. J'en bus donc un peu[_]; elle paraissait bonne. J'en donnai donc à mon frère.".

Instead of inserting l' eau into le mortier:
	maintenant l' eau est dans le mortier;
	dire "Je versai l'eau dans le mortier.".

Chapter 2 - Jardin ouest

Le jardin-ouest est à l'ouest du jardin. "Nous étions maintenant dans le recoin le plus sombre du jardin. Ici, la lune était invisible, cachée derrière le bâtiment, et les plantes, encore plus touffues. Revenir à l'est était sûrement plus judicieux que d'entrer dans cette remise délabrée.".
Le printed name est "Au fond du jardin".
Le jardin-ouest est privately-named.

Section 1 - Remise

La remise-décor est une chose décorative privately-named dans le jardin-ouest.
Le printed name est "remise".
La description est "Une construction délabrée, envahie par la végétation. Elle n'était pas rassurante, et je préférais ne pas m'y aventurer tant que cela n'aurait pas été nécessaire.".

Comprendre "remise", "construction", "batiment" ou "batisse" comme la remise-décor.

Instead of entering la remise-décor, essayer de going dedans.

Section 2 - Caisse

La caisse est un entrable poussable entre endroits support. La caisse est dans le jardin-ouest.
La description est "Une grosse caisse en bois pourri trônait ici.".

[À FAIRE : son pour pousser la caisse]

Instead of taking la caisse, dire "Elle était bien trop lourde.".
Instead of pushing la caisse, dire "Je devais plutôt la pousser vers une direction.".

Section 3 - Sorbier

Le sorbier est une chose fixée sur place privately-named dans le jardin-ouest.
Le printed name est "arbuste".
La description est "[parmi]Cette plante, je connaissais son nom… C'était un… sorbier. Oui, il s'agissait bien d'un sorbier. Pourtant, j'étais certaine qu'il y avait quelque chose dont j'étais censée me souvenir[ou]Un sorbier se tenait là, paraissant protéger le bâtiment de ses branches[stoppant].".

Comprendre "arbuste" comme le sorbier.
Understand "sorbier" as le sorbier when we have examined le sorbier.

Carry out examining le sorbier pour la première fois:
	maintenant le printed name du sorbier est "sorbier".

Section 4 - Branches

Les branches (f) sont une partie du sorbier.
La description est "[si les branches sont une partie du sorbier]Les branches de cet arbre étaient magnifiques[sinon si brûlés]Les branches produisait un étrange feu qui rassurait[sinon]J'avais cassé ces branches, car je savais qu'elles pouvaient servir à quelque chose, bien que je ne me rappelasse plus quoi[fin si].".
Les branches peuvent être brûlés. Les branches sont non brûlés.

Comprendre "branche" comme les branches.

Instead of taking les branches when les branches sont une partie du sorbier:
	maintenant les branches sont dans la location;
	dire "Je cassai les branches du sorbier.";
	découvrir les branches.

Instead of taking les brûlés branches, dire "Elles brûlaient doucement, mais ce n'était pas une raison de tenter de les prendre.".

Instead of burning les branches when le player porte l' allumette:
	si les branches sont dans la cheminée:
		dire "Je mis le feu aux branches, et un petit feu s'éleva. Rien de grandiose, mais on faisait avec ce que l'on pouvait.";
		maintenant les branches sont brûlés;
		maintenant l' ambiance du salon est le sound hearth;
		loop l' ambiance of la location on background;
	sinon si la location est dans l' extérieur:
		dire "Une idée folle me vint soudain. Je mis le feu aux branches, que je lâchai. Le feu se répandit rapidement, brûlant ces ronces qui nous sommaient de partir. Je souris à les voir ainsi mourir.";
		attendre une touche;
		dire "Mon frère me regardait, mon rictus et moi, avec des yeux terrorisés. Le voyant ainsi, je repris mes esprits[_]; étais-je devenue folle[_]?";
		attendre une touche;
		dire "Malheureusement, les flammes ne se contentèrent pas des plantes.";
		attendre une touche;
		fin de l'histoire en disant "C'était la fin…";
	sinon si la location est dans l' intérieur:
		dire "J'allumai une branche, qui m'échappa des mains. Rapidement, toute la demeure prit feu. Était-ce ainsi que tout cela devait finir[_]?";
		attendre une touche;
		dire "Je regardai une dernière fois mon frère, qui me rendit le regard, avant que les flammes nous engloutissent…[à la ligne]";
		attendre une touche;
		fin de l'histoire en disant "C'était la fin…";
	sinon si la location est la remise:
		dire "Je mis le feu à une branche et la lançai dans le tas d'outils malveillants. Ils s'enflammèrent.";
		attendre une touche;
		dire "Nous fûmes bientôt pris au piège. Mon frère me lança un dernier regard terrifié.";
		attendre une touche;
		fin de l'histoire en disant "C'était la fin…";
	sinon si la location est la chapelle:
		dire "Je ne pouvais pas mettre le feu dans ce lieu saint.".

Instead of switching on les branches, essayer de burning les branches.

Section 5 - Plantes

Les plantes sont des choses décoratives dans le jardin-ouest.
La description est "Des plantes touffues nous empêchaient d'aller plus loin.".
Le sound est "Elles restaient immobiles et silencieuse. Menaçantes.".

Comprendre "plante" comme les plantes.

Instead of cutting les plantes when le player porte le couteau:
	dire "Toutes ces plantes voulaient nous engloutir, j'en étais certaine. Je pris le couteau et commençai à les couper comme une démente. Heureusement, mon frère me fit comprendre que ça ne servait à rien, et j'arrêtai.";
	faire passer 5 minutes.

Instead of pulling les plantes:
	dire "Toutes ces plantes voulaient nous engloutir, j'en étais certaine. Je commençai à les arracher comme une forcenée. Heureusement, mon frère me fit comprendre que ça ne servait à rien, et j'arrêtai.";
	faire passer 5 minutes.

Chapter 3 - Remise

La remise est à l'intérieur du jardin-ouest. "Dans la pénombre, on pouvait distinguer de nombreux outils, entreposés un peu partout. Ils étaient réellement effrayants, on aurait pu se croire dans une salle de torture.".
Le printed name est "Dans la remise".
La remise est sombre.

For printing the description of a dark room when la location est la remise:
	dire "L'obscurité était effrayante ici. Je n'osais pas trop bouger, de peur de heurter un outil dangereux. Je ne pouvais que sortir.".

For printing the announcement of darkness when la location est la remise:
	dire "Le noir revint masquer tous les danger de cet endroit.".

Section 1 - Pièges

Les pièges sont une chose privately-named dans la remise. "Des pièges à loups étaient accrochés en hauteur sur le mur.".
Le printed name est "pièges à loups".
La description est "D'effrayantes mâchoires métalliques qui ne demandaient qu'à être nourries, j'en étais certaine.".
Le sound est "Je préférais ne jamais savoir quel bruit feraient les pièges lorsqu'ils se refermaient.".

Comprendre "pieges", "piege" ou "pieges/piege a loups/loup" comme les pièges.

[Quand on prend les pièges]
Instead of taking les pièges when les pièges ne sont pas manipulés et le player n'est pas sur la caisse:
	dire "Je tendis la main pour essayer d'attraper un piège. Il était trop haut. Déterminée, je sautai alors et réussit à toucher l'objet métallique. Je retombai au sol, mais ne fut pas la seule[_]: je n'eus que le temps de voir les mâchoires se refermer…";
	attendre une touche;
	play the sound jaws on foreground;
	attendre une touche;
	fin de l'histoire en disant "C'était la fin…".

After taking les non manipulés pièges, dire "Grâce à la caisse, je pus prendre les pièges sans risques.".

[Quand on pose les pièges dans la forêt]
Instead of dropping les pièges dans la forêt:
	retirer les pièges du jeu;
	dire "Malgré ma peur, j'indiquai à mon frère de rester à l'entrée du jardin et m'enfonçai dans la forêt, posant les pièges à loups devant le domaine. Je revins bientôt, espérant qu'ils allaient [italique]le[romain] repousser.";
	faire passer 20 minutes.

Section 2 - Outils

Les outils sont une chose décorative dans la remise.
La description est "De nombreux outils de jardinage étaient entreposés ici. Cependant, ils étaient tous en mauvais état, ou cassés.".

Instead of taking les outils, dire "Ils nous auraient été inutiles.".

Instead of searching les outils:
	dire "Malgré tout, je regardai si rien ne pouvait nous aider. Je perdis mon temps.";
	faire passer 4 minutes.

Chapter 4 - Jardin est

Le jardin-est est à l'est du jardin. "Cette partie du jardin était moins sauvage et les plantes avaient consenti à nous laisser passer. Ici, une petite chapelle de pierre se dressait, et sa seule présence parvenait à nous apaiser, ainsi que les épineux, qui n'osaient pas s'approcher d'un lieu si sacré. Ça aurait été une bonne idée d'entrer dans le bâtiment, ou de retourner sur nos pas à l'ouest.".
Le printed name est "Devant la chapelle".
Le jardin-est est privately-named

Section 1 - Chapelle

La chapelle-décor est une chose décorative privately-named dans le jardin-est.
Le printed name est "chapelle".
La description est "Une petite bâtisse de pierre, qui dégageait une aura de sûreté. Elle ne semblait pas du tout sinistre malgré les fissures qui la parcouraient.".
Le sound est "Contrairement au silence inquiétant qui régnait dans le jardin, un calme apaisant et invitant émanait de la chapelle.".

Comprendre "chapelle", "batisse" ou "batiment" comme la chapelle-décor.

Instead of entering la chapelle-décor, essayer de going dedans.

Section 2 - Fleurs

Les fleurs (f) sont une chose dans le jardin-est. "De petites fleurs blanches poussaient dans un coin.".
La description est "[si manipulé]J'avais cueilli ces fleurs blanches dans le jardin. Elles n'avaient pas semblé être là par hasard, et je m'étais dit qu'elles pouvaient peut-être servir à quelque chose[sinon]Ces fleurs délicates ressemblaient à des intrus acculés par les autres plantes alentour. Elles dégageaient quelque chose de surnaturel[fin si].".

Comprendre "fleur", "plante" ou "plantes" comme les fleurs.

After taking les fleurs pour la première fois, dire "Je cueillis les fleurs doucement, afin de ne pas les abîmer.".

Instead of pulling les fleurs, essayer de taking les fleurs.

After burning les fleurs, dire "Leur pureté m'empêchait de leur faire du mal.".

Section 3 - Pétales

Les pétales sont une partie des fleurs.
La description est "De beaux pétales immaculés. Ils semblaient rayonner.".
Les pétales sont privately-named.

Comprendre "petales" ou "petale" comme les pétales.

Instead of taking les pétales when les pétales sont une partie des fleurs:
	dire "Je détachai délicatement les pétales de chacune des fleurs et me débarrassais de ce qui restait de celles-ci.";
	maintenant le player porte les pétales;
	retirer les fleurs du jeu.

Instead of pulling les pétales when les pétales sont une partie des fleurs, essayer de taking les pétales.

Section 4 - Pelle

La pelle est une chose dans le jardin-est.
La description est "Une vieille pelle usée, mais encore utilisable. Avait-elle servi à jardiner, ou à creuser des tombes[_]?".

Chapter 5 - Chapelle

La chapelle est à l'intérieur du jardin-est. "Malgré la nuit au-dehors, l'intérieur de ce lieu sacré rayonnait. Ici, le silence n'était pas pesant, il régnait au contraire un calme apaisant. C'était la première fois depuis longtemps que je n'avais pas ressenti pareille sensation.".
Le printed name est "Dans la chapelle".
Le sound est "Le silence régnait ici, mais un silence apaisant, religieux.".

Section 1 - Autel

L' autel est un support. L' autel est dans la chapelle.
La description est "Un autel sobre en pierre. En regardant de plus près, on pouvait voir quelques inscriptions gravées[_]: H[unicode 298]C R[unicode 274]M P[unicode 332]NIS, SANCTA ERIT. Que pouvait bien signifier cette phrase[_]?".

Carry out putting quelque chose on the autel:
	maintenant le noun est sanctifié.

Section 2 - Crucifix

Le crucifix est une chose dans la chapelle.
La description est "[si vissé]Un crucifix était accroché au-dessus de l'autel[sinon]Une belle croix de bois sombre. La garder ne pouvait pas faire de mal[fin si].".
Le crucifix peut être vissé. Le crucifix est vissé.

Comprendre "croix" comme le crucifix.

Instead of taking le vissé crucifix, dire "Impossible, il était solidement vissé au mur.".

Instead of turning le vissé crucifix when le player ne porte pas le tournevis:
	dire "Je n'avais rien pour le dévisser.".

Carry out turning le vissé crucifix:
	maintenant le crucifix n'est pas vissé;
	essayer silencieusement de taking le crucifix.

After turning le crucifix, dire "Je dévissai le crucifix du mur, et le pris.".

After burning le crucifix, dire "Je ne pouvais pas brûler un symbole sacré.".

Chapter 6 - Forêt

La forêt est au sud du portail. "Nous voilà revenus à l'endroit que nous désirions le moins revoir. Tout était étrangement silencieux, comme si la forêt retenait sa respiration avant quelque danger. Je ne savais pas ce qui m'avait pris d'entraîner mon frère ici, mais il était trop tard pour les regrets. Il fallait retourner au nord."
Le printed name est "Au cœur de la forêt".
La forêt est privately-named.
La forêt can be sanglant. La forêt est non sanglant.

Instead of going nulle part from la forêt:
	dire "Il n'était pas sage d'aller plus loin et, de toute façon, nous n'en avions certainement pas le courage.".

Instead of going to la forêt when les arbres sont brûlés, dire "L'incendie devenait violent. Mieux valait rester ici.".

Before doing something when la location est la forêt et la location n'est pas sanglant during le saignement:
	maintenant la forêt est sanglant;
	play the sound nature steps on foreground;
	dire "Avant de continuer, une pensée me vint[_]: peut-être que si je répandais du sang dans la forêt, cela [italique]l[romain][']occuperait[_]? Je pris mon courage à deux mains et m'enfonçait entre les arbres. Je m'enfonçais assez loin, puis je revins. Cela fonctionnerait-il[_]?";
	attendre une touche;
	faire passer 7 minutes.

Section 1 - Arbres

Les arbres (m) sont une chose décorative dans la forêt.
La description est "[si brûlés]Les arbres étaient en feu et un doux crépitement nous invitait à nous jeter dans l'incendie, pour en finir une fois pour toutes… Non, je devais me ressaisir[_]![sinon]Pas une feuille ne bruissait, il régnait un silence de mort. Les arbres semblaient former une prison nous empêchant de nous échapper. Mais cette cage ne [italique]l[romain][']empêcherait pas de venir ici.[fin si]".
Le sound est "[si brûlés]Un doux crépitement nous invitait à rejoindre les flammes, pour que tout soient enfin terminé… Que pensais-je[_]? Nous devions vite quitter cet endroit[_]![sinon][fin si]".
Les arbres peuvent être brûlés. Les arbres sont non brûlés.

Comprendre "arbre", "branches" ou "branche" comme les arbres.

Instead of taking les arbres, dire "Les branches étaient bien trop hautes.".

Instead of dropping l' allumette when la location est la forêt, essayer de burning les arbres.

Instead of burning les non brûlés arbres when le player porte l' allumette:
	maintenant les arbres sont brûlés;
	retirer l' allumette du jeu;
	le joueur meurt du feu in three minutes from now;
	maintenant le sound de la forêt est "Un doux crépitement qui contrastait avec la violence de l'incendie, le rendant encore plus dangereux.";
	maintenant l' ambiance de la forêt est le sound forest fire;
	loop l' ambiance de la location on background;
	dire "Dans l'espoir de [italique]le[romain] ralentir, je lâchai l'allumette enflammée. Le feu se propageait vite, il ne fallait pas rester ici.".

Instead of burning les brûlés arbres when le player porte l' allumette:
	dire "Le feu dévorait déjà la forêt. Je n'avais pas envie de le nourrir davantage.".

At the time when le joueur meurt du feu:
	si la location est la forêt:
		dire "La chaleur devenait insupportable et les flammes nous encerclaient, de plus en plus proches. Bientôt, elles nous atteignirent.";
		attendre une touche;
		fin de l'histoire en disant "C'était la fin…".

Section 2 - Portail

Le portail est au sud du jardin. Le portail est une ouverte verrouillable porte.
La description est "Un haut portail [si ouvert]ouvert[sinon]fermé[fin si] et rouillé se dressait, frontière entre la forêt et le jardin. [si la location est le jardin]S'aventurer au-delà signifiait une mort certaine, j'en étais convaincue[sinon]Il était préférable de rentrer. Dans la forêt, tout pouvait arriver[fin si].".
Le sound est "Il grinçait un peu.". [TODO: SFX]

Part 3 - Premier étage

Chapter 1 - Chambre

La chambre est à l'ouest du couloir. "La lumière blafarde d'une lune gibbeuse filtrait par la vitre ébréchée, éclairant comme elle pouvait la pièce. Nous avions trouvé refuge ici, dans cette chambre, mais la peur, plus que le grabat, nous avait empêchés de dormir. Maintenant… Maintenant, que faire, justement[_]?[à la ligne]La porte était à l'est.".
Le printed name est "Dans une chambre".

Section 1 - Nevena

Nevena est une femme dans la chambre.
La description est "J'avais treize ans et, par ma faute, [italique]il[romain] nous poursuivait. Je devais trouver un moyen de nous sauver. Si nous ne pouvions pas nous en sortir tous les deux, je ferais en sorte qu'il puisse s'en tirer[_]; il n'avait que huit ans. Après tout, n'était-ce pas le rôle d'une grande sœur[_]?".
Le sound est "Je m'immobiliai un instant et me concentrai sur ma respiration. Mon cœur se calma un peu ; nous pouvions continuer.".
Le carrying capacity est 7.
Nevena peut être dessiné. Nevena est non dessiné.
[SE COUPER]
Nevena can be coupé. Nevena is not coupé.
Le player est Nevena.

Section 2 - Frère

Le frère est une homme privately-named dans la chambre. "Mon frère se tenait là, à mes côtés.".
L' indefinite article est "mon".
Le printed name est "frère".
La description est "Mon frère était de cinq ans mon cadet, et si vulnérable. Je devais nous sortir de là[si le frère porte quelque chose].[à la ligne]Il portait [une liste des choses carried by le frère][fin si].".
Le sound est "Il ne faisait pas de bruit. Le silence était sûrement préférable aux paroles inutiles.".
Le carrying capacity du frère est 5.
Le frère peut être dessiné. Le frère est non dessiné.

Comprendre "mon frère" ou "frere" comme le frère.

Carry out going to un endroit (called destination):
	maintenant le frère est dans la destination.

[Quand on parle au frère]
Instead of talking to le frère, dire "J'avais une foule de paroles réconfortantes à lui dire, mais toutes auraient sonné creux. Non, il valait mieux se partager les choses découvertes[si le frère porte quelque chose] ou lui demander de me montrer ce qu'il portait[fin si].".

Instead of answering le frère that, essayer de talking to le frère.
Instead of telling le frère about, essayer de talking to le frère.
Instead of asking le frère about, essayer de talking to le frère.

[Quand on frappe le frère]
Instead of attacking le frère, dire "Je ne pouvais lui faire de mal. Pas après tout ce qu'il avait enduré.".


[Pour pouvoir donner des ordres au frère]
Persuasion rule for asking le frère to try showing: persuasion succeeds.
Persuasion rule for asking le frère to try giving: persuasion succeeds.
Persuasion rule for asking le frère to try taking: persuasion succeeds.
The block giving rule is not listed in the check giving it to rulebook.

Section 3 - Grabat

Le grabat est un entrable support. Le grabat est dans la chambre.
La description est "Un antique lit supportant un vestige de matelas. Nous nous étions crus en sécurité, blottis sous la couverture miteuse. Mais le serions-nous vraiment un jour[_]?".
Le sound est "Il avait émis de piteux grincements aux moindres mouvements que nous avions faits.".

Comprendre "lit" ou "couchette" comme le grabat.

Instead of entering or climbing le grabat, dire "Inutile. Jamais nous n'aurions pu dormir.".

Instead of looking under le grabat:
	dire "Je me penchai et inspectai le sol sous le lit. Il y avait peu de lumière, mais on comprenait qu'il n'y avait que de la poussière.".

Section 4 - Matelas

Le matelas est une chose décorative sur le grabat.
La description est "Il était si vieux et si mince qu'il était difficile de véritablement l'appeler matelas. Mais ce n'était pas cela qui nous avait empêchés de dormir.".
Le sound est "Le matelas était si usé qu'il n'émettais plus aucun bruit lorsque l'on s'allongeais dessus.".

Instead of entering or climbing le matelas, essayer d' entering le grabat.

Instead of looking under le matelas, dire "Je soulevai aisément le matelas, afin de voir si son ancien propriétaire y avait caché quelque chose dessous. Il n'y avait rien.".

Section 5 - Couverture

[La couverture en tant que tel]
La couverture est une chose mettable sur le grabat. 
La description est "Une vieille couverture usée jusqu'à la corde. Elle ne pouvait absolument plus servir, mais nous nous y étions blottis pour l'illusion de sûreté qu'elle apportait.".
Le sound est "Je fis bruire la couverture. Elle ne protégeais plus guère de grand-chose.".

Instead of cutting la couverture:
	play the sound tear cloth on foreground;
	dire "Je déchirai la couverture, et me retrouvai avec de nombreux lambeaux de tissu.";
	maintenant les lambeaux sont dans le porteur de la couverture;
	retirer la couverture du jeu;
	faire passer 2 minutes.

[Les lambeaux]
Les lambeaux sont une chose.
La description est "J'avais obtenu ces morceaux de tissu en déchirant la couverture. Était-ce une bonne idée[_]? Pourrais-je en tirer quelque chose[_]?".

Comprendre "lambeau" ou "tissu" comme les lambeaux.

Instead of tying les lambeaux to les lambeaux:
	dire "Je nouai les morceaux de couverture entre eux, et m'en fit une sorte de corde. Elle n'était pas solide, et j'espérais qu'elle n'allait pas céder quand je l'utiliserais.";
	maintenant la corde est dans le porteur des lambeaux;
	retirer les lambeaux du jeu;
	faire passer 3 minutes.

[La corde]
La corde est une chose.
La description est "Des morceaux usés de couverture, noués entre eux et formant une corde précaire[si la marmite est une partie de la corde]. Une marmite y était attachée[fin si].".

After printing the name of la corde when la marmite est une partie de la corde, dire " à laquelle était attachée [la marmite]".

Section 6 - Table de nuit

La table de nuit (f) est un support. La table de nuit est dans la chambre.
La description est "Une petite table de nuit, à côté du lit. Elle n'avait pas l'air très solide. Elle comportait un petit tiroir.".

Instead of looking under la table de nuit, dire "Il n'y avait rien ici.".

Section 7 - Tiroir

Le tiroir de nuit est un contenant fermé ouvrable privately-named.
Le tiroir de nuit est une partie de la table de nuit.
Le printed name est "tiroir".
La description est "Un petit tiroir pourri était encastré dans la table.".
Le sound est "Le tiroir faisait le bruit habituel d'un tiroir quand on le tirait.".

Comprendre "tiroir" comme le tiroir de nuit.

Instead of pushing le tiroir de nuit, essayer de closing le tiroir de nuit.
Instead of pulling le tiroir de nuit, essayer d' opening le tiroir de nuit.

Carry out opening le tiroir de nuit:
	play the sound draw on foreground.

Carry out closing le tiroir de nuit:
	play the sound draw on foreground.

Section 8 - Lampe

La lampe est un appareil sur la table de nuit.
La description est "Une lampe à gaz, que j'avais trouvée lorsque nous étions entrés dans la maison. Elle était encore en état de marche.".

[Quand on allume la lampe]
Before switching on l' éteinte lampe:
	si le player ne porte pas l' allumette:
		dire "Je n'avais rien pour l'allumer.";
		arrêter l'action.

Carry out switching on la lampe:
	maintenant la lampe est lumineuse;

After switching on la lampe:
	retirer l' allumette du jeu;
	dire "J'allumai la lampe avec l'allumette et me débarrassai de cette dernière.".

[Quand on éteint la lampe]
Carry out switching off la lampe:
	maintenant la lampe n'est pas lumineuse.
	
Section 9 - Boîte d'allumettes

La boîte d'allumettes est une chose privately-named dans le tiroir de nuit.
La description est "Une boîte d'allumettes à moitié écrasée. Je l'avais prise avant notre fuite dans la forêt.[saut de paragraphe]Elle [si le reste de la boîte d'allumettes est 1]contenait une allumette[sinon si le reste de la boîte d'allumettes > 1]contenait [reste de la boîte d'allumettes en lettres] allumettes[sinon]était vide[fin si].".
Le sound est "Je secouai la boîte d'allumettes[si le reste est 0] mais elle demeura silencieuse[sinon]. Un petit bruit s'en échappa[fin si].". [TODO: SFX]
La boîte d'allumettes has un nombre called reste. Le reste de la boîte d'allumettes est 6.

Comprendre "boite" ou "d' allumettes" comme la boîte d'allumettes.

[TODO: Ajouter l'action de secouer.]

Instead of inserting quelque chose into la boîte d'allumettes when le noun n'est pas l' allumette, say "C'était bien trop gros pour entrer.".

Instead of opening la boîte d'allumettes:
	si la boîte d'allumettes est mouillé:
		dire "Le séjour de la boîte d'allumettes dans l'eau avait rendu toutes les allumettes inutilisables.";
	sinon:
		si le player porte l' allumette:
			dire "J'avais déjà sorti une allumette[_]; je n'en avais pas besoin d'une autre pour le moment.";
		sinon si le reste de la boîte d'allumettes est 0:
			dire "J'avais déjà utilisé toutes les allumettes.";
		sinon:
			play the sound match on foreground;
			décrémenter le reste de la boîte d'allumettes;
			maintenant le player porte l' allumette;
			dire "Je tirai une allumette de la boîte et réussis à l'allumer. Une flamme rassurante apparut.";
			établir les pronoms pour l' allumette.


[Cet objet sert à pouvoir taper « prendre allumette » à la place de « ouvrir boîte ». Utile car on veut que « prendre boîte » fasse prendre la boîte d'allumette normalement.]
L' allumette-boîte (f) est une partie de la boîte d'allumettes. L' allumette-boîte est privately-named.

Comprendre "allumette" comme l' allumette-boîte.

[Toutes les actions sauf celle de prendre sur cet objet sont redirigées vers la boîte d'allumettes.]
Before doing something other than taking when la current action implique l' allumette-boîte:
	si l' allumette-boîte est le noun:
		maintenant le noun est la boîte d'allumettes;
	si l' allumette-boîte est le second noun:
		maintenant le second noun est la boîte d'allumettes;
	essayer la current action instead.

[On redirige l'action de prendre cet objet vers la boîte d'allumettes.]
Instead of taking l' allumette-boîte:
	essayer d' opening la boîte d'allumettes.

Section 10 - Les allumettes

L' allumette (f) est une chose lumineuse.
La description est "Les allumettes étaient humides. Que celle-là se fût allumée était un miracle.".

Instead of switching on l' allumette:
	dire "Je l'avais déjà allumée.".

Instead of switching off l' allumette:
	retirer l' allumette du jeu;
	dire "Je soufflai l'allumette et me débarrassai du morceau de bois brûlé.".

Every turn when l' allumette est on-stage:
	si une chance de 1 sur 5 réussit:
		si l' allumette est visible:
			dire "La flamme mourut[si l' allumette était off-stage] malheureusement presque aussitôt[fin si], faisant disparaître sa chaude lumière. Je me débarrassai alors du morceau de bois brûlé.";
		retirer l' allumette du jeu.

Does the player mean doing something with l' allumette: it is very likely.
For clarifying the parser's choice of l' allumette: do nothing.

Chapter 2 - Couloir

Le couloir est au nord de l' escalier. "Un sombre couloir se déroulait devant nous. Les longues ombres des carreaux de la fenêtre formaient une grille sur le tapis poussiéreux. Par l'ouverture, deux yeux incandescents nous fixaient sévèrement. Par bonheur, ce n'était pas [italique]lui.[romain] Du moins, je l'espérais…[à la ligne]La chambre était à l'ouest, le hall au sud. Il y avait également une autre porte, mais je n'était pas parvenue à l'ouvrir.".
Le printed name est "Le long d'un sombre couloir".
Le sound est "Un hululement insistant semblait vouloir nous avertir. Il valait mieux partir.".
L' ambiance est le sound owl.

Section 1 - Tapis

Le tapis est une chose décorative dans le couloir.
La description est "Un tapis rongé par les insectes s'étendait au sol. Il empêchait le parquet de grincer, mais j'aurais juré entendre ces bestioles grouiller à l'intérieur.".
Le sound est "Il étouffait le bruit de nos pas.".

Instead of looking under le tapis pour la première fois:
	say "Je m'accroupis et soulevais le lourd tapis comme je le pus. Je regardai dessous et…[à la ligne]";
	attendre une touche;
	dire "Un énorme mille-pattes surgit d'un interstice du plancher. Je lâchai le pan du tapis et reculai prestement, tandis que mon frère se plaquait contre le mur. Une fois le myriapode parti, je repris prudemment mon inspection[_]; une clef était cachée là[_]! Je la pris et remis le tapis en place, ce qui souleva un nuage de poussière.";
	attendre une touche;
	dire "Une clef… Que pouvait-elle bien ouvrir[_]?";
	attendre une touche;
	découvrir la clef dorée.

Instead of looking under le tapis:
	dire "J'avais cherché ici, et je ne tenais pas à refaire une mauvaise rencontre.".

Instead of taking le tapis when la player's command contient "soulever", essayer de looking under le tapis.

Section 2 - Clef dorée

La clef dorée est une chose privately-named.
Le printed name est "petite clef dorée".
La description est "Cette petite clef dorée avait été cachée sous le tapis du couloir. [si le tiroir de travail a été déverrouillé]Elle ouvrait le tiroir du bureau, dans le cabinet de travail.[sinon]Que permettait-elle d'ouvrir[_]?[fin si]".
Le sound est "Elle émettait un petit son clair quand on la frappait.".
La clef dorée ouvre le tiroir de travail.

Comprendre "petite", "clef", "cle", "doree", "dore" comme la clef dorée.

Section 3 - Yeux

Les yeux sont un animal décoratif dans le couloir.
L' indefinite article des yeux est "les".
La description est "C'était un hibou. C'était… sûrement un hibou. C'était… peut-être… un hibou[_]? Était-ce vraiment un hibou[_]? Je… je voulais partir d'ici au plus vite.".
Le sound est "Les yeux nous hululaient de partir. C'était ce que je pensait comprendre.".

Comprendre "oeil" ou "hibou" comme les yeux.

Instead of talking to les yeux, dire "«[_]Ouuuuco, Ouuuuco…[_]» Ce hibou avait une étrange manière de hululer. Je devais dérailler.".

Instead of doing something with les yeux:
	si we are examining ou we are listening ou we are praying for, ne pas prendre de décision;
	dire "Je ne tenais pas à embêter l'animal.".

Section 4 - Porte

La porte-chambre est une non décrite verrouillable verrouillée privately-named porte.
La porte-chambre est à l'est du couloir.
Le printed name est "porte".
La description est "Lorsque nous étions arrivés dans la demeure, j'avais essayé de trouver un endroit qui n'était pas trop inquiétant pour passer la nuit. C'est à ce moment-là que j'avais su que cette porte était verrouillée."

Comprendre "porte" comme la porte-chambre.

Instead of opening la porte-chambre, dire "Je tournai de nouveau la poignée, mais il n'y avait rien à faire.".

Instead of unlocking la porte-chambre with quelque chose:
	si le second noun est la clef dorée ou le second noun est la clef rouillée, dire "J'essayai d'ouvrir la porte avec [le second noun], mais elle ne correspondait pas à la serrure.";
	sinon dire "Utiliser une clef aurait été plus judicieux.".

Instead of attacking la porte-chambre, dire "Je frappai la porte, et ne réussit qu'à me faire mal à la main.".

Chapter 3 - Escalier

L' escalier est au-dessus du hall. "Nous étions à l'étage, et seule une balustrade pourrie nous empêchait de chuter. Un tic-tac régulier se faisait entendre, mais je ne parvenais pas à distinguer l'horloge dans la pénombre. Il n'y avait pas grand-chose d'autre ici.[à la ligne]Je pouvais descendre dans le hall, le couloir menant à la chambre était au nord et deux portes se dressaient de chaque côté du palier[_]: à l'ouest[si la bibliothèque est visitée], la bibliothèque,[fin si] et à l'est[si le cabinet est visité], le cabinet de travail[fin si].".
Le printed name est "Au sommet de l'escalier".
Le sound est "Le silence pesant était ponctué d'un tic-tac encore plus inquiétant.".
L' ambiance est le sound tick.

Carry out going from l' escalier to le hall:
	play the sound stairs on foreground.

Section 1 - Balustrade

La balustrade est une chose décorative dans l' escalier.
La description est "Elle semblait fragile et je préférais ne pas avoir à éprouver sa solidité.".

Chapter 4 - Bibliothèque

La bibliothèque est à l'ouest de l' escalier. "La pièce était emplie de livres, trônant fièrement sur leurs étagères depuis ce qui semblait des lustres. Je me demandai si ces gardiens du savoir avaient survécu au temps et aux insectes. Il n'y avait qu'un moyen de le savoir, mais j'ignorais si cela nous aiderait.".
Le printed name est "Entourée d'étagères".
La bibliothèque est privately-named.

Section 1 - Étagères

Les étagères (f) sont une chose décorative privately-named dans la bibliothèque.
La description est "Des meubles anciens faits de bois massif nous dominaient. La pièce était vaste et ils étaient nombreux, jetant leur ombre dans les sombres recoins de la pièce. S'il y avait quelque chose à faire ici, il valait mieux que je le fisse rapidement.".

Comprendre "etagere" ou "etageres" comme les étagères.

Instead of searching les étagères pour la première fois:
	dire "Je pris un livre sur l'étagère la plus à gauche, puis demandai à mon frère d'en faire autant sur celle de droite. Peut-être cette pièce renfermait une information capitale[_]? La longue fouille commença…[à la ligne]";
	attendre une touche;
	play the sound books on foreground;
	dire "De longues minutes s'écoulèrent. Soudain, un livre attira mon attention par son dos moins usé. Je me saisis de lui et lut le titre, qui était écrit en lettres d'or terni.";
	attendre une touche;
	say "[italique]Monstres, démons et autres créatures du folklore local[romain]. Peut-être…[à la ligne]";
	attendre une touche;
	découvrir le volume;
	établir les pronoms pour le volume;
	faire passer 20 minutes.

Instead of searching les étagères:
	dire "Le livre que j'avais trouvé semblait contenir les informations dont j'avais besoin, et nous n'avions plus le temps de chercher quoi que se soit.".

Section 2 - Livres

Les livres sont des choses décoratives dans la bibliothèque.
La description est "De nombreux livres trônaient sur les étagères, mais ne rendaient pas ces dernières moins inquiétantes pour autant.".

Comprendre "livre" comme les livres.

Instead of searching les livres, essayer de searching les étagères.

Does the player mean doing something with les livres: it is very unlikely.

Section 3 - Volume

Le volume est une chose.
Le printed name est "volume des [italique]Monstres, démons et autres créatures du folklore local[romain]".
La description est "Un livre qui, malgré sa vieillesse, avait su garder une étrange beauté. Sous le titre, une illustration représentait une silhouette noire portant deux yeux incandescents. Sur le dos, une mystérieuse figure cabalistique avait été gravée sur le cuir. Ce livre me donnait des frissons.".

Comprendre "livre" comme le volume.

Instead of reading le volume pour la première fois:
	play sound book on foreground;
	dire "J'ouvris le livre et le feuilletai rapidement. Je cherchai une information quelconque, n'importe quoi qui pouvait [italique]le[romain] tenir à distance… Là, enfin quelque chose[_]!";
	attendre une touche;
	dire "«[_][italique]Il[romain] arrive à minuit… Minuit… Survivre jusqu'au lever du Soleil… Les symboles sacrés [italique]le[romain] font fuir… Sacrés…[_]»[à la ligne]";
	attendre une touche;
	dire "Il n'y avait plus de temps à perdre[_]; si elles étaient vraies, ces informations nous seraient très utiles.";
	attendre une touche;
	faire passer 10 minutes.

Instead of consulting le volume about a topic, essayer de reading le volume.
Instead of opening le volume, essayer de reading le volume.
Instead of searching le volume, essayer de reading le volume.

Instead of reading le volume:
	dire "Je n'avais plus le temps de le relire. Cependant, je me souvenais qu['][italique]il[romain] arriverait à minuit si rien n'était fait, et que les symboles sacrés [italique]le[romain] faisaient fuir.".

Instead of examining le mouillé volume, dire message mouillé.
Instead of reading le mouillé volume, dire message mouillé.

Chapter 5 - Cabinet

Le cabinet est à l'est de l' escalier. "Cette pièce devait le lieu de travail de l'ancien résident. Tout semblait si bien rangé, on aurait pu croire que quelqu'un l'avait utilisé la veille. Pourtant, la maison devait être vide depuis des lustres. C'était comme si on avait su que l'on reviendrait à cet endroit.".
Le printed name est "À l'intérieur du cabinet de travail".
Le cabinet est sombre.

For printing the description of a dark room when la location est le cabinet:
	dire "Il faisait sombre ici, les fenêtres avaient été condamnées. Je ne pouvais pas distinguer grand-chose.".

For printing the announcement of darkness when la location est cabinet:
	dire "La lumière disparut, et l'obscurité revint.".

Section 1 - Bureau

Le bureau est un support. Le bureau est dans le cabinet.
La description est "Un beau meuble portant des traces de vernis. Il semblait fait d'un bois précieux qui avait résisté au temps. Il possédait un tiroir sur la gauche.".

Comprendre "table" comme le bureau.

Instead of looking under le bureau:
	dire "Je jetai un coup d[']œil sous le bureau. Rien hormis la poussière.";
	faire passer 2 minutes.

Section 2 - Tiroir de travail

Le tiroir de travail est un contenant fermé ouvrable verrouillé verrouillable privately-named.
Les tiroir de travail est une partie du bureau.
Le printed name est "tiroir".
La description est "Un beau tiroir, quoique quelque peu abîmé, se trouvait sur la gauche du bureau. Il avait une poignée ouvragée, bien que ternie, et une serrure.".
Le sound est "[Si we have opened le tiroir de travail]Le bureau était d'excellente facture et le tiroir glissait sans difficulté, faisant son bruit habituel[sinon]Je frappai le tiroir afin d'essayer de déterminer s'il contenait quelque chose, en vain[fin si].".

Comprendre "tiroir" comme le tiroir de travail.

After unlocking le tiroir de travail with la clef dorée pour la première fois:
	dire "La clef entra parfaitement dans la serrure, et je déverrouillai le tiroir.".

Instead of pushing le tiroir de travail, essayer de closing le tiroir de travail.
Instead of pulling le tiroir de travail, essayer d' opening le tiroir de travail.

Carry out opening le tiroir de travail:
	play the sound draw on foreground.

Carry out closing le tiroir de travail:
	play the sound draw on foreground. 

Section 3 - Plan

Le plan de la demeure est une chose privately-named sur le bureau.
Le printed name est "papier".
La description est "[parmi]Je regardai ce vieux papier jauni. Il s'agissait d'un plan[_]![run paragraph on][ou]Ce plan représentait la demeure dans laquelle nous nous trouvions.[run paragraph on][stoppant]".

After examining le plan de la demeure:
	display figure map [centered]; [À FAIRE]
	dire "[à la ligne]". [Enlever ?]

Comprendre "papier" comme le plan de la demeure.
Understand "plan", "plan de la demeure", "demeure" or "carte" as le plan de la demeure when we have examined le plan de la demeure.

Carry out examining le plan de la demeure pour la première fois:
	maintenant le printed name du plan de la demeure est "plan".

Instead of examining le mouillé plan de la demeure, dire message mouillé.

Section 4 - Journal

Le journal est une chose privately-named dans le tiroir de travail.
Le printed name est "cahier".
La description est "Un ouvrage de cuir abîmé, sans aucune indication sur sa couverture à propos de son contenu. Il semblait banal, mais devait être précieux aux yeux de son ancien propriétaire pour être ainsi entreposé dans un tiroir verrouillé à double tour.".
Le sound est "Je fis bruire les pages. Le papier semblait en assez bon état malgré son apparente vieillesse.".
Le journal peut être lu. Le journal est non lu.

Comprendre "cahier" comme le journal.
Understand "journal" as le journal when we have read le journal.

Instead of opening le journal, essayer de reading the journal.

After reading le journal:
	play sound book on foreground;
	maintenant le printed name du journal est "journal";
	dire "Je feuillettai le cahier afin d'en voir brièvement le contenu. Il s'agissait d'un journal, pourtant il n'avait été tenu que quatre jours [--] à savoir les 30 et 31 octobre et le 1er et 2 novembre. L'année n'avait pas été écrite, mais le papier jauni semblait très ancien. Devais-je prendre le temps de le consulter[_]?".

Instead of consulting le journal about a topic, essayer de reading le journal.

Instead of consulting le journal about a topic when we have read le journal, dire "Je devais le consulter à propos de l'une des dates où il avait été écrit.".

Instead of consulting le journal about a topic listed in the Table of Journal when we have read le journal:
	play sound book on foreground;
	dire "[italique][entrée description][romain]";
	faire passer 3 minutes.

Table of Journal
Topic	Description
"30 octobre" or "trente octobre"	"[journal 1]"
"31 octobre" or "trente et un octobre" or "trente-et-un octobre"	"[journal 2]"
"1 novembre" or "1er novembre" or "premier novembre"	"[journal 3]"
"2 novembre" or "deux novembre"	"[journal 4]"

To dire journal 1:
	maintenant le couteau est utile;
	say "30 octobre[saut de paragraphe]C'est dans l'angoisse que j'écris ces lignes. Mes jours sont comptés. Ce journal permettra aux générations suivantes d'éviter le pire. En effet, mes recherches m'indiquent qu'une certaine espèce de plante poussant ici, dans la forêt alentour, aurait le pouvoir de [romain]le[italique] tenir à distance, pour une raison que j'ignore.[à la ligne]Ce matin, je me suis donc enfoncé dans la forêt et, après de longues heures, j'ai trouvé cette fleur extrêmement rare. Je me suis coupé la main en la cueillant, mais ma joie a été telle que je n'y ai pas pris attention. J'ai eu une chance incroyable en la trouvant la première journée. Cependant, ma blessure saignait abondamment[_]; j'ai dû rentrer au plus vite, car le liquide rouge s'épandait au sol. Une fois arrivé, j'ai pansé la plaie et planté le spécimen dans le jardin, pour qu'elle puisse servir dans le futur.[à la ligne]Je me couche ce soir plein d'espoir, car demain je commencerai les premiers essais.";

To dire journal 2:
	say "31 octobre[saut de paragraphe]Tout est fini. Aujourd'hui, en compulsant mes livres et mes notes, j'ai découvert qu['][romain]il[italique] viendrait ce soir, en vérité. Alors que je suis si près du but[_]! Je me résigne à disparaître, mais je dois au moins transmettre mon savoir. C'est pourquoi se trouvent ci-dessous les étapes importantes qui permettent de préparer la fleur[_]:[saut de paragraphe]• Cueillir la fleur et en arracher les pétales tant qu'elle est fraîche.[à la ligne]• Mettre les pétales dans un mortier avec un peu d'eau.[à la ligne]• Piler le mélange jusqu'à l'obtention d'une pâte uniforme. S'il n'y a pas assez d'eau, en rajouter[_]; attention toutefois, la mixture ne doit pas être liquide, mais consistante.[saut de paragraphe]Je ne sais pas encore à quoi peut servir cette préparation, mais je suis certain d'être sur la bonne voie, voie dont je ne verrai jamais le bout. Au lecteur qui lira ces lignes, j'espère que tu sauras trouver la solution.[saut de paragraphe]Adieu.";

To dire journal 3:
	maintenant le journal est lu;
	say "1er novembre[saut de paragraphe]Je ne m'attendais pas à me réveiller ce matin, et pourtant je suis encore en vie. Pourquoi[_]? Peut-être a-t-[romain]il[italique] été retardé… Oui, mais par quoi[_]? J'ai beau fouiller dans ma mémoire, je ne vois pas ce qui [romain]l[italique][']a empêché de venir me chercher.[à la ligne]Profitons de ce répit inespéré pour finir ce qui a été commencé. La préparation de la veille permettrait en théorie de [romain]l[italique][']éloigner, mais elle n'est pas assez puissante. L'idée m'est venue de combiner les étranges pouvoirs de cette plante avec un certain symbole ésotérique, qui protégeait des entités malfaisantes. J'ai déjà dessiné des ébauches de cette figure, mais je compare encore les différentes versions que j'ai pu trouver dans divers manuscrits afin de la reproduire dans les proportions idéales, ce qui lui permettra de maximiser son énergie. Je pense réussir demain, si le temps me le permet [--] ce que j'espère plus que tout.";

To dire journal 4:
	dire "2 novembre[saut de paragraphe]Tout est prêt. Je viens de créer le symbole parfait. Je vais le dessiner sur mon front avec la préparation.[à la ligne]Non[_]! [romain]Le[italique] voilà[_]! Je [romain]l[italique][']entends qui arrive[_]! Tout est perdu, la préparation est dans la cuisine[_]! Jamais je ne pourrais l'atteindre à temps[_]! Toi qui lis ces lignes, cherch[saut de paragraphe][romain]Le texte s'arrêtait ici. Les derniers mots avaient été écrits à la hâte, montrant toute l'angoisse qu'avait ressentie l'auteur de ces lignes. Si nous pouvions savoir ce qu'il avait voulu que nous cherchassions…[saut de paragraphe]":

Instead of reading le mouillé journal, dire message mouillé.
Instead of consulting le mouillé journal about a topic, dire message mouillé.

Part 4 - Incendie

Dropping l' allumette is pyromaniac.
Inserting l' allumette into something is pyromaniac.
Putting l' allumette on something is pyromaniac.
Burning l' armoire is pyromaniac.
Burning le bureau is pyromaniac.
Burning le grabat is pyromaniac.
Burning l' horloge is pyromaniac.
Burning les marches is pyromaniac.
Burning le papier peint is pyromaniac.
Burning la boîte d'allumettes is pyromaniac.
Burning la caisse is pyromaniac.
Burning la corde is pyromaniac.
Burning la couverture is pyromaniac.
Burning les lambeaux is pyromaniac.
Burning les plantes is pyromaniac.
Burning la serviette is pyromaniac.
Burning le sorbier is pyromaniac.
Burning la table de nuit is pyromaniac.
Burning le tiroir de nuit is pyromaniac.
Burning le tiroir de travail is pyromaniac.
Burning les étagères is pyromaniac.
Burning le tapis is pyromaniac.
Burning le volume is pyromaniac.
Burning le journal is pyromaniac.

Instead of pyromaniac when le player porte l' allumette:
	si la location est dans l' intérieur:
		dire "Sans comprendre pourquoi, je démarrai un incendie, ici, dans la demeure. Mon frère me regarda avec des yeux horrifiés.";
		attendre une touche;
		dire "Nous restâmes ainsi, immobiles, alors que les flammes se propageaient. Quand j'eus retrouvé mes esprits, il était trop tard.";
		attendre une touche;
		fin de l'histoire en disant "C'était la fin…";
	sinon si la location est dans l' extérieur:
		dire "Ce jardin devenu sauvage n'était pas normal[_]; toutes ses mauvaises herbes nous observaient dans notre dos de leur regard malfaisant. Je pris une allumette et la lançai. Le jardin prit feu.";
		attendre une touche;
		dire "Mon frère ne comprenait pas, il me regardait, la peur au fond des yeux. Il ne pouvait pas comprendre. C'était nécessaire, pourtant.";
		attendre une touche;
		dire "Les flammes nous entourèrent. Il ne restait que deux choses [--] deux personnes [--] à supprimer avant que le jardin ne soit de nouveau libre. Quand je me rendis compte de la triste réalité, il était trop tard. Avais-je perdu la raison[_]?";
		attendre une touche;
		fin de l'histoire en disant "C'était la fin…";
	sinon si la location est la remise:
		say "Je mis le feu à la remise, détruisant ainsi cette salle de torture. L'incendie se propagea et nous fûmes bientôt pris au piège. C'était ainsi que tout devait finir.";
		attendre une touche;
		fin de l'histoire en disant "C'était la fin…";
	sinon si la location est la chapelle:
		dire "Je ne pouvais pas mettre le feu dans ce lieu saint.".

Volume 4 - Tests - not for release

[Pour éclairer un endroit]
Instead of jumping: maintenant la location est éclairé.

[Pour déverrouiller un contenant]
Instead of squeezing un contenant:
	si le noun est verrouillé, maintenant le noun est déverrouillé;
	sinon maintenant le noun est verrouillé.

[Pour afficher l'heure]
When play begins:
	maintenant le left hand status line est "[les alentours du joueur] ([turn count])";
	maintenant le right hand status line est "[time of day] ; [heure fatidique]".

Understand "heure fatidique" as a mistake ("[heure fatidique]")

Timing is an action out of world applying to one time.
Comprendre "[time]" comme timing.
Report timing:
	dire "[time understood en lettres].".

[Pour gagner.]
Instead of waking up, fin définitive de l'histoire en disant "Ce n'était qu'un cauchemar".

Volume 5 - Releasing

Release along with cover art and the source text.

Book 1 - Bibliographic data

Le story headline est "Une fiction interactive".
Le story genre est "Horreur".
Le release number est 1.
Le story description est "Nous étions poursuivis, mon frère et moi. Par ma faute, nous courions au coeur de la forêt pour lui échapper. Je devais le protéger, il n'avait que huit ans. Il s'agissait de mon devoir de grande soeur. Mais où aller, quand tout se ressemblait[unicode 160]?
Soudain, mon frère me montra quelque chose, derrière les arbres[unicode 160]: «[unicode 160]Regarde, Nevena, là-bas[unicode 160]![unicode 160]» Nous étions sauvés. Cette vieille bâtisse allait nous abriter jusqu'au matin. Jusqu'au matin... Vraiment[unicode 160]?".
Le story creation year est 2013.

Book 2 - Index map

Index map with
	map-outline set to off, 
	font set to "Baskerville", [Fonts]
	title set to "Plan de la demeure", [Titres]
	subtitle of the level 0 set to "Premier etage",
	subtitle of the level -1 set to "Rez-de-chaussee",
	subtitle of the level -2 set to "Cave",
	room-name-length set to 128,
	room-colour of intérieur set to "Khaki", [Intérieur]
	room-name of the hall set to "Hall", [Hall]
	room-size of hall set to 50,
	room-name of salon set to "Salon", [Salon]
	room-name of salle à manger set to "Salle a manger", [Salle à manger]
	room-name of cuisine set to "Cuisine", [Cuisine]
	room-name of salle de bain set to "Salle de bain", [Salle de bain]
	room-name of escalier set to "Palier", [Escalier]
	room-name of bibliothèque set to "bibliotheque", [Bibliothèque]
	room-name of cabinet set to "Cabinet de travail", [Cabinet de travail]
	room-name of couloir set to "Couloir", [Couloir]
	room-name of chambre set to "Chambre", [Chambre]
	room-name of cave set to "Cave", [Cave]
	room-colour of extérieur set to "Olive", [Extérieur]
	room-name of jardin set to "Jardin", [Jardin]
	room-name of jardin-ouest set to "Jardin", [Jardin ouest]
	room-name of jardin-est set to "Jardin", [Jardin est]
	room-name of remise set to "Remise", [Remise]
	room-name of chapelle set to "Chapelle", [Chapelle]
	room-name of forêt set to "Foret". [Forêt]

Book 3 - Commandes

Part 1 - Aide

After printing the banner text, dire "[première fois]Tapez «[_]infos[_]» pour obtenir plus d'informations sur le jeu, si vous avez un problème concernant la lecture des sons ou si vous jouez à une fiction interactive pour la première fois.[seulement]".

Understand "infos", "info", "informations" or "information" as a mistake ("Ce jeu est une fiction interactive[_]; il se joue en tapant des commandes à l'infinitif telles que «[_]voir[_]», «[_]prendre [italique]objet[romain][_]», etc. (pour plus d'informations, consultez [italique]www.ifiction.free.fr[romain]).[unless glulx sound is supported][saut de paragraphe]Votre interpréteur ne supporte pas les sons. Vous devez utiliser un interpréteur qui les supporte, comme Gargoyle.[end unless][saut de paragraphe]Tapez «[_]licence[_]» pour l'obtenir et «[_]auteur[_]» pour en savoir plus sur l'auteur.[à la ligne]Si vous êtes bloqué(e), tapez «[_]indices[_]» pour avoir des indications générales qui vous aideront à continuer.").

Part 2 - Licence

Understand "licence" or "license" as a mistake ("[gras]Pour faire bref[_]:[à la ligne][romain]Ce programme est un logiciel libre[_]; vous pouvez le redistribuer ou le modifier suivant les termes de la «[_]GNU General Public License[_]» telle que publiée par la Free Software Foundation[_]: soit la version 3 de cette licence, soit toute version ultérieure.[saut de paragraphe]Ce programme est distribué dans l'espoir qu'il vous sera utile, mais SANS AUCUNE GARANTIE[_]: sans même la garantie implicite de COMMERCIALISABILITÉ ni d'ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.[saut de paragraphe][gras]Pour faire long, [romain]consultez[_]: [italique]http://www.gnu.org/licenses[romain].[saut de paragraphe]L'image de la couverture est un dessin d'Ylang, et est sous la licence CC-BY-NC-SA ([italique]http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr[romain]).[saut de paragraphe]Les sons sont soit libres de droits, soit sous licence LESF ([italique]http://www.sound-fishing.net/licences.html[romain])[_]; pour plus de détails, tapez «[_]sons[_]», mais gardez à l'esprit que le nom de certains sons pourrait vous réveler des éléments du jeu.").


Understand "sons" ou "sons" as a mistake ("[licence des sons]").

To say licence des sons:
	créditer la Table of Free Sounds avec "Sons libres de droits";
	dire saut de paragraphe;
	créditer la Table of LESF Sounds avec "Sons sous license LESF".

To créditer la/-- (T - a table name) avec (titre - a text):
	say "[gras][titre][romain]";
	parcourir T:
		dire "[à la ligne]• [entrée Nom]".

Table of Free Sounds
Nom
"Chute de la balle"
"Coups de l'horloge"
"Fermeture du portail"
"Hibou"
"Ouverture du portail"
"Tic-tac de l'horloge"
"«[_]Tu dors[_]?[_]»"

Table of LESF Sounds
Nom
"Affûtage du couteau"
"Allumette"
"Bris du pot"
"Chute de Nevena"
"Creusage"
"Déchirement de la couverture"
"Éclaboussure"
"Feu dans la cheminée"
"Feu dans la forêt"
"Grincement de l'armoire"
"Grincement de la pharmacie"
"Lampe à gaz"
"Loup"
"Ouverture du robinet"
"Page qui tourne"
"Pages qui tournent"
"Pas dans l'escalier"
"Pas à l'extérieur"
"Pas à l'intérieur 1"
"Pas à l'intérieur 2"
"Piège à loups qui se referme"
"Serrure"
"Tiroir"

Part 3 - Auteur

Understand "credits", "credit", "auteur" or "auteurs" as a mistake ("C'est dans la peur que moi, Natrium729, écris ce texte, car [italique]il[romain] viendra bientôt, quand le moment sera venu. Ce jeu permettra aux gens de se préparer avant [italique]son[romain] arrivée. Cependant, je ne possède peut-être pas toutes les informations nécessaires, et si vous avez quelque chose à me communiquer avant qu'il ne soit trop tard, faites comme mes bêta-testeurs et contactez-moi par courriel à [italique]natrium729@gmail.com[romain] ou directement sur mon site, [italique]http://ulukos.com[romain].[saut de paragraphe][italique]Sa[romain] seule image disponible, qui est en couverture, a été dessinée par Ylang avant qu['][italique]il[romain] ne disparaisse dans les ténèbres…[saut de paragraphe]En espérant que mes recherches n'aient pas été vaines, adieu.").

Part 4 - Indices

Chapter 1 - Indices généraux

Understand "aide", "astuce", "astuces", "indice", "indices", "soluce" or "solution" as a mistake ("Voici quelques indices pour vous guider[_]: [indices]").

To say indices:
	let L be {"Pour gagner, vous devez survivre jusqu'à l'aube, le soleil se levant à 5[_]heures.", "Si vous ne faites rien, la chose qui vous poursuit arrivera à minuit.", "Pour retarder son arrivée, il vous faut accomplir certaines actions. Le plus simple serait par exemple de fermer le portail. Mais où est la clef[_]?", "Essayez d'explorer tous les recoins de la maison. Qui sait ce qui se cache sous ce vieux tapis ou ce que vous trouverez en fouillant cette étagère[_]?", "Si vous ne pouvez plus rien porter de plus, donnez certaines choses à votre frère. De plus, vous pouvez lui ordonner de prendre ou de vous montrer quelque chose, ou bien de vous le redonner.", "Il est dit qu'il existe un moyen de sanctifier les objets. Un objet béni aurait sûrement une efficacité accrue face à ce qui vous poursuit[_]?", "Si vous pensez avoir fait tout ce qu'il y avait à faire, vous pouvez attendre un certain temps («[_]attendre 15 minutes[_]», par exemple) et voir si tout se passe bien…", "Si jamais ces indices n'étaient pas suffisants, allez dans la chapelle et tapez «[_]prier pour[_]» suivi de n'importe quel objet du jeu pour revevoir une vision. Certaines vous donneront peut-être des indications[_]!"};
	repeat with indice running through L:
		say "[à la ligne]• [indice]".

Chapter 2 - Indices spécifiques

Section 1 - Prier

Praying for is an action out of world applying to one visible thing.
Comprendre "prier pour [anything]" comme praying for.


Table of Hints
Sujet	Indice
allumette	"J'allumai l'allumette et elle m'éclairait. Je brûlais aussi quelque chose, mais je n'arrivais pas à distinguer quoi."
arbres	"J'étais dans la forêt, un objet lumineux à la main. Soudain, les branches s'enflammèrent."
armoire	"L'armoire n'était pas verrouillée et je pus ainsi l'ouvrir."
autel	"Il y avait une inscription en latin. J'avais appris le latin avant que nous fussions obligés de fuir, je devais être capable de la déchiffrer, même si toutes ces leçons semblaient si lointaines à présent."
baignoire	"Je voyais une personne, un homme. Il se baignait. C'était il y a très longtemps[_]; la baignoire ne servait à rien aujourd'hui."
balle	"J'insérais une balle en argent dans quelque chose, mais tout était flou, je ne distinguais pas ce que c'était."
boîte d'allumettes	"J'ouvrais la boîte d'allumettes afin d'en prendre une. Cependant, je voyais qu'il n'en restait pas beaucoup."
branches	"Des branches dans le foyer. Elles dégageaient une douce chaleur."
bureau	"Le bureau possédait un tiroir, mais il était verrouillé. Où était la clef[_]?"
caisse	"La caisse était lourde, mais je la poussais à l'intérieur."
chapelle-décor	"Je pénétrais dans la chapelle."
cheminée	"Nous étions assis, mon frère et moi, devant le feu. Nous n'étions pas dans la demeure en ruine, nous étions dans la demeure familiale, longtemps auparavant. Tous ces souvenirs… Mais ceci était fini à présent."
clef dorée	"J'insérais la clef dans une serrure, mais je n'aurais su dire laquelle."
clef rouillée	"Étrangement, je voyais une clef dorée, étincelante."
corde	"La corde était attachée à ce qui semblait être une marmite."
couteau	"Un éclat dans la nuit[_]; une lame qui s'abattait[_]; un cri. Le cauchemar… Soudain, je me retrouvais dans une forêt, m'entaillant la main. Puis tout disparaissait."
couverture	"Je me voyais déchirer la couverture en morceaux."
crucifix	"Le calme m'envahissait soudain. Je portais le crucifix."
cuillère	"Une flamme, une goutte argentée. C'était tout ce que je voyais dans ce noir d'encre."
dessins	"Une feuille emplie de dessins était dans ma main. Je la regardais, et les figures semblaient tournoyer. Elles restaient gravées dans ma mémoire."
eau	"Il y avait quelque chose dans l'eau. Puis j'écrasais ce quelque chose. Il y avait trop d'eau, je le versais sur un objet étrange."
fenêtre	"Je regardais par la fenêtre. Mon frère était blotti contre moi. Nous étions dans la demeure familiale. Soudain, il y eut du mouvement dehors. Ils arrivaient. Nous étions maintenant dans une vieille bâtisse, et la fenêtre était ébréchée, rien de plus."
fleurs	"Je cueillais des fleurs d'un blanc pur."
frère	"J'avais mis mon frère en sécurité, à l'écart, et je m'étais cachée dans un coin sombre de la pièce. Il ne fallait pas qu'il visse cela. Il y eut des cris, un éclat blanc, un coup de tonnerre. Ce souvenir me hanterait à jamais. Nous fuyions maintenant à travers la forêt et nous nous réfugions dans un bâtiment en ruine. Je demandais à mon frère de porter les objets en trop. Comment pouvais-je lui donner des ordres après tout ce qu'il avait subi par ma faute[_]?"
fusil	"Le noir total[_]; un soudain éclair aveuglant[_]; un coup de tonnerre. Le cauchemar… Puis tout disparut, et je me voyais mettre un objet brillant dans une arme."
grabat	"Nous étions couchés là, dans le noir. Mais il arrivait, nous ne pouvions pas rester ici."
horloge	"Les aiguilles tournaient, tourbillonnaient. Elles s'arrêtèrent soudain en indiquant [heure fatidique en lettres]. Puis treize coups résonnèrent."
journal	"Une main tremblante traçait des mots à la hâte. Mais elle n'eut pas le temps de terminer son message pour les générations futures. [romain]Il[italique] était là."
lambeaux	"La couverture n'était plus et je nouai maintenant les lambeaux de tissu entre eux."
lampe	"Cette vieille lampe nous éclairait et ramenait la lumière sur les souvenirs de cet endroit."
lune	"La lune nous surveillait, mais nous ne savions pas si c'était pour nous protéger ou non."
marches	"Nous dévalions l'escalier en toute hâte. Mon frère avait des larmes dans les yeux après ce qui venait d'arriver, mais je n'avais pas le temps de lui expliquer. Il fallait fuir le foyer qui nous avait vu grandir."
marmite	"Je jetais la marmite. Il y eut un bruit d'éclaboussure, puis le silence revint."
mixture	"Je plongeai mon doigt dans une étrange mixture, puis je le portais à mon front, et à celui de mon frère."
mortier	"Il y avait de l'eau et une autre chose à l'intérieur du mortier. Je n'eus pas le temps de savoir quoi, je commençais à piler son contenu."
moule	"Une goutte argentine, de l'eau. J'ouvrais le moule."
outils	"Des outils gisaient, brisés. Je passais sans y prendre attention."
papier peint	"Il décorait la maison autrefois. Aujourd'hui, il attendait."
pelle	"Je creusais, creusais, et creusais encore."
pharmacie	"J'ouvrais la pharmacie."
pierre à affûter	"J'avais le couteau en main, et je l'aiguisais."
pièges	"Des pièges dans la forêt. Pour les loups, ou pour autre chose[_]?"
plan de la demeure	"Nous nous étions perdus. Je tirais une feuille de ma poche, et nous situais sur le plan."
plantes	"Nous courions dans la forêt, et les ronces nous écorchaient les membres. Nous entrions dans le jardin, mais les ronces étaient toujours présentes."
portail	"Le portail grinçait alors que nous pénétrions dans le jardin. Dans notre terreur, nous oubliions de le fermer pour nous réfugier dans la demeure en ruine."
pot	"Un bruit de vaisselle cassée retentissait, et le pot se brisait par terre."
puits	"Il y avait de l'eau tout au fond du puits."
pétales	"J'arrachais ces beaux pétales de leur propriétaire. Puis j'étais dans la cuisine, et les plaçais dans le mortier."
remise-décor	"J'entrais, malgré le sentiment de peur qui me nouait l'estomac."
robinet	"Je tournais le robinet en vain. Il s'était tari depuis fort longtemps."
serviette	"Je prenais la serviette et la nouais. Elle se teintait de rouge."
sorbier	"Le sorbier était majestueux, mais je cassai ses branches."
table de nuit	"Je mettais la boîte d'allumettes dans le tiroir, puis me couchais avec mon frère sur le grabat. Quelques minutes plus tard, je me levais[_]: [romain]il[italique] arrivait."
table à manger	"Beaucoup de monde s'affairait autour de la table à manger. Cependant, chaque personne disparaissait subitement, une à une. Il n'en resta plus, et tout devint silencieux."
tapis	"Un homme était accroupi par terre. Il soulevait un coin du tapis, y cachait quelque chose. Puis il disparut."
tournevis	"Je le trouvais dans le salon, et le prenais, étant sûre qu'il pouvait me servir."
ustensiles	"Ils avaient tous l'air en mauvais état, mais je les fouillais minutieusement."
le volume	"Je feuilletais cet énorme livre, espérant trouver une information concrète."
yeux	"Une paire d'yeux incandescents nous surveillait. Était-il l'un de [romain]ses[italique] messagers[_]?"
étagères	"La bibliothèque était vaste, mais je la fouillais de fond en comble malgré tout."
Nevena	"Je me voyais serrant mon frère devant la fenêtre, alors qu'ils arrivaient. Je me voyais courant dans l'escalier, le tenant par la main et le forçant à me suivre. Je me voyais l'entraînant dans la forêt, alors qu'il était exténué. Je me voyais entrant avec lui dans la bâtisse, malgré son air peu rassurant. Je le voyais qui me suivait toujours et qui m'aimait, après tout ce que je lui avais fait endurer. Il fallait que je le sortisse de là, quoi qu'il en coûte."


Check praying for:
	if the location is not chapelle, say "Ce n'était pas le bon endroit pour prier." instead.

[
Report hinting something:
	say "Nous nous agenouillâmes, fermâmes les yeux et nous mirent à prier. Il n'y avait plus beaucoup d'espoir…[line break]";
	if the noun is an Item listed in the Table of Hints
	begin;
		say "J'eus soudain une vision…[paragraph break]";
		wait for any key;
		say "[italic type][Hint entry][roman type][paragraph break]";
		wait for any key;
		say "La vision s'estompa. Encore secouée, je me relevai et tirai mon frère de sa prière. Il ne fallait pas s'attarder.";
		wait for any key;
	else;
		say "Nous restâmes ainsi quelques minutes, et nous nous relevâmes. Il ne fallait pas baisser les bras.";
	end if;
	faire passer 5 minutes.
]

Report praying for something:
	say "Nous nous agenouillâmes, fermâmes les yeux et nous mirent à prier. Il n'y avait plus beaucoup d'espoir…[à la ligne]";
	si there is a sujet of the noun in the Table of Hints:
		attendre une touche;
		dire "J'eus soudain une vision…[saut de paragraphe]";
		attendre une touche;
		choose row with a sujet of the noun in la Table of Hints;
		dire "[italique][entrée Indice][romain][saut de paragraphe]";
		attendre une touche;
		dire "La vision s'estompa. Encore secouée, je me relevai et tirai mon frère de sa prière. Il ne fallait pas s'attarder.";
		attendre une touche;
	sinon:
		dire "Nous restâmes ainsi quelques minutes, et nous nous relevâmes. Il ne fallait pas baisser les bras.".

Section 1 - Supprimer les désambiguïsations et les clarifications

For clarifying the parser's choice while praying for: ne rien faire.

Does the player mean praying for le plan de la demeure: it is very likely.
Does the player mean praying for les murs: it is very likely.
Does the player mean praying for la table à manger: it is very likely.
Does the player mean praying for la pierre à affûter: it is very likely.
Does the player mean praying for la clef dorée: it is very likely.
Does the player mean praying for la chapelle-décor: it is very likely.
Does the player mean praying for les plantes: it is very likely.
Does the player mean praying for le tiroir de travail: it is very likely.

Part 1 - En connaître plus sur l'histoire

Table of Final Question Options (continued)
final question wording	only if victorious	topic	final response rule	final response activity
"en savoir plus sur l'HISTOIRE"	true	"histoire"	know more about the story rule	--

This is the know more about the story rule:
	dire "Avez-vous prié dans la chapelle, comme indiqué dans les indices[_]? Si non, félicitations[_]! Cependant, sachez que certaines des visions reçues en priant contiennent des allusions au passé des protagonistes.";
	attendre une touche;
	dire "Avez-vous en outre réussi l'énigme de la cave[_]? Si tel est le cas, <liste d'actions à faire sur la montre>. Lorsque la chose qui vous poursuit arrivera, le temps sera suspendu et vous pourrez continuer à explorer la demeure sans crainte de perdre, afin de découvrir tous ses secrets.[saut de paragraphe]";
	attendre une touche;
	dire "Si tout cela a déjà été fait[_]: merci d'avoir joué si longtemps[_]! J'espère que vous vous êtes bien amusé[_]!";
	attendre une touche;
	la règle réussit.