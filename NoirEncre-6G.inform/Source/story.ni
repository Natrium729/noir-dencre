"Noir d'encre" by le Nahual

[
Pour faire bref :
Ce code source est libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la « GNU General Public License » telle que publiée par la Free Software Foundation : soit la version 3 de cette licence, soit toute version ultérieure.

Ce programme est distribué dans l'espoir qu'il vous sera utile, mais SANS AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ ni d'ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.

Pour faire long, consultez : http://www.gnu.org/licenses
]
	
Volume 1 - Initialisation

Use MAX_VERBSPACE of 4143.
Use full-length room descriptions.
Use no scoring.

Book 1 - Temps

The time of day is 10:30 PM.

Part 1 - Dire le temps

Chapter 1 - En chiffres

To say 24h (relevant time - a time):
	let H be the hours part of relevant time;
	let M be the minutes part of relevant time;
	say "[if H is less than 10]0[end if][H]:[if M is less than 10]0[end if][M]".

Chapter 2 - En lettres

To say 24h (relevant time - a time) in words:
	let T be an indexed text;
	let T be "[relevant time in words]";
	let H be the hours part of relevant time;
	if the minutes part of relevant time > 30, increase H by 1;
	if H is 24, let H be 0;
	if the H is 0:
		replace word number 1 in T with "minuit";
		replace the text " heures"  in T with "";
	if the H is 12:
		replace word number 1 in T with "midi";
		replace the text " heures"  in T with "";
	if H > 12:
		if H is:
			-- 13: replace word number 1 in T with "treize";
			-- 14: replace word number 1 in T with "quatorze";
			-- 15: replace word number 1 in T with "quinze";
			-- 16: replace word number 1 in T with "seize";
			-- 17: replace word number 1 in T with "dix-sept";
			-- 18: replace word number 1 in T with "dix-huit";
			-- 19: replace word number 1 in T with "dix-neuf";
			-- 20: replace word number 1 in T with "vingt";
			-- 21: replace word number 1 in T with "vingt et une";
			-- 22: replace word number 1 in T with "vingt-deux";
			-- 23: replace word number 1 in T with "vingt-trois";
	replace the text "vingt-et-une"  in T with "vingt et une";
	replace the text "et demi"  in T with "et demie";
	say T.

Part 2 - Gestion du temps

Chapter 1 - Faire passer le temps en prenant implicitement

Rule for implicitly taking something: 
	follow the advance time rule;
	continue the activity.

Chapter 2 - Faire passer le temps

Spending time is a truth state that varies.

To spend (T - a time):
	let TC be the turn count;
	let the target time be T after time of day;
	decrease the target time by one minute;
	now spending time is true;
	while the time of day is not the target time
	begin;
		[say "[24h time of day] - [24h target time]";]
		follow the the turn sequence rules;
		if the time of day is the monster time or the time of day is 5:00 AM, break;
	end while;
	now the turn count is TC;
	now spending time is false.

Part 3 - Fin du jeu

The Ending is a scene.
The Ending begins when play begins.
The Ending ends sadly when the time of day is the monster time.
The Ending ends happily when the time of day is 5 AM.

When the Ending ends sadly:
	stop the background sound;
	say "Nous nous immobilisâmes soudain. Nous [italic type]l[roman type][']avions entendu. [italic type]Il[roman type] était venu nous chercher. Il était trop tard maintenant.";
	wait for any key;
	End the game saying "J'avais échoué…".

When the Ending ends happily:
	stop the background sound;
	say "Les premiers rayons du soleil teintèrent le ciel d'or. Enfin ! Nous étions exténués par cette nuit blanche, mais nous nous en étions sortis. Nous pouvions quitter cet endroit et partir le plus loin possible. Il nous restait encore un foyer à trouver.";
	end the story finally saying "Nous avions réussi !".

When play ends when the player is carrying the montre and the story has not ended finally:
	say "Tout semblait perdu quand, soudain, la montre luit comme si elle avait été chauffée à blanc. Elle s'ouvrit et les aiguilles tournèrent telle une tempête. Tout s'arrêta aussi subitement que cela avait commencé, et le temps sembla suspendu. Une étrange impression s'insinua en moi : je pouvais continuer d'explorer la demeure.";
	wait for any key;
	resume the story;
	try looking.

The monster time is a time that varies.
The monster time is 12 AM.

Book 2 - Objets

Part 1 - Nouvelles sortes

Chapter 1 - Allumette

A match is a kind of device with printed name "allumette", printed plural name "allumettes" and description "L'allumette était humide. Ça aurait été un miracle si elle s'allumait.".
Understand "allumette" as a match.
A match is always female.
A match has a number called remaining time.

When play begins (this is the give a random remaining time to every matches rule):
	repeat with M running through matches
	begin;
		now the remaining time of M is a random number between 2 and 5;
	end repeat.

Every turn when there is a switched on match (this is the decrease every matches remaining time rule):
	repeat with M running through switched on matches
	begin;
		decrement the remaining time of M;
		if the remaining time of M is 0
		begin;
			if M is visible, say "La flamme mourut, faisant disparaître sa chaude lumière. Je me débarrassai alors du morceau de bois brûlé.";
			remove M from play;
		end if;
	end repeat.

Check switching on a match (this is the can't light a match without a matchbox rule):
	if the boîte d'allumettes is not touchable, say "J'avais besoin de la boîte pour l'allumer." instead.

Carry out switching on a match (this is the standard switching on a match rule):
	play the sound match in foreground;
	now the noun is lit.

After switching on a match, say "L'allumette réussit à s'allumer, et une flamme rassurante apparut.".

Carry out switching off a match:
	remove the noun from play.

After switching off a match, say "Je soufflai l'allumette et l'obscurité reprit sa place. Je me débarrassai alors du morceau de bois brûlé.".

Does the player mean switching on a switched on match: it is very unlikely.
Does the player mean switching off a switched off match: it is very unlikely.

Part 2 - Backdrops

Chapter 1 - Fenêtre

The fenêtre is a female backdrop.
The fenêtre is in the chambre, the hall, the escalier, the salon, the salle à manger, the cuisine, the bibliothèque and the couloir.
Understand "vitre", "fenetre" or "carreaux" as the fenêtre.
The description is "La fenêtre était [if the location is salon]brisée, et le vent qui filtrait était malsain[else]dans un piteux état, et la vitre ne tenait que par miracle[end if]. On pouvait apercevoir la lune au travers, inquiétante.".

Instead of opening or attacking the fenêtre, say "[if the location is salon]Le vent glacial se permettait d'entrer, et je ne voulais pas l'inviter à continuer[else]Je ne voulais pas la briser, le vent qui se serait alors engouffré n'aurait rien arrangé[end if]."

Chapter 2 - Lune

The lune is a female backdrop.
The lune is in the chambre, the hall, the salon, the salle à manger, the cuisine, the bibliothèque, the couloir, the jardin, the jardin-e and the forêt.
The description is "Une lune gibbeuse éclairait du mieux qu'elle le pouvait, mais sa triste lumière n'avait rien de rassurant.".

Chapter 3 - Marches

Some marches are a backdrop.
The marches are in the escalier and the hall.
Understand "escalier" as the marches.
The description is "Des marches grinçantes [if the location is hall]montaient à l[']étage[else]descendaient vers le hall d[']entrée[end if]. Visiblement, elles allaient s'effondrer d'un instant à l'autre. Nous ne devrions pas nous attarder dans notre [if the location is hall]ascension[else]descente[end if]."

Chapter 4 - Robinet

The robinet is a backdrop. The robinet is in the cuisine and the salle de bain.
Understand "evier" as the robinet when the location is cuisine.
Understand "lavabo" as the robinet when the location is salle de bain.
The description is "[if the location is cuisine]Un antique évier et son robinet. Il n'avait plus l'air utilisable[else]Un vieux lavabo sale et son robinet. Il ne semblait plus en état de marche[end if].".

Instead of opening the robinet:
	play the sound gurgle in foreground;
	say "[one of]J'ouvris le robinet ; un lointain gargouillis se fit entendre depuis les entrailles de la demeure. Puis tout s'arrêta, sans la moindre petite goutte[or]J'essayais encore d'obtenir de l'eau, pourtant je savais déjà qu'elle ne coulerait pas[stopping].".
Instead of  turning the robinet, try opening the robinet.

Part 3 - Propriétés

Chapter 1 - Mouillé / Sec

A thing can be wet or dry. A thing is usually dry.

To say wet message:
	say "Il était détrempé et ne servait maintenant plus à rien.".

Chapter 2 - Sanctifié

A thing can be sanctified. A thing is usually not sanctified.

Chapter 3 - En fer

A thing can be iron. A thing is usually not iron.		

Carry out an actor taking a iron thing:
	if the noun is sanctified, now the monster time is 25 minutes after the monster time;
	else now the monster time is 20 minutes after the monster time.

Carry out dropping a iron thing:
	if the noun is sanctified, now the monster time is 25 minutes before the monster time;
	else now the monster time is 20 minutes before the monster time.

Carry out inserting a iron thing into something:
	if the noun is sanctified, now the monster time is 25 minutes before the monster time;
	else now the monster time is 20 minutes before the monster time.

Carry out putting a iron thing on a thing:
	if the noun is sanctified, now the monster time is 25 minutes before the monster time;
	else now the monster time is 20 minutes before the monster time.

Book 3 - Actions

Part 1 - Compréhension

Understand "h" as up.
Understand "b" as down.
Understand "interieur" or "dedans" as inside.
Understand "exterieur" or "dehors" as outside.

Understand "dechirer [something]" as cutting.
Understand "arracher [something]" as pulling.
Understand the command "piler" or "broyer" as "squeeze".
Understand the commands "affuter" or "aiguiser" as "rub".
Understand "fondre [something]" as burning.
Understand "verser [something] dans/sur [something]" as inserting it into.

Understand "montre [something]" as showing it to.
Understand "donne [something]" as giving it to.
Understand "prends [something]" as taking.

Part 2 - Attendre

After of waiting:
	say "Je me figeai soudain de désespoir, et attendit. Puis, après quelque temps, je me secouai et repris lentement mes esprits. Il n[']était pas question d'abandonner.".

Waiting more is an action applying to a number.
Understand "attendre [a time period]", "patienter [a time period]", "a [a time period]" or "z [a time period]" as waiting more.

Carry out waiting more:
	let TC be the turn count;
	let T be the time understood;
	if T is greater than one hour, now T is one hour;
	let the target time be the time of day plus the T;
	decrease the target time by one minute;
	now spending time is true;
	while the time of day is not the target time
	begin;
		follow the the turn sequence rules;
		if the time of day is the monster time or the time of day is 5:00 AM, break;
	end while;
	now the turn count is TC;
	now spending time is false.

Report waiting more:
	say "Je ne savais vraiment plus quoi faire, et nous attendîmes, seuls dans la pénombre. Enfin, quand l'attente ne fut plus supportable, je fis signe à mon frère que nous devions continuer. Il ne fallait pas baisser les bras.".

Part 3 - Écouter

Every room has a text called sound.
The sound of a room is usually "Un inquiétant silence régnait.".

Instead of listening to a room, say "[sound of the location][line break]".

Part 4 - Prendre

The can't take people's possessions rule is not listed in the check taking rulebook.

Check the frère taking something:
	if the player carries the noun, try giving the noun to the frère instead.

Unsuccessful attempt by the frère taking:
	if the reason the action failed is the can't exceed carrying capacity rule, say "Il secoua la tête ; il ne pouvait rien porter de plus.";
	else say "Mon frère essaya, mais n'y arriva pas.".

Report the frère taking something:
	say "J'indiquai à mon frère de ramasser [the noun].[no line break]".

Part 5 - Donner

Check the player giving something to the frère (this is the brother can't be given more than the carrying capacity rule):
	if the number of the things carried by the frère >= the carrying capacity of the frère, say "Il secoua la tête ; il ne pouvait rien porter de plus." instead.

Report giving something to the frère:
	say "Je demandai à mon frère de garder [the noun]. Il acquiesça.[no line break]".


Check the frère giving something to the player (this is the player can't be given more than the carrying capacity rule):
	if the number of the things carried by the player >= the carrying capacity of the player, stop the action.

Unsuccessful attempt by the frère giving:
	if the reason the action failed is the the player can't be given more than the carrying capacity rule, say "Il secoua la tête ; j'en portais déjà trop.";
	else say "Il ne put me le donner.".

Report the frère giving something to the player:
	say "Je redemandai [the noun] à mon frère, qui me [if the noun is male]le[else]la[end if] donna.[no line break]".

Rule for supplying a missing second noun while an actor giving:
	now the second noun is the player.

Part 6 - Montrer

Instead of the frère showing something (called S) to the player:
	try examining S;
	rule succeeds.

Rule for supplying a missing second noun while an actor showing:
	now the second noun is the player.

Part 7 - Attacher

Understand "nouer [something]" as tying it to.

Rule for supplying a missing second noun while tying:
	if the noun is the serviette, now the second noun is the player;
	else now the second noun is the noun.

Part 8 - Verrouiller

Carry out locking something with something:
	play the sound lock in foreground.

Carry out unlocking something with something:
	play the sound lock in foreground.

Part 9 - Dessiner

Drawing on is an action applying to one thing.
Understand "dessiner sur [something]", "dessiner le/un symbole sur [something]", "dessiner symbole sur [something]", "dessiner une/la figure sur [something]" or "dessiner figure sur [something]" as drawing on.
Understand the command "tracer" as "dessiner".

Check drawing on:
	say "Il n[']était pas temps de dessiner." instead.

Part 10 - Creuser

Digging is an action applying to nothing.
Understand "creuser" as digging.

Check digging when the pelle is not carried:
	say "Je n'avais pas de pelle et, de toute façon, c[']était inutile." instead.

Check digging:
	say "C[']était inutile." instead.

Instead of digging when the pelle is carried and the location is forêt for the first time:
	play the sound shovel in foreground;
	say "Je priai mon frère de rester près du portail et commençait à m[']éloigner. Je creusai de toutes mes forces, autour de l'entrée, une fosse, qui [italic type]l[roman type][']empêcherait [--] peut-être [--] de venir.";
	now the monster time is 25 minutes after the monster time;
	spend 20 minutes.

Part 11 - Brûler

The block burning rule is not listed in the check burning rulebook.

Check burning something (this is the can't burn without fire rule):
	if the player does not carry a switched on match, say "Je n'avais rien pour allumer un feu." instead.

Check burning someone (this is the block burning people rule):
	if the noun is the player, say "Je n'allais pas me brûler !" instead;
	if the noun is the frère, say "Je ne pouvais pas lui faire de mal. Pas après tout ce qu'il avait subi par ma faute." instead.

Report burning:
	say "Il aurait été bête de brûler ceci.".

Book 4 - Phrases

To discover (item - a thing):
	If the number of things carried by the player >= the carrying capacity of the player
	begin;
		now the item is in the location;
		say "[line break]Malheureusement, je ne pouvais rien porter de plus, et dut [if the item is male]le[else]la[end if] laisser ici.";
	else;
		now the player carries the item;
	end if.
	
To say --:
	say unicode 8212.

Book 5 - Extensions

Part 1 - French

Include French by Eric Forgeot.
Use French 1PSPa Language.

[Include French Second Person Understanding by Natrium729.]

Part 2 - Basic Screen Effects

Include Basic Screen Effects by Emily Short.

Part 3 - Glulx Image Centering

Include Glulx Image Centering by Emily Short.

Figure map is the file "plan.png".

Part 4 - Multiple Sounds

Include Multiple Sounds by Massimo Stella.

When play begins:
	create the midground channel; [COUPS DE L'HORLOGE]
	create the midground 1 channel; [SIFFLEMENT DE LA LAMPE]
	set the midground 1 volume to 0;
	play the sound gas in midground 1 with loop;

Chapter 1 - Sons

Section 1 - Effets

[INTRO]
Sound are you sleeping is the file "tu_dors.ogg".
Sound wolf is the file "loup.ogg".

[SALON]
Sound falling is the file "chute.ogg".
Sound item falling is the file "chute_objet.ogg".

[SALLE DE BAIN]
Sound gurgle is the file "gargouillis.ogg".
Sound pharmacy is the file "grincement_pharmacie.ogg".

[CUISINE]
Sound sharpening is the file "affutage.ogg".
Sound creaking cupboard is the file "grincement_armoire.ogg".
Sound pot is the file "pot.ogg".

[CHAMBRE]
Sound match is the file "allumette.ogg".
Sound gas is the file "gaz.ogg".
Sound tear cloth is the file "dechirement_tissu.ogg".

[BIBLIOTHÈQUE]
Sound books is the file "livres.ogg".
Sound book is the file "livre.ogg".

[JARDIN]
Sound splashing is the file "eclaboussure.ogg".
Sound closing gate is the file "fermer_portail.ogg".
Sound opening gate is the file "ouvrir_portail.ogg".
Sound shovel is the file "pelle.ogg".

[REMISE]
Sound jaws is the file "machoires.ogg".

[PLUSIEURS ENDROITS]
Sound draw is the file "tiroir.ogg".
Sound lock is the file "serrure.ogg".

[AUTRES]
Sound clock is the file "horloge.ogg".
Sound steps1 is the file "pas_plancher1.ogg".
Sound steps2 is the file "pas_plancher2.ogg".
Sound stairs is the file "escalier.ogg".
Sound nature steps is the file "pas_feuilles.ogg".

Section 2 - Ambiance

Sound owl is the file "hibou.ogg". [COULOIR]
Sound tick is the file "tictac.ogg". [HALL et ESCALIER]
Sound hearth is the file "foyer.ogg". [SALON après avoir allumé la cheminéei]
Sound forest fire is the file "feu_foret.ogg". [FORÊT pendant l'incendie]

Chapter 2 - Ambiance des pièces

Every room has a sound name called ambiance.
The ambiance of a room is usually sound wolf.

After going to a room (called destination):
	stop the background sound;
	if the ambiance of the destination is not sound wolf, play the ambiance of the destination in background with loop;
	continue the action.

Chapter 3 - Coups des heures

Every turn when the minutes part of time of day is 59:
	if the hours part of the time of day > 11
	begin;
		let N be the hours part of the time of day - 11;
		play the sound clock in midground for N times;
	else;
		let N be the hours part of the time of day + 1;
		play the sound clock in midground for N times;
	end if.

Volume 2 - Jeu

Book 1 - Introduction

When play begins:
	say "[italic type]Pour profiter au mieux de ce jeu, il vaut mieux porter des écouteurs ou un casque, et le jouer dans une pièce sombre…[roman type]";
	wait for any key;
	clear the screen;
	wait for any key;
	say "« Tu dors ? »[line break]";
	play the sound are you sleeping in foreground;
	wait for any key;
	say "Je lui répondis que non. Après tout, comment pouvais-je dormir ?";
	wait for any key;
	say "J'essayais de trouver des mots qui le rassureraient quand soudain…[line break]";
	wait for any key;
	play the sound wolf in foreground;
	wait for any key;
	say "[line break]Nous nous pétrifiâmes de terreur. [italic type]Il[roman type] était là. Si l'on ne faisait rien, nous étions perdus. [italic type]Il[roman type] allait nous retrouver, rapidement. Peut-être y avait-il un moyen de nous sauver, mon frère et moi, dans ce bâtiment en ruines.[paragraph break]";
	wait for any key;
	say "Il fallait l'explorer, malgré mon appréhension…[line break]";
	wait for any key.

Book 2 - Jeu

Part 1 - Rez-de-chaussée

Interior is a region.
The hall, salle à manger, cuisine, salon, salle de bain, escalier, bibliothèque, cabinet, couloir, chambre and cave are in interior.

Carry out going from a room (called R1) to a room (called R2) in interior:
	Unless (R1 is escalier and R2 is hall) or (R1 is hall and R2 is escalier)
	begin;
		if a random chance of 1 in 2 succeeds, play the sound steps1 in foreground;
		else play the sound steps2 in foreground;
	end unless.

Chapter 1 - Hall

The hall is a room. "Un hall de taille imposante, à l'aspect lugubre. Le mur crasseux laissait entrevoir des morceaux de papier peint décollés. Le décor, dans la faible luminosité, n'avait rien d'accueillant. Pourtant, cette bâtisse était le meilleur moyen de nous abriter. Du moins, je l'espérais…[line break]Nous pouvions aller [if the salon is visited]dans le salon [end if]au nord, [if the salle à manger is visited]dans la salle à manger [end if]à l'ouest, [if the salle de bain is visited]dans la salle de bain [end if]à l'est, ou sortir dans le jardin par le sud. Nous pouvions également emprunter l'escalier pour aller à l[']étage.".
The printed name is "Dans le hall d'entrée".
The sound is "Le tic-tac narquois de l'horloge brisait le silence.".
The ambiance is the sound tick.

Carry out going from the hall to the escalier:
	play the sound stairs in foreground.

Section 1 - Horloge

The horloge is a female fixed in place thing in the hall. "[If broken]L'horloge demeurait là, inutile[else]Au mur, une imposante horloge affichait environ [24h time of day + 1 minute to the nearest five minutes in words][end if].".
Understand "heure" as the horloge.
The description of the horloge is "Une énorme horloge était adossée au mur. [if the horloge is broken]Elle n'avait plus d'aiguilles par ma faute, et il était maintenant impossible de savoir l'heure[else]Ses aiguilles avançaient trop vite à mon goût. Elles semblaient nous narguer du peu de temps qu'il nous restait ; en effet, il était environ [24h time of day + 1 minute to the nearest five minutes in words]. Je ne devais pas m'attarder pour la contempler[end if].".
The horloge can be broken. The horloge is not broken.
Instead of listening to the horloge, try listening.

Instead of attacking the horloge:
	now the horloge is broken;
	say "Le temps pressait et cette horloge ne cessait de nous le rappeler par son tic-tac narquois. Dans un accès de rage, je me jetai sur le cadran et lui donnai un coup. Il vola en éclat, et seule la main de mon frère sur mon épaule m'arrêta. J'étais honteuse maintenant, car j'avais perdu mon sang-froid devant lui et par ma faute, nous ne pouvions plus savoir l'heure. En outre, si les aiguilles étaient brisées, le mécanisme, lui, continuait de fonctionner, et le bruit n'avait pas cessé.".

Section 2 - Papier peint

The papier peint is a scenery in the hall.
Understand "papier" or "mur" as the papier peint.
The description is "Un reste de décoration subsistait, tentant tant bien que mal de s'accrocher aux murs couverts de crasse. Depuis quand cet endroit était-il désert ?".

Instead of pulling the papier peint:
	say "Ce papier peint m'exaspérait, pour je ne savais quelle raison, et je tentais de l'arracher en vain.";
	spend 3 minutes.

Instead of taking off the papier peint, try pulling the papier peint.

Chapter 2 - Salon

The salon is north from the hall. "Un riche salon, mais éprouvé par le temps. Il était spacieux, et le peu de mobilier accentuait cet aspect. La vitre cassée laissait passer un vent glacial, qui semblait [if the branches are burnt]mener un combat sans merci contre la chaleur du feu[else]se moquer de la cheminée éteinte. Il ne fallait pas traîner[end if].".
The printed name is "Au milieu du salon".
The sound is "Le vent nous murmurait de quitter cet endroit au plus vite avant que… Non, j[']étais en train de délirer.".

Instead of exiting when the location is salon, try going south.
Instead of going outside when the location is salon, try going south.

Section 1 - Dessin

The dessins is a privately-named female thing in salon.
Understand "feuille" as the dessins.
Understand "dessins", "dessin", "symbole" or "symboles" as the dessins when the dessins are examined.
The printed name is "[if examined]dessins[else]feuille".
The description is "Le papier abîmé était le support de nombreux dessins, dont la plupart étaient raturés ; d'autres avaient été effacés et ne subsistaient que par un pâle tracé. Mais au centre de la feuille, parmi l'amas des crayonnés qui se superposaient, un grand symbole entouré d'un trait épais attirait l[']œil.[if the journal is read] Était-ce la figure que mentionnait le journal ?[end if]".

The dessins can be examined. The dessins is not examined

Carry out examining the dessins for the first time:
	now the dessins is male;
	now the dessins is plural-named;
	now the dessins is examined.

Instead of examining the wet dessins, say wet message.

Instead of burning the dessins when the player carries a switched on match (called M):
	remove M from play;
	say "Ces dessins ne m'inspiraient rien qui vaille. Je pris l'allumette et le réduisis en cendre, sous le regard incompréhensif de mon frère.";
	spend 2 minutes.

Section 2 - Fusil

The fusil is a thing in the salon. "Un fusil était accroché au mur, au-dessus de la cheminée.".
The description is "Une arme antique, qui ne servait maintenant plus que de décoration destinée à prendre la poussière[if the fusil is not charged] ; en effet, il n'avait pas l'air chargé[else]. Je l'avais chargé, mais je ne savais pas si le fusil était fonctionnel[end if]. Je détestais les armes depuis ce jour qui paraissait déjà si lointain, mais elle pouvait s'avérer utile.".

After taking the fusil for the first time, say "Je décrochai le fusil du mur.".
The fusil can be charged. The fusil is not charged.

Carry out taking the charged fusil:
	now the monster time is 20 minutes after the monster time.

Carry out dropping the charged fusil:
	now the monster time is 20 minutes before the monster time.

Carry out inserting the charged fusil into something:
	now the monster time is 20 minutes before the monster time.

Carry out putting the charged fusil on a thing:
	now the monster time is 20 minutes before the monster time.

Section 3 - Moule

The moule is a thing.
The description is "Ce lourd objet m'avait bien fait mal. On aurait dit… une sorte de moule. Mais j'ignorais à quoi cela pouvait servir[if hot].[line break]Il contenait de l'argent en fusion[else if filled].[line break]Je pouvais l'ouvrir, maintenant que le métal était froid[end if].".
The moule can be hot. The moule is not hot.
The moule can be filled. The moule is not filled.

[DÉCOUVRIR MOULE]
Instead of going from the salon to the hall for the first time:
	play the sound falling in foreground;
	wait for any key;
	say "Mon pied venait de heurter quelque chose, et la chute fut douloureuse. Mon frère accourut et m'aida à m'asseoir. Je pris le temps de souffler cinq minutes, tandis qu'il me montrait sur quoi j'avais trébuché. C[']était un objet étrange, une espèce de moule…[line break]";
	discover the moule;
	wait for any key;
	spend 5 minutes.

[REFROIDIR MOULE]
Instead of inserting the eau into the hot moule:
	[SFX SIFFLEMENT]
	say "Je versai l'eau sur le moule afin de solidifier le métal. Il y eut un sifflement, puis le calme revint.";
	now the moule is not hot;
	now the moule is filled.

[OUVRIR MOULE]
Instead of opening the moule, say "On pouvait apparemment l'ouvrir. Je le fis plusieurs fois, sans en voir l'utilité. Je le fermai de dépit.".

Instead of opening the hot moule, say "Il s'agissait d'une très mauvaise idée, je me serais brûlée.".

Instead of opening the filled moule:
	say "Maintenant que tout paraissait sûr, je pris fermement le moule et l'ouvris.";
	wait for any key;
	play the sound item falling in foreground;
	say "Un objet fuselé et brillant tomba à terre. Je me baissai et m'en saisis. Une balle ?";
	wait for any key;
	discover the balle.

Section 4 - Balle

The balle is a female thing.
The description is "Cette balle était en argent. En argent… peut-être que… ?".

Instead of inserting the balle into the fusil:
	remove the balle from play;
	say "J'essayai de charger le fusil, mais je ne savais vraiment pas comment faire, et mon frère non plus. Après avoir examiné l'arme dans ses moindres détails, je trouvai pourtant  une solution.";
	now the fusil is charged;
	if the player carries the fusil, now the monster time is 20 minutes after the monster time;
	spend 2 minutes.

Instead of burning the balle when the player carries a switched on match (called M):
	remove M from play;
	remove the balle from play;
	say "Cette balle… était une balle, une arme, et je voulais oublier… J'approchai la flamme et la fit fondre et disparaître.".

Section 5 - Tournevis

The tournevis is a thing in the salon.
The description is "Un tournevis banal, qui possédait un manche en bois. Combien de fois avais-je vu un outil semblable, à l'époque où… Non, il fallait se concentrer sur le présent.".

Section 6 - Cheminée

The cheminée is a scenery open not openable container in the salon.
Understand "cheminee" or "foyer" as the cheminée.
Understand "feu" or "flammes" as the cheminée when the branches are burnt. 
The description is "[if the branches are burnt]Un petit feu brûlait dans la cheminée. Il ne réchauffait pas beaucoup, mais on se rassurait comme on pouvait[else]La cheminée était vide et triste. Si seulement[end if]…[line break]".
The carrying capacity of the cheminée is 1.

Chapter 3 - Salle à manger

The salle à manger is west from the hall. "Ici, seule la poussière témoignait du temps qui s[']était écoulé depuis l'abandon de la maison. Tout était en ordre, comme si un spectre du passé tenait à ce qu'un éventuel visiteur puisse encore se restaurer. Je secouai la tête, il fallait chasser ces pensées. Plus personne n'habitait ici, je commençais à divaguer.".
The printed name is "À l'intérieur de la salle à manger".
The sound is "Des bruits de couverts, des rires, des discussions… On entendait presque les anciens habitants. Maintenant, on ressentait un grand chagrin, un chagrin qui témoignait d'un effroyable événement survenu dans cette maison, il y a longtemps.".

Instead of exiting when the location is salle à manger, try going east.
Instead of going outside when the location is salle à manger, try going east.

Section 1 - Table à manger

The table à manger is a supporter in the salle à manger.
The description is "Une grande table destinée à de nombreux convives et qui attendait, depuis leur disparition, que quelqu'un vienne manger avec elle.".

Section 2 - Couteau

The couteau is an iron thing on the table à manger.
The description is "Ce couteau me remémorait de tristes souvenirs. Je voyais défiler des images dans ma tête, je… Je repris soudain mes esprits. Que dirait mon frère s'il me voyait faiblir ?[line break][if rusty]Sa lame rouillée le rendait totalement inutile[else]Maintenant qu'il était aiguisé, il devenait un ustensile dangereux[end if].".

The couteau can be rusty or sharpened. The couteau is rusty.

After burning the couteau:
	say "J'approchai l'allumette du couteau. Il chauffa, mais la flamme n'était pas assez chaude pour le faire fondre.";
	spend 5 minutes.

Instead of cutting the frère when the player carries the couteau, say "Jamais je n'aurais levé la main sur mon frère. Pas après tout le mal qu'il avait dû subir à cause de moi.".

[SE COUPER]
Yourself can be cut. Yourself is not cut.

Instead of cutting the player when the player carries the couteau and the player is not cut:
	if the couteau is rusty
	begin;
		say "La lame était horriblement rouillée et je ne me serais pas risquée à me couper avec elle.";
	else;
		now the player is cut;
		say "Une folle idée me vint ; ma main tremblait, je fermai les yeux. Malgré ce que cet acte ravivait en moi, j'entaillai la paume de ma main. Je tressaillis, et le sang commença à couler.";
	end if.

Instead of cutting the player when the player is cut or the serviette is part of the player, say "Je m[']étais déjà coupée, et je ne tenais pas à recommencer.".

Instead of attacking the player when the player carries the couteau, try cutting the player.

Bleeding is a scene. Bleeding begins when the player is cut. Bleeding ends sadly when the time since bleeding began is 15 minutes. Bleeding ends happily when the serviette is part of the player.

Every turn when spending time is false during Bleeding:
	say "[one of]Ma coupure me faisait mal[or]Le sang continuait de couler[or]Je ne la laissais pas paraître ma douleur à mon frère[or]Je serrais ma main très fort tant la douleur était grande[at random].".

When Bleeding ends sadly:
	say "Ma tête se mit à tourner… Ma vision se troubla… J'avais perdu trop de sang.";
	wait for any key;
	end the story saying "C[']était la fin…".

Chapter 4 - Cuisine

The cuisine is north from the salle à manger. "La cuisine, contrairement à la salle à manger, était véritablement abandonnée, des ustensiles traînant çà et là. Une imposante armoire se dressait dans un coin, un plan de travail au fond de la pièce, à côté de l[']évier… J'imaginais de nombreuses domestiques s'affairant à cet endroit, même si je savais qu'il ne fallait plus songer au passé.".
The printed name is "Dans la cuisine".

Instead of exiting when the location is cuisine, try going south.
Instead of going outside when the location is cuisine, try going south.

Section 1 - Ustensiles

Some ustensiles are scenery in the cuisine.
The description is "Des ustensiles jonchaient le sol, mais aucun n'avait l'air utilisable.".

Instead of searching the ustensiles for the first time:
	say "Malgré tout, je décidai de faire l'inventaire de ce qu'il y avait ici. Au bout d'un moment, nous trouvâmes une belle cuillère. Elle était terne, mais il s'agissait assurément d'argent.";
	discover the cuillère;
	spend 7 minutes.

Instead of searching the ustensiles, say "Il n'y avait plus rien d'intéressant.".

Section 2 - Cuillère

The cuillère is a female thing.
Understand "cuillere" or "cuiller" as the cuillère.
The description is "L'ustensile en argent était vieux et terni. Il avait dû connaître de nombreux repas.".

Instead of burning the cuillère when the player carries the switched on match (called M):
	remove M from play;
	If the moule is touchable
	begin;
		say "J'approchai la flamme de la cuillère. Lentement, elle commença à s'amollir. Enfin, quelques gouttes tombèrent dans le moule.";
		now the moule is hot;
	else;
		say "J'approchai la flamme de la cuillère. Lentement, elle commença à s'amollir. Enfin, les premières gouttes tombèrent…[line break]";
		wait for any key;
		say "Soudain, une douleur me fit retenir un cri. Du métal en fusion venait de me brûler la main. Je ne recommencerais plus cela.";
	end if;
	remove the cuillère from play;
	spend 5 minutes.

Section 3 - Armoire

The armoire is a female scenery fixed in place closed openable container in the cuisine.
The printed name is "lourde armoire".
Understand "lourde armoire" as the armoire.
The description is "[if pushed]Cette armoire dissimulait un passage dans le sol ! Il devait sûrement mener à la cave. Était-ce une bonne idée de descendre, je n'aurais su le dire[else]Cette imposante armoire occupait un coin de la pièce. Elle n'avait rien de particulier[end if].".

Carry out opening the armoire:
	play the sound creaking cupboard in foreground.

Carry out closing the armoire:
	play the sound creaking cupboard in foreground.

[Pousser Armoire]
The armoire can be pushed. The armoire is not pushed.

Instead of pushing the not pushed armoire:
	say "Je pris appui sur le meuble et poussai de toutes mes forces ; en vain. J'appelai alors mon frère et lui expliquai ce que je voulais faire. À deux, nous tentâmes ce que je n'avais pas réussi toute seule.";
	wait for any key;
	say "Au début, rien ne se passa, puis, finalement, l'armoire bougea. Il y avait un passage caché sous elle ! Allions-nous descendre par là ?";
	wait for any key;
	now the armoire is pushed;
	spend 2 minutes.

Instead of pushing the pushed armoire, say "Nous n'avions plus besoin de la pousser, et nous n'avions pas de temps à perdre.".

Section 4 - Marmite

The marmite is a female iron container in the armoire.
After printing the name of the marmite while not taking the eau or examining the marmite:
	if the eau is in the marmite, say " pleine d'eau".
The description is "Une petite marmite esseulée [if the marmite is in the armoire]attendait[else]avait longtemps attendu[end if] dans l'armoire[if the marmite is part of the corde] et était maintenant attachée à la corde[end if]. Elle était encore en bon état.".

[Attacher]
Instead of tying the marmite to the corde:
	now the marmite is part of the corde;
	say "J'attachai la marmite à la corde.".

Instead of tying the corde to the marmite, try tying the marmite to the corde.

[Comportement quand attachée]
Instead of taking the marmite when the marmite is part of the corde:
	try taking the corde.

Instead of dropping the marmite when the marmite is part of the corde:
	try dropping the corde.

Instead of inserting the marmite into something (called C):
	try inserting the corde into C.

Instead of putting the marmite on something (called S):
	try putting the corde on S.

Section 5 - Plan de travail

The plan de travail is a scenery supporter in the cuisine.
The description is "Une surface de travail qui avait beaucoup dû servir autrefois. Maintenant, elle restait là, silencieuse et poussiéreuse.".

Section 6 - Pierre à affûter

The pierre à affûter is a female thing on the plan de travail.
Understand "affuter" or "pierre a affuter" as the pierre à affûter.
The description is "Ceci me permettait d'aiguiser quelque chose.".

Instead of rubbing the rusty couteau when the pierre à affûter is touchable:
	play the sound sharpening in foreground;
	now the couteau is sharpened;
	say "Je le préférais inoffensif, mais j'aiguisai tout de même le couteau, qui retrouva son tranchant de jadis.".

Instead of rubbing the sharpened couteau, say "Il était déjà affûté.".

Section 7 - Mortier

The mortier is a container on the plan de travail.
Understand "pilon" as the mortier.
The description is "Un mortier et un pilon qui permettaient d[']écraser des choses.".

Instead of inserting something which is not the pétales into the mortier, say "Je ne pouvais pas piler ceci.".

Instead of squeezing the pétales when the pétales are in the mortier:
	if the eau is in the mortier
	begin;
		[SFX piler]
		remove the eau from play;
		remove the pétales from play;
		now the mixture is in the mortier;
		say "J[']écrasai les pétales, jusqu[']à l'obtention d'une sorte de pâte.";
	else;
		say "Je ne pensais pas qu[']écraser les pétales seuls aurait été utile.";
	end if.

Section 8 - Mixture

The mixture is a female thing.
Understand "preparation" or "pate" as the mixture.
The description is "J'avais préparé cette mixture en écrasant les pétales avec de l'eau. Il ne me restait plus qu[']à l'utiliser.".

Instead of taking the mixture, say "Il valait mieux la laisser dans le mortier.".

Instead of eating the mixture, say "J'en pris un peu avec mon index et en goûtai. Je ne savais pas si c'était toxique, mais ce n'était pas mauvais.".

Instead of tasting the mixture, try eating the mixture.

Instead of smelling the mixture, say "Elle sentait bon, à l'image des fleurs d'où elle provenait.".

[DESSINER]
Instead of drawing on the player when the mixture is touchable and the journal is read and the dessins are examined:
	if the player is drawn
	begin;
		say "J'avais déjà dessiné le symbole sur moi-même.";
	else;
		now the player is drawn;
		now the monster time is 30 minutes after the monster time;
		say "Je dessinai le symbole ésotérique sur mon front. Cela serait-il suffisant ?";
	end if.

Instead of drawing on the frère when the mixture is touchable and the journal is read and the dessins are examined:
	if the frère is drawn
	begin;
		say "J'avais déjà dessiné le symbole sur mon frère.";
	else;
		now the frère is drawn;
		now the monster time is 30 minutes after the monster time;
		say "Je dessinai le symbole ésotérique sur son front. Cela serait-il suffisant ?";
	end if.

Chapter 5 - Cave

The cave is down from the cuisine. "Un sombre et humide endroit avait été aménagé sous la cuisine. Ici, les murs étaient de pierre nue, et le sol aussi. Autrement, rien d'autre que le bruit de nos pas qui résonnait.".
The printed name is "Sous terre".
The cave is dark.
The sound of the cave is "Le bruit de nos pas qui se réverbérait sur les murs de la cave lorsque nous marchions. Lorsque nous nous arrêtions, le silence.".

Instead of going to the cave when the armoire is not pushed, say "Je ne pouvais aller dans cette direction.".

Instead of exiting when the location is cave, try going up.
Instead of going outside when the location is cave, try going up.

Section 1 - Murs

Some murs are scenery in the cave.
Understand "mur" as the murs.
The description is "Ces murs se refermaient sur nous, nous étouffaient. Ils voulaient que la cave soit notre tombeau.".

Instead of touching the murs, say "Des pierres d'une humidité glaciale.".

Instead of attacking the murs for the first time:
	now the pierre du mur is in the cave;
	say "Je cognais ces pierres qui nous voulaient du mal. L'une d'elles sonna creux. Étrange…[line break]";
	set pronouns from the pierre du mur;
	spend 2 minutes.

Section 2 - Pierre

The pierre du mur is female thing. "Une pierre sonnait creux.".
The description is "Cette pierre [if handled]s[']était descellée du mur de la cave[else]sonnait creux[end if].".

Instead of pulling the pierre du mur, try taking the pierre.

After taking the pierre du mur:
	say "La pierre se descella sans difficulté. Avec appréhension, je mis ma main dans la cavité qu'elle avait libérée ; elle rencontra quelque chose, que je pris et examinai. Il s'agissait d’un curieux cylindre.";
	discover the cylindre;
	set pronouns from the cylindre.

Section 3 - Cylindre

The cylindre is a closed unopenable container.
The description is "Cet étrange objet absorbait toute la lumière qu'on lui donnait. Il en devenait presque invisible dans la pénombre de la demeure. De plus, il était bien plus lourd que sa taille ne le laissait présager.[line break][italic type][bracket]Cet objet est le début d'une énigme optionnelle qui n'a pas été implantée. Ne perds pas ton temps dessus ![close bracket][roman type]".
The cylindre can be revealed. The cylindre is not revealed.

Section 4 - Montre de gousset

The montre de gousset is a female thing in the cylindre.
Understand "montre a gousset" as the montre.
The description is "J'ouvris la montre et l'examinai. Étrangement, elle possédait cinq aiguilles : les trois premières étaient celles que l'on retrouvait sur toutes les horloges [--] pour les heures, les minutes et les secondes [--], elles indiquaient [24h time of day + 1 minute in words] ; les deux autres étaient rouge sang et leur pointe en forme de tête de mort montraient [24h monster time in words]. Que cela pouvait-il bien signifier ?".

Instead of opening the montre, try examining the montre.

Chapter 6 - Salle de bain

The salle de bain is east from the hall. "Une salle de bain complètement fêlée et sale. Pourtant, sous toute cette crasse, on devinait une baignoire ou un lavabo de luxe. Le propriétaire de cette demeure avait dû être aisé. Cependant, cette richesse ne l'avait pas sauvé, et ne nous sauverait pas non plus ; il fallait faire vite.".
The printed name is "Dans la salle de bain".
The salle de bain is dark.

Instead of exiting when the location is salle de bain, try going west.
Instead of going outside when the location is salle de bain, try going west.

Section 1 - Baignoire

The baignoire is a female scenery enterable container in the salle de bain.
The description is "Cette baignoire était très abîmée, mais on devinait tout de même sa valeur.".

Section 2 - Serviette

The serviette is a female thing in the salle de bain.
The description is "Cette serviette paraissait plus propre que la plupart des autres choses de cette maison[if the serviette is part of the player]. Je m'en étais servi comme bandage[end if].".

Instead of tying the serviette to the player during bleeding:
	now the serviette is part of the player;
	say "Je nouai la serviette autour de ma main et arrêtai l'hémorragie.".

Instead of tying the serviette which is part of the player to the player:
	say "Je l'avais déjà nouée autour de ma main.".

Instead of taking the serviette when the serviette is part of the player, say "Il valait mieux la laisser là où elle était.".

Section 3 - Pharmacie

The pharmacie is a closed openable fixed in place female container in the salle de bain.
The description is "Un petit meuble de rangement destiné aux médicaments était accroché au-dessus du lavabo. Il n'avait rien de spécial.".

Carry out opening the pharmacie:
	play the sound pharmacy in foreground.

Carry out closing the pharmacie:
	play the sound pharmacy in foreground.

Section 4 - Pot

The pot is a thing in the pharmacie.
Understand "medicament" as the pot.
The description is "Un vieux pot que j'avais découvert dans la pharmacie. Le médicament à l'intérieur, une pâte durcie par le temps, n[']était pas engageant et dégageait une forte odeur. Je préférais tenir cela loin de moi.".

Instead of searching the pot when the clef rouillée is off-stage and the clef rouillée is not immersed by the puits for the first time:
	say "Bravant mon dégoût, je mis la main dans le pot. Étrangement, je rencontrai un objet dur. Je le pris et l'examinai : une énorme clef rouillée. Celui qui l'avait mise là ne voulait pas qu'on la trouve…[line break]";
	discover the clef rouillée.

Instead of eating the pot, say "Je ne savais pas s'il s'agissait de pommade ou si le médicament était destiné à l'ingestion. Dans le doute, je m'abstins.".
Instead of tasting the pot, try eating the pot.

Instead of smelling the pot, say "Le pot dégageait une forte odeur de médicament.".

Instead of attacking the pot:
	play the sound pot in foreground;
	remove the pot from play;
	say "Je jetais le pot à terre. Il se brisa en mille morceaux[if the clef rouillée is off-stage and the clef rouillée is not immersed by the puits], révélant une grosse clef. Je me demandais quel homme sensé l'aurait mise là.";
	if the clef rouillée is off-stage and the clef rouillée is not wet, now the clef rouillée is in the location.

Section 5 - Clef rouillée

The clef rouillée is a female thing.
The clef rouillée unlocks the portail.
Understand "cle" or "clef/cle rouillee" as the clef rouillée.
The description is "Une vieille clef volumineuse trouvée dans un pot de médicament. Elle en gardait des traces : l'odeur, par exemple.".

Instead of smelling the clef rouillée, say "Il avait gardé l'odeur du médicament dans lequel il avait été conservé pendant si longtemps [--] sûrement des années.".

Part 2 - Extérieur

Exterior is a region.
The jardin, jardin-o, jardin-e and forêt are in exterior.

Carry out going to a room in exterior:
	play the sound nature steps in foreground;

Chapter 1 - Jardin

The jardin is south from the hall. The jardin is outside from the hall. "La lune nous observait depuis un ciel sans étoiles. Sous son triste regard, les ombres paraissaient encore plus inquiétantes. Les plantes semblaient mortes depuis la nuit des temps, excepté des épineux, qui nous tendaient leurs branches accusatrices. Elles voulaient sûrement que nous quittassions leur domaine.[line break]Le jardin s[']étendait plus à l'ouest et à l'est, et la forêt se dressait, menaçante, par-delà le portail au sud, mais mon frère me pressait de rentrer."
The printed name is "Dans le jardin".

Instead of ingoing when the location is jardin, try going north.

Section 1 - Puits

The puits is thing in the jardin.
The description of the puits is "Un puits de pierre délabré se dressait à mi-chemin entre le portail et le bâtiment. Il était profond : impossible de distinguer quoi que ce soit dans les ténèbres en contrebas.".

[IMMERSING RELATION]
Immersing relates various thing to a thing.
The verb to immerse (he immerses, they immerse, he immersed, it is immersed, he is immersing) implies the reversed immersing relation.


[JETER QQC DANS PUITS]
Instead of inserting something (called item) into the puits:
	now the item is immersed by the puits;
	now the item is wet;
	remove the item from play;
	play the sound splashing in foreground;
	say "Je jetai [the item] dans le puits, et [if the item is male]il[else]elle[end if] plongea dans une grande éclaboussure qui déchira le silence.".

Instead of throwing something (called item) at the puits, try inserting the item into the puits.

Instead of inserting the eau into the puits, say "Il aurait été inutile de rendre l'eau au puits.".

[JETER MARMITE DANS PUITS]
Instead of inserting the marmite into the puits when the marmite is part of the corde:
	play the sound splashing in foreground;
	if the eau is off-stage, now the eau is in the marmite;
	say "Je jetai la marmite dans le puits et la fit descendre le plus possible. Enfin, je la remontai[if the eau was off-stage]. Je récupérai ainsi de l'eau[end if].";
	if the eau was off-stage, now the eau is in the marmite;
	if something is immersed by the puits
	begin;
		let item be a random thing immersed by the puits;
		say "À ma grande surprise, je retrouvai [the item], que j'avais fait tomber dans le puits.";
		discover the item;
		now the item is not immersed by the puits;
	end if.

Before throwing the marmite at the puits:
	if the marmite is part of the corde
	begin;
		try inserting the marmite into the puits;
		stop the action;
	end if.

Section 2 - Eau

The eau is a female thing.
The indefinite article is "de l[']".
The description is "De l'eau que j'avais remontée du puits.".

Instead of taking the eau, say "Il valait mieux la laisser dans [the holder of the eau], ou elle se serait répandue par terre.".

Instead of drinking the eau, say "Je n'avais aucune idée quant à la salubrité de l'eau, mais j'avais soif. J'en bus donc un peu ; elle paraissait bonne. J'en donnai donc à mon frère.".

Instead of inserting the eau into the mortier:
	now the eau is in the mortier;
	say "Je versai l'eau dans le mortier.".

Chapter 2 - Jardin ouest

The jardin-o is west from the jardin. "Nous étions maintenant dans le recoin le plus sombre du jardin. Ici, la lune était invisible, cachée derrière le bâtiment, et les plantes encore plus touffues. Revenir à l'est était sûrement plus judicieux que d'entrer dans cette remise délabrée.".
The printed name is "Au fond du jardin".

Section 1 - Remise

The remise-sc is a female scenery in the jardin-o.
The printed name is "remise".
Understand "remise", "construction", "batiment" or "batisse" as the remise-sc.
The description is "Une construction délabrée, envahie par la végétation. Elle n'était pas rassurante, et je préférais ne pas m'y aventurer tant que cela n'aurait pas été nécessaire.".

Instead of entering the remise-sc, try ingoing.

Section 2 - Caisse

The caisse is a female enterable pushable between rooms supporter in the jardin-o.
The description is "Une grosse caisse en bois pourri trônait ici.".

[SFX pousser caisse]

Instead of taking the caisse, say "Elle était bien trop lourde.".
Instead of pushing the caisse, say "Je devais plutôt la pousser vers une direction.".

Section 3 - Sorbier

The sorbier is a fixed in place thing in the jardin-o.
The printed name is "arbuste".
Understand "arbuste" as the sorbier.
The description is "[one of]Cette plante, je connaissais son nom… C[']était un… sorbier. Oui, il s'agissait bien d'un sorbier. Pourtant, j[']étais certaine qu'il y avait quelque chose dont j[']étais censée me souvenir[or]Un sorbier se tenait là, paraissant protéger le bâtiment de ses branches[stopping].".

Section 4 - Branches

Some branches are part of the sorbier.
Understand "branche" as the branches.
The description is "[if the branches are part of the sorbier]Les branches de cet arbre étaient magnifiques[else if burnt]Les branches produisait un étrange feu qui rassurait[else]J'avais cassé ces branches, car je savais qu'elles pouvaient servir à quelque chose, bien que je ne me rappelasse plus quoi[end if].".
The branches can be burnt. The branches are not burnt.

Carry out examining the sorbier for the first time:
	now the printed name of the sorbier is "sorbier".

Instead of taking the branches when the branches are part of the sorbier:
	now the branches are in the location;
	say "Je cassai les branches du sorbier.";
	discover the branches.

Instead of taking the burnt branches, say "Elles brûlaient doucement, mais ce n'était pas une raison de tenter de les prendre.".

Instead of burning the branches when the player carries a switched on match:
	if the branches are in the cheminée
	begin;
		say "Je mis le feu aux branches, et un petit feu s[']éleva. Rien de grandiose, mais on faisait avec ce que l'on pouvait.";
		now the branches are burnt;
		now the ambiance of the salon is sound hearth;
		play the ambiance of the location in background with loop;
		now the monster time is 25 minutes after the monster time;
	else if the location is in exterior;
		say "Une idée folle me vint soudain. Je mis le feu aux branches, que je lâchai. Le feu se répandit rapidement, brûlant ces ronces qui nous sommaient de partir. Je souris à les voir ainsi mourir.";
		wait for any key;
		say "Mon frère me regardait, mon rictus et moi, avec des yeux terrorisés. Le voyant ainsi, je repris mes esprits ; étais-je devenue folle ?";
		wait for any key;
		say "Malheureusement, les flammes ne se contentèrent pas des plantes.";
		wait for any key;
		end the story saying "C[']était la fin…";
	else if the location is in interior;
		say "J'allumai une branche, qui m[']échappa des mains. Rapidement, toute la demeure prit feu. Était-ce ainsi que tout cela devait finir ?";
		wait for any key;
		say "Je regardai une dernière fois mon frère, qui me rendit le regard, avant que les flammes nous engloutissent…[line break]";
		wait for any key;
		end the game saying "C[']était la fin…";
	end if.

Instead of switching on the branches, try burning the branches.

Section 5 - Plantes

Some plantes are scenery in the jardin-o.
The description is "Des plantes touffues nous empêchaient d'aller plus loin.".

Instead of cutting the plantes when the player carries the couteau:
	say "Toutes ces plantes voulaient nous engloutir, j'en étais certaine. Je pris le couteau et commençai à les couper comme une démente. Heureusement, mon frère me fit comprendre que ça ne servait à rien, et j'arrêtai.";
	spend 5 minutes.

Instead of pulling the plantes:
	say "Toutes ces plantes voulaient nous engloutir, j'en étais certaine. Je commençai à les arracher comme une forcenée. Heureusement, mon frère me fit comprendre que ça ne servait à rien, et j'arrêtai.";
	spend 5 minutes.

Chapter 3 - Remise

The remise is inside from the jardin-o. "Dans la pénombre, on pouvait distinguer de nombreux outils, entreposés un peu partout. Ils étaient réellement effrayants, on aurait pu se croire dans une salle de torture.".
The printed name is "Dans la remise".
The remise is dark.

Section 1 - Pièges

Some pièges are in the remise. "Des pièges à loups étaient accrochés en hauteur sur le mur."
The printed name of the pièges is "pièges à loups".
Understand "pieges", "piege" or "pieges/piege a loups/loup" as the pièges.
The description is "D'effrayantes mâchoires métalliques qui ne demandaient qu[']à être nourries, j'en étais certaine.".

[PRENDRE LES PIÈGES]
Instead of taking the pièges when the pièges are not handled and the player is not on the caisse:
	say "Je tendis la main pour essayer d'attraper un piège. Il était trop haut. Déterminée, je sautai alors et réussit à toucher l'objet métallique. Je retombai au sol, mais ne fut pas la seule : je n'eus que le temps de voir la mâchoire se refermer…";
	wait for any key;
	play the sound jaws in foreground;
	wait for any key;
	end the game saying "C[']était la fin…".

After taking the not handled pièges, say "Grâce à la caisse, je pus prendre les pièges sans risques.".

[POSER LES PIÈGES DANS LA FORÊT]
Instead of dropping the pièges in the forêt:
	remove the pièges from play;
	say "Malgré ma peur, j'indiquai à mon frère de rester à l'entrée du jardin et m'enfonçai dans la forêt, posant les pièges à loups devant le domaine. Je revins bientôt, espérant qu'ils allaient [italic type]le[roman type] repousser.";
	if the pièges are sanctified
	begin;
		now the monster time is 35 minutes after the monster time;
	else;
		now the monster time is 25 minutes after the monster time;
	end if;
	spend 20 minutes.

Section 2 - Outils

Some outils are scenery in the remise.
The description is "De nombreux outils de jardinage étaient entreposés ici. Cependant, ils étaient tous en mauvais état, ou cassés.".

Instead of taking the outils, say "Ils nous auraient été inutiles.".

Instead of searching the outils:
	say "Malgré tout, je regardai si rien ne pouvait nous aider. Je perdis mon temps.";
	spend 4 minutes.

Chapter 4 - Jardin est

The jardin-e is east from the jardin. "Cette partie du jardin était moins sauvage et les plantes avaient consenti à nous laisser passer. Ici, une petite chapelle de pierre se dressait, et sa seule présence parvenait à nous apaiser, ainsi que les épineux, qui n'osaient pas s'approcher d'un lieu si sacré. Ça aurait été une bonne idée d'entrer dans le bâtiment, ou de retourner sur nos pas à l'ouest.".
The printed name is "Devant la chapelle".

Section 1 - Chapelle

The chapelle-sc is a scenery in the jardin-e.
Understand "chapelle", "batisse" or "batiment" as the chapelle-sc.
The printed name is "chapelle".
The description is "Une petite bâtisse de pierre, qui dégageait une aura de sûreté. Elle ne semblait pas du tout sinistre malgré les fissures qui la parcouraient.".

Instead of entering the chapelle-sc, try ingoing.

Section 2 - Fleurs

Some fleurs are a thing in the jardin-e. "De petites fleurs blanches poussaient dans un coin.".
Understand "fleur", "plante" or "plantes" as the fleurs.
The description is "[if handled]J'avais cueilli ces fleurs blanches dans le jardin. Elles n'avaient pas semblé être là par hasard, et je m[']étais dit qu'elles pouvaient peut-être servir à quelque chose[else]Ces fleurs délicates ressemblaient à des intrus acculés par les autres plantes alentour. Elles dégageaient quelque chose de surnaturel[end if].".

After taking the fleurs for the first time, say "Je cueillis les fleurs doucement, afin de ne pas les abîmer.".

Instead of pulling the fleurs, try taking the fleurs.

After burning the fleurs, say "Leur pureté m'empêchait de leur faire du mal.".

Section 3 - Pétales

Some pétales are part of the fleurs.
Understand "petales" or "petale" as the pétales.
The description is "De beaux pétales immaculés. Ils semblaient rayonner.".

Instead of taking the pétales when the pétales are part of the fleurs:
	say "Je détachai délicatement les pétales de chacune des fleurs.";
	now the player carries the pétales;
	remove the fleurs from play.

Instead of pulling the pétales when the pétales are part of the fleurs, try taking the pétales.

Section 4 - Pelle

The pelle is a female thing in the jardin-e.
The description is "Une vieille pelle usée, mais encore utilisable. Avait-elle servi à jardiner, ou à creuser des tombes ?".

Chapter 5 - Chapelle

The chapelle is inside from the jardin-e. "Malgré la nuit au-dehors, l'intérieur de ce lieu sacré rayonnait. Ici, le silence n[']était pas pesant, il régnait au contraire un calme apaisant. C[']était la première fois depuis longtemps que je n'avais pas ressenti pareille sensation.".
The printed name is "Dans la chapelle".
The sound is "Le silence régnait ici, mais un silence apaisant, religieux.".

Section 1 - Autel

The autel is a supporter in the chapelle.
The description is "Un autel sobre en pierre. En regardant de plus près, on pouvait voir quelques inscriptions gravées : H[unicode 298]C R[unicode 274]M P[unicode 332]NIS, SANCTA ERIT. Que pouvait bien signifier cette phrase ?".

Carry out putting something (called S) on the autel:
	now S is sanctified.

Section 2 - Crucifix

The crucifix is a thing in the chapelle.
Understand "croix" as the crucifix.
The description is "[if screwed]Un crucifix était vissé au-dessus de l'autel[else]Une belle croix de bois sombre. La garder ne pouvait pas faire de mal[end if].".
The crucifix can be screwed. The crucifix is screwed.

Instead of taking the screwed crucifix, say "Impossible, il était solidement fixé au mur.".

Instead of turning the screwed crucifix when the tournevis is not carried:
	say "Je n'avais rien pour le dévisser.".

Carry out turning the screwed crucifix:
	now the crucifix is not screwed;
	try silently taking the crucifix.

After turning the crucifix, say "Je dévissai le crucifix du mur, et le pris.".

Carry out taking the crucifix:
	now the monster time is 35 minutes after the monster time.

Carry out dropping the crucifix:
	now the monster time is 35 minutes before the monster time.

Carry out inserting the crucifix into something:
	now the monster time is 35 minutes before the monster time.

Carry out putting the crucifix on something:
	now the monster time is 35 minutes before the monster time.

After burning the crucifix, say "Je ne pouvais pas brûler un symbole sacré.".

Chapter 6 - Forêt

The forêt is south from the portail. "Nous voilà revenus à l'endroit que nous désirions le moins revoir. Tout était étrangement silencieux, comme si la forêt retenait sa respiration avant quelque danger. Je ne savais pas ce qui m'avait pris d'entraîner mon frère ici, mais il était trop tard pour les regrets. Il fallait retourner au nord."
The printed name is "Au cœur de la forêt".
The forêt can be bloody. The forêt is not bloody.

Instead of going nowhere from the forêt:
	say "Il n[']était pas sage d'aller plus loin et, de toute façon, nous n'en avions certainement pas le courage.".

Instead of going to the forêt when the arbres are burnt, say "L'incendie devenait violent. Mieux valait rester ici.".

Before doing something in the not bloody forêt during Bleeding:
	now the monster time is 30 minutes after the monster time;
	now the forêt is bloody;
	play the sound nature steps in foreground;
	say "Avant de continuer, une pensée me vint : peut-être que si je répandais du sang dans la forêt, cela [italic type]l[roman type][']occuperait ? Je pris mon courage à deux mains et m'enfonçait entre les arbres. Je m'enfonçais assez loin, puis je revins. Cela fonctionnerait-il ?";
	wait for any key;
	spend 7 minutes.

Section 1 - Arbres

Some arbres are scenery in the forêt.
Understand "arbre", "branches" or "branche" as the arbres.
The description is "[if burnt]Les arbres étaient en feu et un doux crépitement nous invitait à nous jeter dans l'incendie, pour en finir une fois pour toutes… Non, je devais me ressaisir ![else]Pas une feuille ne bruissait, il régnait un silence de mort. Les arbres semblaient former une prison nous empêchant de nous échapper. Mais cette cage ne [italic type]l[roman type][']empêcherait pas de venir ici.[end if]".
The arbres can be burnt. The arbres are not burnt.

Instead of taking the arbres, say "Les branches étaient bien trop hautes.".

Instead of dropping a switched on match when the location is forêt, try burning the arbres.

Instead of burning the not burnt arbres when the player carries a switched on match (called M):
	now the arbres are burnt;
	remove M from play;
	the player dies from fire in three minutes from now;
	now the monster time is 60 minutes after the monster time;
	now the sound of the forêt is "Un doux crépitement qui contrastait avec la violence de l'incendie, le rendant encore plus dangereux.";
	now the ambiance of the forêt is sound forest fire;
	play the ambiance of the location in background with loop;
	say "Dans l'espoir de [italic type]le[roman type] ralentir, je lâchai l'allumette enflammée. Le feu se propageait vite, il ne fallait pas rester ici.".

Instead of burning the burnt arbres when the player carries a switched on match (called M):
	say "Le feu dévorait déjà la forêt. Je n'avais pas envie de le nourrir davantage.".

At the time when the player dies from fire:
	if the location is forêt
	begin;
		say "La chaleur devenait insupportable et les flammes nous encerclaient, de plus en plus proches. Bientôt, elles nous atteignirent.";
		wait for any key;
		end the game saying "C[']était la fin…";
	end if.

Section 2 - Portail

The portail is south from the jardin. The portail is a open lockable door.
The description is "Un haut portail [if open]ouvert[else]fermé[end if] et rouillé se dressait, frontière entre la forêt et le jardin. [if the location is jardin]S'aventurer au-delà signifiait une mort certaine, j'en étais convaincue[else]Il était préférable de rentrer. Dans la forêt, tout pouvait arriver[end if].".

Carry out closing the portail:
	play the sound closing gate in foreground;
	if the location is not forêt, now the monster time is 5 minutes after the monster time.

Carry out opening the portail:
	play the sound opening gate in foreground;
	if the location is not forêt, now the monster time is 5 minutes before the monster time.

Carry out locking the portail with the clef rouillée:
	if the location is not forêt, now the monster time is 10 minutes after the monster time.

Carry out unlocking the portail with the clef rouillée:
	if the location is not forêt, now the monster time is 10 minutes before the monster time.

Part 3 - Premier étage

Chapter 1 - Chambre

The chambre is west from the couloir. "La lumière blafarde d'une lune gibbeuse filtrait par la vitre ébréchée, éclairant comme elle pouvait la pièce. Nous avions trouvé refuge ici, dans cette chambre, mais la peur, plus que le grabat, nous avait empêchés de dormir. Maintenant… Maintenant, que faire, justement ?[line break]La porte était à l'est.".
The printed name is "Dans une chambre".
Yourself is a woman in the chambre.

Instead of exiting when the location is chambre, try going east.
Instead of going outside when the location is chambre, try going east.

Section 1 - Nevena

Nevena is a woman in the chambre.
The description is "J'avais treize ans et, par ma faute, [italic type]il[roman type] nous poursuivait. Je devais trouver un moyen de nous sauver. Si nous ne pouvions pas nous en sortir tous les deux, je ferais en sorte qu'il puisse s'en tirer ; il n'avait que huit ans. Après tout, n[']était-ce pas le rôle d'une grande sœur ?".
The carrying capacity is 7.
Nevena can be drawn. Nevena is not drawn.
The player is Nevena.

Section 2 - Frère

The frère is a man in the chambre. "Mon frère se tenait là, à mes côtés.".
The indefinite article is "mon".
The printed name is "frère".
Understand "mon frère" or "frere" as the frère.
The description is "Mon frère était de cinq ans mon cadet, et si vulnérable. Je devais nous sortir de là[if the frère carries something]. [line break]Il portait [a list of thing carried by the frère][end if].".
The carrying capacity of the frère is 5.
The frère can be drawn. The frère is not drawn.

Carry out going to a room (called destination):
	now the frère is in the destination.

[PARLER À FRÈRE]
Instead of talking to the frère, say "J'avais une foule de paroles réconfortantes à lui dire, mais toutes auraient sonné creux. Non, il valait mieux se partager les choses découvertes[if the frère is carrying something] ou lui demander de me montrer ce qu'il portait[end if].".

Instead of answering the frère that, try talking to the frère.
Instead of telling the frère about, try talking to the frère.
Instead of asking the frère about, try talking to the frère.

[FRAPPER FRÈRE]
Instead of attacking the frère, say "Je ne pouvais lui faire de mal. Pas après tout ce qu'il avait enduré.".


[ORDONNER À FRÈRE]
Persuasion rule for asking the frère to try showing: persuasion succeeds.
Persuasion rule for asking the frère to try giving: persuasion succeeds.
Persuasion rule for asking the frère to try taking: persuasion succeeds.
The block giving rule is not listed in the check giving it to rulebook.

Section 3 - Grabat

The grabat is a enterable supporter in the chambre.
Understand "lit" or "couchette" as the grabat.
The description is "Un antique lit supportant un vestige de matelas. Nous nous étions crus en sécurité, blottis sous la couverture miteuse. Mais le serions-nous vraiment un jour ?".

Instead of entering or climbing the grabat, say "Inutile. Jamais nous n'aurions pu dormir.".

Instead of looking under the grabat:
	say "Je me penchai et inspectai le sol sous le lit. Il y avait peu de lumière, mais on comprenait qu'il n'y avait que de la poussière.".

Section 4 - Matelas

The matelas is a scenery on the grabat.
The description is "Il était si vieux et si mince qu'il était difficile de véritablement l'appeler matelas. Mais ce n'était pas cela qui nous avait empêchés de dormir.".

Instead of entering or climbing the matelas, try entering the grabat.

Instead of looking under the matelas, say "Je soulevai aisément le matelas, afin de voir si son ancien propriétaire y avait caché quelque chose dessous. Il n'y avait rien.".

Section 5 - Couverture

[COUVERTURE]
The couverture is a female wearable thing on the grabat. 
The description is "Une vieille couverture usée jusqu[']à la corde. Elle ne pouvait absolument plus servir, mais nous nous y étions blottis pour l'illusion de sûreté qu'elle apportait.".

Instead of cutting the couverture:
	play the sound tear cloth in foreground;
	say "Je déchirai la couverture, et me retrouvai avec de nombreux lambeaux de tissu.";
	now the lambeaux are in the holder of the couverture;
	remove the couverture  from play;
	spend 2 minutes.

[LAMBEAUX]
Some lambeaux are a thing.
Understand "lambeau" or "tissu" as the lambeaux.
The description of the lambeaux is "J'avais obtenu ces morceaux de tissu en déchirant la couverture. Était-ce une bonne idée ? Pourrais-je en tirer quelque chose ?".

Instead of tying the lambeaux to the lambeaux:
	say "Je nouai les morceaux de couverture entre eux, et m'en fit une sorte de corde. Elle n[']était pas solide, et j'espérais qu'elle n'allait pas céder quand je l'utiliserais.";
	now the corde is in the holder of the lambeaux;
	remove the lambeaux from play;
	spend 3 minutes.

[CORDE]
The corde is a female thing.
After printing the name of the corde when the marmite is part of the corde, say " à laquelle était attachée [the marmite]".
The description of the corde is "Des morceaux usés de couverture, noués entre eux et formant une corde précaire[if the marmite is part of the corde]. Une marmite y était attachée[end if].".

Section 6 - Table de nuit

The table de nuit is a female supporter in the chambre..
Understand "table" as the table de nuit.
The description is "Une petite table de nuit, à côté du lit. Elle n'avait pas l'air très solide. Elle comportait un petit tiroir.".

Instead of looking under the table de nuit, say "Il n'y avait rien ici.".

Section 7 - Tiroir

The tiroir de nuit is a closed openable container.
It is part of the table de nuit.
The printed name of the tiroir de nuit is "tiroir".
Understand "tiroir" as the tiroir de nuit.
The description is "Un petit tiroir pourri était encastré dans la table.".

Instead of pushing the tiroir de nuit, try closing the tiroir de nuit.
Instead of pulling the tiroir de nuit, try opening the tiroir de nuit.

Carry out opening the tiroir de nuit:
	play the sound draw in foreground.

Carry out closing the tiroir de nuit:
	play the sound draw in foreground.

Section 8 - Lampe


The lampe is a female device on the table de nuit.
The description is "Une lampe à gaz, que j'avais trouvée lorsque nous étions entrés dans la maison. Elle était encore en état de marche.".

[ALLUMER]
Before switching on the switched off lampe:
	unless the player carries a switched on match 
	begin;
		say "Je n'avais rien pour l'allumer.";
		stop the action;
	end unless.

Carry out switching on the lampe:
	now the lampe is lit;

After switching on the lampe:
	remove a random match which is carried by the player from play;
	say "J'allumai la lampe avec l'allumette et me débarrassai de cette dernière.".

[ÉTEINDRE]
Carry out switching off the lampe:
	now the lampe is not lit.

[SON]
Every turn:
	if the switched on lampe is visible, set the midground 1 volume to 2;
	else set the midground 1 volume to 0;
	
Section 9 - Boîte d'allumettes

The boîte d'allumettes is a female closed openable container in the tiroir de nuit.
Understand "boite" or "boite d'allumettes" as the boîte d'allumettes.
The description is "Une boîte d'allumettes à moitié écrasée. Je l'avais prise avant notre fuite dans la forêt.".
6 matches are in the boîte d'allumettes.

Instead of inserting something into the boîte d'allumettes when the noun is not a match, say "C[']était bien trop gros pour entrer.".

[
[BOÎTE]
The boîte d'allumettes is a female closed openable transparent container in the tiroir de nuit.
Understand "boite" or "boite d'allumettes" as the boîte d'allumettes.
The description is "Une boîte d'allumettes à moitié écrasée. [if the reste of the boîte d'allumettes > 0]Elle contenait [reste of the boîte d'allumettes in words] allumettes[else]Elle était vide[end if].".
The boîte d'allumettes has a number called reste. The reste of the boîte d'allumettes is 6.

Instead of opening the boîte d'allumettes:
	if the allumette is on-stage
	begin;
		say "J'avais déjà sorti une allumette. Elle devait être là, quelque part.";
	else if the reste of the boîte d'allumettes is 0;
		say "Il n'y avait plus d'allumettes.";
	else;
		decrease the reste of the boîte d'allumettes by 1;
		now the player carries the allumette;
		say "Je tirai une allumette de la boîte.";
		set pronouns from the allumette;
	end if.

[ALLUMETTE]
The allumette is a female device.
The description is "L'allumette était humide. Ça aurait été un miracle si elle s'allumait.".

Check switching on the allumette:
	if the boîte d'allumettes is not carried by the player, say "J'avais besoin de la boîte pour l'allumer." instead.

Carry out switching on the allumette:
	play the sound match in foreground;
	now the allumette is lit;
	the match burns in 3 minutes from now.

After switching on the allumette, say "L'allumette réussit à s'allumer, et une flamme rassurante apparut.".

Carry out switching off the allumette:
	now the allumette is not lit;
	now the allumette is switched off;
	remove the allumette from play.

After switching off the allumette:
	say "Je soufflai l'allumette et l'obscurité reprit sa place. Je me débarrassai alors du morceau de bois brûlé.".

Instead of dropping the switched on allumette when the location is not forêt, say "C[']était pure folie que de lâcher cette flamme ici.".

At the time when the match burns:
	if the allumette is on-stage
	begin;
		now the allumette is not lit;
		now the allumette is switched off;
		remove the allumette from play;
		say "La flamme mourut, faisant disparaître sa chaude lumière. Je me débarrassai alors du morceau de bois brûlé.";
	end if.
]

Chapter 2 - Couloir

The couloir is north from the escalier. "Un sombre couloir se déroulait devant nous. Les longues ombres des carreaux de la fenêtre formaient une grille sur le tapis poussiéreux. Par l'ouverture, deux yeux incandescents nous fixaient sévèrement. Par bonheur, ce n[']était pas [italic type]lui[roman type]. Du moins, je l'espérais…[line break]La chambre était à l'ouest, le hall au sud. Il y avait également une autre porte, mais je n[']était pas parvenue à l'ouvrir.".
The printed name is "Le long d'un sombre couloir".
The sound is "Un hululement insistant semblait vouloir nous avertir. Il valait mieux partir.".
The ambiance is the sound owl.

Section 1 - Tapis

The tapis is a scenery in the couloir.
The description is "Un tapis rongé par les insectes s[']étendait au sol. Il empêchait le parquet de grincer, mais j'aurais juré entendre ces bestioles grouiller à l'intérieur.".

Instead of looking under the tapis for the first time:
	say "Je m'accroupis et soulevais le lourd tapis comme je le pus. Je regardai dessous et…[line break]";
	wait for any key;
	say "Un énorme mille-pattes surgit d'un interstice du plancher. Je lâchai le pan du tapis et reculai prestement, tandis que mon frère se plaquait contre le mur. Une fois le myriapode parti, je repris prudemment mon inspection ; une clef était cachée là ! Je la pris et remis le tapis en place, ce qui souleva un nuage de poussière.";
	wait for any key;
	say "Une clef… Que pouvait-elle bien ouvrir ?";
	wait for any key;
	discover the clef dorée.

Instead of looking under the tapis:
	say "J'avais cherché ici, et je ne tenais pas à refaire une mauvaise rencontre.".

Instead of taking the tapis when the player's command includes "soulever", try looking under the tapis.

Section 2 - Clef dorée

The clef dorée is a female thing.
Understand "cle", "doree", "dore" and "petite" as the clef dorée.
The printed name is "petite clef dorée".
The description is "Cette petite clef dorée avait été cachée sous le tapis du couloir. [if the tiroir de travail has been unlocked]Elle ouvrait le tiroir du bureau, dans le cabinet de travail.[else]Que permettait-elle d'ouvrir ?[end if]".
The clef dorée unlocks the tiroir de travail.

Section 3 - Yeux

Some yeux are a scenery animal in the couloir.
The indefinite article of the yeux is "les".
Understand "oeil" or "hibou" as the yeux.
The description is "C[']était un hibou. C[']était… sûrement un hibou. C[']était… peut-être… un hibou ? Était-ce vraiment un hibou ? Je… je voulais partir d'ici au plus vite.".

Instead of talking to the yeux, say "« Ouuuuco, Ouuuuco… » Ce hibou avait une étrange manière de hululer. Je devais dérailler.".

Instead of doing something other than examining or talking to the yeux, say "Je ne tenais pas à l'embêter.".

Section 4 - Porte

The porte is a female undescribed lockable locked door.
The porte is east from the couloir.
The description is "Lorsque nous étions arrivés dans la demeure, j'avais essayé de trouver un endroit qui n[']était pas trop inquiétant pour passer la nuit. C'est à ce moment-là que j'avais su que cette porte était verrouillée."

Instead of opening the porte, say "Je tournai de nouveau la poignée, mais il n'y avait rien à faire.".

Instead of unlocking the door with something:
	if the second noun is the clef dorée or the second noun is the clef rouillée, say "J'essayai d'ouvrir la porte avec [the second noun], mais elle ne correspondait pas à la serrure.";
	else say "Utiliser une clef aurait été plus judicieux.".

Instead of attacking the porte, say "Je frappai la porte, et ne réussit qu[']à me faire mal à la main.".

Chapter 3 - Escalier

The escalier is up from the hall. "Nous étions à l[']étage, et seule une balustrade pourrie nous empêchait de chuter. Un tic-tac régulier se faisait entendre, mais je ne parvenais pas à distinguer l'horloge dans la pénombre. Il n'y avait pas grand-chose d'autre ici.[line break]Je pouvais descendre dans le hall, le couloir menant à la chambre était au nord et deux portes se dressaient de chaque côté du palier : à l'ouest[if the bibliothèque is visited], la bibliothèque,[end if] et à l'est[if the cabinet is visited], le cabinet de travail[end if].".
The printed name is "Au sommet de l'escalier".
The sound is "Le silence pesant était ponctué d'un tic-tac encore plus inquiétant.".
The ambiance is the sound tick.

Carry out going from the escalier to the hall:
	play the sound stairs in foreground.

Section 1 - Balustrade

The balustrade is scenery in the escalier.
The description is "Elle semblait fragile et je préférais ne pas avoir à éprouver sa solidité.".

Chapter 4 - Bibliothèque

The bibliothèque is west from the escalier. "La pièce était emplie de livres, trônant fièrement sur leurs étagères depuis ce qui semblait des lustres. Je me demandai si ces gardiens du savoir avaient survécu au temps et aux insectes. Il n'y avait qu'un moyen de le savoir, mais j'ignorais si cela nous aiderait.".
The printed name is "Entourée d[']étagères".

Instead of exiting when the location is bibliothèque, try going east.
Instead of going outside when the location is bibliothèque, try going east.

Section 1 - Étagères

Some étagères are a scenery in the bibliothèque.
Understand "etagere" or "etageres" as the étagères.
The description is "Des meubles anciens faits de bois massif nous dominaient. La pièce était vaste et ils étaient nombreux, jetant leur ombre dans les sombres recoins de la pièce. S'il y avait quelque chose à faire ici, il valait mieux que je le fisse rapidement.".

Instead of searching the étagères for the first time:
	say "Je pris un livre sur l[']étagère la plus à gauche, puis demandai à mon frère d'en faire autant sur celle de droite. Peut-être cette pièce renfermait une information capitale ? La longue fouille commença…[line break]";
	wait for any key;
	play the sound books in foreground;
	say "De longues minutes s[']écoulèrent. Soudain, un livre attira mon attention par son dos moins usé. Je me saisis de lui et lut le titre, qui était écrit en lettres d'or terni.";
	wait for any key;
	say "[italic type]Monstres, démons et autres créatures du folklore local[roman type]. Peut-être…[line break]";
	wait for any key;
	discover the volume;
	set pronouns from the volume;
	spend 20 minutes.

Instead of searching the étagères:
	say "Le livre que j'avais trouvé semblait contenir les informations dont j'avais besoin, et nous n'avions plus le temps de chercher quoi que se soit.".

Section 2 - Livres

Some livres are scenery in the bibliothèque.
Understand "livre" as the livres.
The description is "De nombreux livres trônaient sur les étagères, mais ne rendaient pas ces dernières moins inquiétantes pour autant.".

Instead of searching the livres, try searching the étagères.

Does the player mean doing something with the livres: it is very unlikely.

Section 3 - Volume

The volume is a thing.
Understand "livre" as the volume.
The printed name is "volume des [italic type]Monstres, démons et autres créatures du folklore local[roman type]".
The description is "Un livre qui, malgré sa vieillesse, avait su garder une étrange beauté. Sous le titre, une illustration représentait une silhouette noire portant deux yeux incandescents. Sur le dos, une mystérieuse figure cabalistique avait été gravée sur le cuir. Ce livre me donnait des frissons.".

Instead of reading the volume for the first time:
	play sound book in foreground;
	say "J'ouvris le livre et le feuilletai rapidement. Je cherchai une information quelconque, n'importe quoi qui pouvait [italic type]le[roman type] tenir à distance… Là, enfin quelque chose !";
	wait for any key;
	say "« [italic type]Il[roman type] arrive à minuit… Minuit… Survivre jusqu'au lever du Soleil… Les symboles sacrés [italic type]le[roman type] font fuir… Sacrés… »[line break]";
	wait for any key;
	say "Il n'y avait plus de temps à perdre ; si elles étaient vraies, ces informations nous seraient très utiles.";
	wait for any key;
	spend 10 minutes.

Instead of consulting the volume about a topic, try reading the volume.
Instead of searching the volume, try reading the volume.

Instead of reading the volume:
	say "Je n'avais plus le temps de le relire. Cependant, je me souvenais qu['][italic type]il[roman type] arriverait à minuit si rien n[']était fait, et que les symboles sacrés [italic type]le[roman type] faisaient fuir.".

Instead of examining the wet volume, say wet message.
Instead of reading the wet volume, say wet message.

Chapter 5 - Cabinet

The cabinet is east from the escalier. "Cette pièce devait le lieu de travail de l'ancien résident. Tout semblait si bien rangé, on aurait pu croire que quelqu'un l'avait utilisé la veille. Pourtant, la maison devait être vide depuis des lustres. C[']était comme si on avait su que l'on reviendrait à cet endroit.".
The printed name of the cabinet is "À l'intérieur du cabinet de travail".
The cabinet is dark.

Instead of exiting when the location is cabinet, try going west.
Instead of going outside when the location is cabinet, try going west.

Rule for printing the description of a dark room when the location is cabinet:
	say "Il faisait sombre ici, les fenêtres avaient été condamnées. Je ne pouvais pas distinguer grand-chose." instead.

Rule for printing the announcement of darkness when the location is cabinet:
	say "La lumière disparut, et l'obscurité revint.".

Section 1 - Bureau

The bureau is a supporter in the cabinet.
Understand "table" as the bureau.
The description is "Un beau meuble portant des traces de vernis. Il semblait fait d'un bois précieux qui avait résisté au temps. Il possédait un tiroir sur la gauche.".

Instead of looking under the bureau:
	say "Je jetai un coup d'œil sous le bureau. Rien hormis la poussière.";
	spend 2 minutes.

Section 2 - Tiroir de travail

The tiroir de travail is a closed openable locked lockable container.
It is part of the bureau.
The printed name is "tiroir".
Understand "tiroir" as the tiroir de travail.
The description is "Un beau tiroir, quoique quelque peu abîmé, se trouvait sur la gauche du bureau. Il avait une poignée ouvragée, bien que ternie, et une serrure.".

After unlocking the tiroir de travail with the clef dorée for the first time:
	say "La clef entra parfaitement dans la serrure, et je déverrouillai le tiroir.".

Instead of pushing the tiroir de travail, try closing the tiroir de travail.
Instead of pulling the tiroir de travail, try opening the tiroir de travail.

Carry out opening the tiroir de travail:
	play the sound draw in foreground.

Carry out closing the tiroir de travail:
	play the sound draw in foreground. 

Section 3 - Plan

The plan de la demeure is a privately-named thing on the bureau.
The printed name is "papier".
Understand "papier" as the plan de la demeure.
Understand "plan", "plan de la demeure", "demeure" or "carte" as the plan de la demeure when the plan de la demeure is examined.
The description of the plan de la demeure is "[one of]Je regardai ce vieux papier jauni. Il s'agissait d'un plan ![or]Ce plan représentait la demeure dans laquelle nous nous trouvions.[stopping][paragraph break][map]".

To say map:
	display figure map centered;

The plan de la demeure can be examined. It is not examined.

Carry out examining the plan de la demeure for the first time:
	now the plan de la demeure is examined;
	now the printed name of the plan de la demeure is "plan".

Instead of examining the wet plan de la demeure, say wet message.

Section 4 - Journal

The journal is a privately-named thing in the tiroir de travail.
The printed name is "[if examined]journal[else]cahier".
Understand "cahier" as the journal.
Understand "journal" as the journal when the journal is examined.
The description is "Un ouvrage de cuir abîmé, sans aucune indication sur sa couverture à propos de son contenu. Il semblait banal, mais devait être précieux aux yeux de son ancien propriétaire pour être ainsi entreposé dans un tiroir verrouillé à double tour.".
The journal can be read. The journal is not read.
The journal can be examined. The journal is not examined.

Instead of opening the journal, try reading the journal.

Instead of reading the journal:
	now the journal is examined;
	play sound book in foreground;
	say "Je feuillettai le cahier afin d'en voir brièvement le contenu. Il s'agissait d'un journal, pourtant il n'avait été tenu que quatre jours [--] à savoir les 30 et 31 octobre et le 1er et 2 novembre. L'année n'avait pas été écrite, mais le papier jauni semblait très ancien. Devais-je prendre le temps de le consulter ?".

Instead of consulting the journal about a topic, try reading the journal.

Instead of consulting the examined journal about a topic, say "Je devais le consulter à propos de l'une des dates où il avait été écrit.".

Instead of consulting the examined journal about a topic listed in the Table of Journal:
	play sound book in foreground;
	now the printed name of the journal is "journal";
	say "[italic type][description entry][roman type]";
	spend 3 minutes.

Table of Journal
Topic	Description
"30 octobre" or "trente octobre"	"[journal 1]"
"31 octobre" or "trente et un octobre" or "trente-et-un octobre"	"[journal 2]"
"1 novembre" or "1er novembre" or "premier novembre"	"[journal 3]"
"2 novembre" or "deux novembre"	"[journal 4]"

To say journal 1:
	say "30 octobre[paragraph break]C'est dans l'angoisse que j[']écris ces lignes. Mes jours sont comptés. Ce journal permettra aux générations suivantes d[']éviter le pire. En effet, mes recherches m'indiquent qu'une certaine espèce de plante poussant ici, dans la forêt alentour, aurait le pouvoir de [roman type]le[italic type] tenir à distance, pour une raison que j'ignore.[line break]Ce matin, je me suis donc enfoncé dans la forêt et, après de longues heures, j'ai trouvé cette fleur extrêmement rare. Je me suis coupé la main en la cueillant, mais ma joie a été telle que je n'y ai pas pris attention. J'ai eu une chance incroyable en la trouvant la première journée. Cependant, ma blessure saignait abondamment ; j'ai dû rentrer au plus vite, car le liquide rouge s[']épandait au sol. Une fois arrivé, j'ai pansé la plaie et planté le spécimen dans le jardin, pour qu'elle puisse servir dans le futur.[line break]Je me couche ce soir plein d'espoir, car demain je commencerai les premiers essais.";

To say journal 2:
	say "31 octobre[paragraph break]Tout est fini. Aujourd'hui, en compulsant mes livres et mes notes, j'ai découvert qu['][roman type]il[italic type] viendrait ce soir, en vérité. Alors que je suis si près du but ! Je me résigne à disparaître, mais je dois au moins transmettre mon savoir. C'est pourquoi se trouvent ci-dessous les étapes importantes qui permettent de préparer la fleur :[paragraph break]• Cueillir la fleur et en arracher les pétales tant qu'elle est fraîche.[line break]• Mettre les pétales dans un mortier avec un peu d'eau.[line break]• Piler le mélange jusqu[']à l'obtention d'une pâte uniforme. S'il n'y a pas assez d'eau, en rajouter ; attention toutefois, la mixture ne doit pas être liquide, mais consistante.[paragraph break]Je ne sais pas encore à quoi peut servir cette préparation, mais je suis certain d[']être sur la bonne voie, voie dont je ne verrai jamais le bout. Au lecteur qui lira ces lignes, j'espère que tu sauras trouver la solution.[paragraph break]Adieu.";

To say journal 3:
	say "[read journal]1er novembre[paragraph break]Je ne m'attendais pas à me réveiller ce matin, et pourtant je suis encore en vie. Pourquoi ? Peut-être a-t-[roman type]il[italic type] été retardé… Oui, mais par quoi ? J'ai beau fouiller dans ma mémoire, je ne vois pas ce qui [roman type]l[italic type][']a empêché de venir me chercher.[line break]Profitons de ce répit inespéré pour finir ce qui a été commencé. La préparation de la veille permettrait en théorie de [roman type]l[italic type][']éloigner, mais elle n'est pas assez puissante. L'idée m'est venue de combiner les étranges pouvoirs de cette plante avec un certain symbole ésotérique, qui protégeait des entités malfaisantes. J'ai déjà dessiné des ébauches de cette figure, mais je compare encore les différentes versions que j'ai pu trouver dans divers manuscrits afin de la reproduire dans les proportions idéales, ce qui lui permettra de maximiser son énergie. Je pense réussir demain, si le temps me le permet [--] ce que j'espère plus que tout.";

To say read journal:
	now the journal is read.

To say journal 4:
	say "2 novembre[paragraph break]Tout est prêt. Je viens de créer le symbole parfait. Je vais le dessiner sur mon front avec la préparation.[line break]Non ! [roman type]Le[italic type] voilà ! Je [roman type]l[italic type][']entends qui arrive ! Tout est perdu, la préparation est dans la cuisine ! Jamais je ne pourrais l'atteindre à temps ! Toi qui lis ces lignes, cherch[paragraph break][roman type]Le texte s'arrêtait ici. Les derniers mots avaient été écrits à la hâte, montrant toute l'angoisse qu'avait ressentie l'auteur de ces lignes. Si nous pouvions savoir ce qu'il avait voulu que nous cherchassions…[line break]":

Instead of reading the wet journal, say wet message.
Instead of consulting the wet journal about a topic, say wet message.

Part 4 - Incendie

Dropping a match is pyromaniac.
Inserting a match into something is pyromaniac.
Putting a match on something is pyromaniac.
Burning the armoire is pyromaniac.
Burning the bureau is pyromaniac.
Burning the grabat is pyromaniac.
Burning the horloge is pyromaniac.
Burning the marches is pyromaniac.
Burning the papier peint is pyromaniac.
Burning the boîte d'allumettes is pyromaniac.
Burning the caisse is pyromaniac.
Burning the corde is pyromaniac.
Burning the couverture is pyromaniac.
Burning the lambeaux is pyromaniac.
Burning the plantes is pyromaniac.
Burning the serviette is pyromaniac.
Burning the sorbier is pyromaniac.
Burning the table de nuit is pyromaniac.
Burning the tiroir de nuit is pyromaniac.
Burning the tiroir de travail is pyromaniac.
Burning the étagères is pyromaniac.
Burning the tapis is pyromaniac.
Burning the volume is pyromaniac.
Burning the journal is pyromaniac.

Instead of pyromaniac when the player carries a switched on match:
	if the location is in interior
	begin;
		say "Sans comprendre pourquoi, je démarrai un incendie, ici, dans la demeure. Mon frère me regarda avec des yeux horrifiés.";
		wait for any key;
		say "Nous restâmes ainsi, immobiles, alors que les flammes se propageaient. Quand j'eus retrouvé mes esprits, il était trop tard.";
		wait for any key;
		end the game saying "C[']était la fin…";
	else if the location is in exterior;
		say "Ce jardin devenu sauvage n'était pas normal ; toutes ses mauvaises herbes nous observaient dans notre dos de leur regard malfaisant. Je pris une allumette et la lança. Le jardin prit feu.";
		wait for any key;
		say "Mon frère ne comprenait pas, il me regardait, la peur au fond des yeux. Il ne pouvait pas comprendre. C[']était nécessaire, pourtant.";
		wait for any key;
		say "Les flammes nous entourèrent. Il ne restait que deux choses [--] deux personnes [--] à supprimer avant que le jardin ne soit de nouveau libre. Quand je me rendis compte de la triste réalité, il était trop tard. Avais-je perdu la raison ?";
		wait for any key;
		end the game saying "C[']était la fin…";
	else if the location is remise;
		say "Je mis le feu à la remise, détruisant ainsi cette salle de torture. L'incendie se propagea et nous fûmes bientôt pris au piège. C[']était ainsi que tout devait finir.";
		wait for any key;
		end the story saying "C[']était la fin…";
	else if the location is chapelle;
		say "Je ne pouvais pas mettre le feu dans ce lieu saint.";
	end if.

Volume 3 - Tests - not for release

[ÉCLAIRER PIÈCE]
Instead of jumping: now the location is lighted.

[DÉVERROUILLER]
Instead of squeezing a container (called C):
	if C is locked, now C is unlocked;
	else now C is locked.

[TEMPS]
When play begins:
	now the left hand status line is "[the player's surroundings] ([turn count])";
	now the right hand status line is "[24h time of day]-[24h monster time]".

Understand "monster time" as a mistake ("[24h monster time]")

Timing is an action out of world applying to one time.
Understand "[time]" as timing.
Report timing:
	say "[24h time understood in words]".

[GAGNER]
Instead of singing, end the story finally saying "Ce fut la plus belle chanson du monde".

Volume 4 - Releasing

Release along with cover art and the source text.

Book 1 - Bibliographic data

The story headline is "Une fiction interactive".
The story genre is "Horreur".
The release number is 1.
The story description is "Nous étions poursuivis, mon frère et moi. Par ma faute, nous courions au coeur de la forêt pour lui échapper. Je devais le protéger, il n'avait que huit ans. Il s'agissait de mon devoir de grande soeur. Mais où aller, quand tout se ressemblait ?
Soudain, mon frère me montra quelque chose, derrière les arbres : ''Regarde, Nevena, là-bas !'' Nous étions sauvés. Cette vieille bâtisse allait nous abriter jusqu'au matin. Jusqu'au matin... Vraiment ?".
The story creation year is 2013.

Book 2 - Index map

Index map with
	map-outline set to off, 
	font set to "Baskerville", [FONTS]
	title set to "Plan de la demeure", [TITRES]
	subtitle of the level 0 set to "Premier etage",
	subtitle of the level -1 set to "Rez-de-chaussee",
	subtitle of the level -2 set to "Cave",
	room-name-length set to 128,
	room-colour of interior set to "Khaki", [INTÉRIEUR]
	room-name of the hall set to "Hall", [HALL]
	room-size of hall set to 50,
	room-name of salon set to "Salon", [SALON]
	room-name of salle à manger set to "Salle a manger", [SALLE À MANGER]
	room-name of cuisine set to "Cuisine", [CUISINE]
	room-name of salle de bain set to "Salle de bain", [SALLE DE BAIN]
	room-name of escalier set to "Palier", [ESCALIER]
	room-name of bibliothèque set to "bibliotheque", [BIBLIOTHÈQUE]
	room-name of cabinet set to "Cabinet de travail", [CABINET DE TRAVAIL]
	room-name of couloir set to "Couloir", [COULOIR]
	room-name of chambre set to "Chambre", [CHAMBRE]
	room-name of cave set to "Cave", [CAVE]
	room-colour of exterior set to "Olive", [EXTÉRIEUR]
	room-name of jardin set to "Jardin", [JARDIN]
	room-name of jardin-o set to "Jardin", [JARDIN-O]
	room-name of jardin-e set to "Jardin", [JARDIN-E]
	room-name of remise set to "Remise", [REMISE]
	room-name of chapelle set to "Chapelle", [CHAPELLE]
	room-name of forêt set to "Foret". [FORÊT]

Book 3 - Commandes

Part 1 - Aide

[After printing the banner text, say "[first time]Tapez « infos » pour obtenir plus d'informations sur le jeu, si vous avez un problème concernant la lecture des sons ou si vous jouez à une fiction interactive pour la première fois.[only]".]

After printing the banner text, say "[first time]Tape « infos » pour plus d'informations, de l'aide, ou si tu rencontres des problèmes avec les sons.[only]".

[Understand "infos", "info", "informations" or "information" as a mistake ("Ce jeu est une fiction interactive ; il se joue en tapant des commandes à l'infinitif telles que « voir », « prendre [italic type]objet[roman type] », etc. (pour plus d'informations, consultez [italic type]www.ifiction.free.fr[roman type]).[paragraph break]Si vous obtenez le message [italic type]Your interpreter doesn't support sound reproduction[roman type], c'est que votre interpréteur ne peut pas jouer les sons. Essayez [italic type]Spatterlight[roman type] (Mac) ou [italic type]Gargoyle[roman type] (Windows).[paragraph break]Tapez « licence » pour l'obtenir et « auteur » pour en savoir plus sur l'auteur.[line break]Si vous êtes bloqué(e), tapez « indices » pour avoir des indications générales qui vous aideront à continuer.").]

Understand "infos", "info", "informations" or "information" as a mistake ("Si tu obtiens le message [italic type]Your interpreter doesn't support sound reproduction[roman type], c'est que ton interpréteur ne peut pas jouer les sons. Essaie [italic type]Spatterlight[roman type] (Mac) ou [italic type]Gargoyle[roman type] (Windows).[paragraph break]Tape « licence » pour l'avoir et si tu es bloqué(e), tape « indices » pour avoir des indications générales qui t'aideront à continuer.").

Part 2 - Licence

Understand "licence" or "license" as a mistake ("[bold type]Pour faire bref :[line break][roman type]Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la « GNU General Public License » telle que publiée par la Free Software Foundation : soit la version 3 de cette licence, soit toute version ultérieure.[paragraph break]Ce programme est distribué dans l'espoir qu'il vous sera utile, mais SANS AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ ni d'ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.[paragraph break][bold type]Pour faire long, [roman type]consultez : [italic type]http://www.gnu.org/licenses[roman type].[paragraph break]L'image de la couverture est un dessin d'Ylang, et est sous la licence CC-BY-NC-SA ([italic type]http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr[roman type]).[paragraph break]Les sons sont soit libres de droits, soit sous licence LESF ([italic type]http://www.sound-fishing.net/licences.html[roman type]), comme indiqué ci-dessous.[paragraph break][sounds licence]").


To say sounds licence:
	credit the Table of Free Sounds with "Sons libres de droits";
	say paragraph break;
	credit the Table of LESF Sounds with "Sons sous license LESF".

To credit the (T - a table name) with (title - a text):
	say "[bold type][title][roman type]";
	repeat through T:
		say "[line break]• [name entry]".

Table of Free Sounds
Name
"Chute de la balle"
"Coups de l'horloge"
"Fermeture du portail"
"Hibou"
"Ouverture du portail"
"Tic-tac de l'horloge"
"« Tu dors ? »"

Table of LESF Sounds
Name
"Affûtage du couteau"
"Allumette"
"Bris du pot"
"Chute de Nevena"
"Creusage"
"Déchirement de la couverture"
"Éclaboussure"
"Feu dans la cheminée"
"Feu dans la forêt"
"Grincement de l'armoire"
"Grincement de la pharmacie"
"Lampe à gaz"
"Loup"
"Ouverture du robinet"
"Page qui tourne"
"Pages qui tournent"
"Pas dans l'escalier"
"Pas à l'extérieur"
"Pas à l'intérieur 1"
"Pas à l'intérieur 2"
"Piège à loups qui se referme"
"Serrure"
"Tiroir"

Part 3 - Auteur

[Understand "credits", "credit", "auteur" or "auteurs" as a mistake ("C'est dans la peur que moi, Natrium729, écris ce texte, car [italic type]il[roman type] viendra bientôt, quand le moment sera venu. Ce jeu permettra aux gens de se préparer avant [italic type]son[roman type] arrivée. Cependant, je ne possède peut-être pas toutes les informations nécessaires, et si vous avez quelque chose à me communiquer avant qu'il ne soit trop tard, faites comme mes bêta-testeurs et contactez-moi par courriel à [italic type]natrium729@gmail.com[roman type] ou directement sur mon site, [italic type]http://ulukos.com[roman type].[paragraph break][italic type]Sa[roman type] seule image disponible, qui est en couverture, a été dessinée par Ylang avant qu['][italic type]il[roman type] ne disparaisse dans les ténèbres…[paragraph break]En espérant que mes recherches n'aient pas été vaines, adieu.").]

Part 4 - Indices

Chapter 1 - Indices généraux

[Understand "aide", "astuce", "astuces", "indice", "indices", "soluce" or "solution" as a mistake ("Voici quelques indices pour vous guider : [hints]").]

Understand "aide", "astuce", "astuces", "indice", "indices", "soluce" or "solution" as a mistake ("Voici quelques indices pour te guider : [hints]").

[To say hints:
	let L be {"Pour gagner, vous devez survivre jusqu[']à l'aube, le soleil se levant à 5 heures.", "Si vous ne faites rien, la chose qui vous poursuit arrivera à minuit.", "Pour retarder son arrivée, il vous faut accomplir certaines actions. Le plus simple serait par exemple de fermer le portail. Mais où est la clef ?", "Essayez d'explorer tous les recoins de la maison. Qui sait ce qui se cache sous ce vieux tapis ou ce que vous trouverez en fouillant cette étagère ?", "Si vous ne pouvez plus rien porter de plus, donnez certaines choses à votre frère. De plus, vous pouvez lui ordonner de prendre ou de vous montrer quelque chose, ou bien de vous le redonner.", "Il est dit qu'il existe un moyen de sanctifier les objets. Un objet béni aurait sûrement une efficacité accrue face à ce qui vous poursuit ?", "Si vous pensez avoir fait tout ce qu'il y avait à faire, vous pouvez attendre un certain temps (« attendre 15 minutes », par exemple) et voir si tout se passe bien…", "Si jamais ces indices n[']étaient pas suffisants, allez dans la chapelle et tapez « prier pour [italic type]quelque chose[roman type] » pour avoir des indications sur l'utilité de n'importe quelle objet dans le jeu."};
	repeat with hint running through L:
		say "[line break]• [hint]".]

To say hints:
	let L be {"Pour gagner, tu dois survivre jusqu[']à l'aube, le soleil se levant à 5 heures.", "Si tu ne fais rien, la chose qui te poursuit arrivera à minuit.", "Pour retarder son arrivée, il te faut accomplir certaines actions. Le plus simple serait par exemple de fermer le portail. Mais où est la clef ?", "Essaie d'explorer tous les recoins de la maison. Qui sait ce qui se cache sous ce vieux tapis ou ce que tu trouveras en fouillant cette étagère ?", "Si tu ne peux plus rien porter de plus, donne certaines choses à ton frère. De plus, tu peux lui ordonner de prendre ou de te montrer quelque chose, ou bien de te le redonner.", "Il est dit qu'il existe un moyen de sanctifier les objets. Un objet béni aurait sûrement une efficacité accrue face à ce qui te poursuit ?", "Si tu penses avoir fait tout ce qu'il y avait à faire, tu peux attendre un certain temps (« attendre 15 minutes », par exemple) et voir si tout se passe bien…", "Si jamais ces indices n[']étaient pas suffisants, va dans la chapelle et tape « prier pour [italic type]quelque chose[roman type] » pour avoir des indications sur l'utilité de n'importe quelle objet dans le jeu."};
	repeat with hint running through L:
		say "[line break]• [hint]".

Chapter 2 - Indices spécifiques

The allumette is a thing.
[Sert à prier pour l'allumette dans la chapelle : on ne pas prier pour celles qui sont en jeu car elles ne sont définies que par leur sorte (match)]

Does the player mean praying for the allumette: it is very likely.
Rule for clarifying the parser's choice of the allumette: stop.


Praying for is an action applying to one visible thing.
Understand "prier pour [any thing]" as praying for.


Table of Hints
Item	Hint
allumette	"J'allumai l'allumette et elle m[']éclairait. Je brûlais aussi quelque chose, mais je n'arrivais pas à distinguer quoi."
arbres	"J[']étais dans la forêt, un objet lumineux à la main. Soudain, les branches s'enflammèrent."
armoire	"L'armoire n[']était pas verrouillée et je pus ainsi l'ouvrir."
autel	"Il y avait une inscription en latin. J'avais appris le latin avant que nous fussions obligés de fuir, je devais être capable de la déchiffrer, même si toutes ces leçons semblaient si lointaines à présent."
baignoire	"Je voyais une personne, un homme. Il se baignait. C[']était il y a très longtemps ; la baignoire ne servait à rien aujourd'hui."
balle	"J'insérais une balle en argent dans quelque chose, mais tout était flou, je ne distinguais pas ce que c[']était."
boîte d'allumettes	"J'ouvrais la boîte d'allumettes afin d'en prendre une. Cependant, je voyais qu'il n'en restait pas beaucoup."
branches	"Des branches dans le foyer. Elles dégageaient une douce chaleur."
bureau	"Le bureau possédait un tiroir, mais il était verrouillé. Où était la clef ?"
caisse	"La caisse était lourde, mais je la poussais à l'intérieur."
chapelle-sc	"Je pénétrais dans la chapelle."
cheminée	"Nous étions assis, mon frère et moi, devant le feu. Nous n[']étions pas dans la demeure en ruine, nous étions dans la demeure familiale, longtemps auparavant. Tous ces souvenirs… Mais ceci était fini à présent."
clef dorée	"J'insérais la clef dans une serrure, mais je n'aurais su dire laquelle."
clef rouillée	"Étrangement, je voyais une clef dorée, étincelante."
corde	"La corde était attachée à ce qui semblait être une marmite."
couteau	"Un éclat dans la nuit ; une lame qui s'abattait ; un cri. Le cauchemar… Soudain, je me retrouvais dans une forêt, m'entaillant la main. Puis tout disparaissait."
couverture	"Je me voyais déchirer la couverture en morceaux."
crucifix	"Le calme m'envahissait soudain. Je portais le crucifix."
cuillère	"Une flamme, une goutte argentée. C[']était tout ce que je voyais dans ce noir d'encre."
dessins	"Une feuille emplie de dessins était dans ma main. Je la regardais, et les figures semblaient tournoyer. Elles restaient gravées dans ma mémoire."
eau	"Il y avait quelque chose dans l'eau. Puis j[']écrasais ce quelque chose. Il y avait trop d'eau, je le versais sur un objet étrange."
fenêtre	"Je regardais par la fenêtre. Mon frère était blotti contre moi. Nous étions dans la demeure familiale. Soudain, il y eut du mouvement dehors. Ils arrivaient. Nous étions maintenant dans une vieille bâtisse, et la fenêtre était ébréchée, rien de plus."
fleurs	"Je cueillais des fleurs d'un blanc pur."
frère	"J'avais mis mon frère en sécurité, à l[']écart, et je m[']étais cachée dans un coin sombre de la pièce. Il ne fallait pas qu'il visse cela. Il y eut des cris, un éclat blanc, un coup de tonnerre. Ce souvenir me hanterait à jamais. Nous fuyions maintenant à travers la forêt et nous nous réfugions dans un bâtiment en ruine. Je demandais à mon frère de porter les objets en trop. Comment pouvais-je lui donner des ordres après tout ce qu'il avait subi par ma faute ?"
fusil	"Le noir total ; un soudain éclair aveuglant ; un coup de tonnerre. Le cauchemar… Puis tout disparut, et je me voyais mettre un objet brillant dans une arme."
grabat	"Nous étions couchés là, dans le noir. Mais il arrivait, nous ne pouvions pas rester ici."
horloge	"Les aiguilles tournaient, tourbillonnaient. Elles s'arrêtèrent soudain en indiquant [24h monster time in words]. Puis treize coups résonnèrent."
journal	"Une main tremblante traçait des mots à la hâte. Mais elle n'eut pas le temps de terminer son message pour les générations futures. [italic type]Il[roman type] était là."
lambeaux	"La couverture n[']était plus et je nouai maintenant les lambeaux de tissu entre eux."
lampe	"Cette vieille lampe nous éclairait et ramenait la lumière sur les souvenirs de cet endroit."
lune	"La lune nous surveillait, mais nous ne savions pas si c[']était pour nous protéger ou non."
marches	"Nous dévalions l'escalier en toute hâte. Mon frère avait des larmes dans les yeux après ce qui venait d'arriver, mais je n'avais pas le temps de lui expliquer. Il fallait fuir le foyer qui nous avait vu grandir."
marmite	"Je jetais la marmite. Il y eut un bruit d[']éclaboussure, puis le silence revint."
mixture	"Je plongeai mon doigt dans une étrange mixture, puis je le portais à mon front, et à celui de mon frère."
mortier	"Il y avait de l'eau et une autre chose à l'intérieur du mortier. Je n'eus pas le temps de savoir quoi, je commençais à piler son contenu."
moule	"Une goutte argentine, de l'eau. J'ouvrais le moule."
outils	"Des outils gisaient, brisés. Je passais sans y prendre attention."
papier peint	"Il décorait la maison autrefois. Aujourd'hui, il attendait."
pelle	"Je creusais, creusais, et creusais encore."
pharmacie	"J'ouvrais la pharmacie."
pierre à affûter	"J'avais le couteau en main, et je l'aiguisais."
pièges	"Des pièges dans la forêt. Pour les loups, ou pour autre chose ?"
plan	"Nous nous étions perdus. Je tirais une feuille de ma poche, et nous situais sur le plan."
plantes	"Nous courions dans la forêt, et les ronces nous écorchaient les membres. Nous entrions dans le jardin, mais les ronces étaient toujours présentes."
portail	"Le portail grinçait alors que nous pénétrions dans le jardin. Dans notre terreur, nous oubliions de le fermer pour nous réfugier dans la demeure en ruine."
pot	"Un bruit de vaisselle cassée retentissait, et le pot se brisait par terre."
puits	"Il y avait de l'eau tout au fond du puits."
pétales	"J'arrachais ces beaux pétales de leur propriétaire. Puis j[']étais dans la cuisine, et les plaçais dans le mortier."
remise-sc	"J'entrais, malgré le sentiment de peur qui me nouait l'estomac."
robinet	"Je tournais le robinet en vain. Il s[']était tari depuis fort longtemps."
serviette	"Je prenais la serviette et la nouais. Elle se teintait de rouge."
sorbier	"Le sorbier était majestueux, mais je cassai ses branches."
table de nuit	"Je mettais la boîte d'allumettes dans le tiroir, puis me couchais avec mon frère sur le grabat. Quelques minutes plus tard, je me levais : [italic type]il[roman type] arrivait."
table à manger	"Beaucoup de monde s'affairait autour de la table à manger. Cependant, chaque personne disparaissait subitement, une à une. Il n'en resta plus, et tout devint silencieux."
tapis	"Un homme était accroupi par terre. Il soulevait un coin du tapis, y cachait quelque chose. Puis il disparut."
tournevis	"Je le trouvais dans le salon, et le prenais, étant sûre qu'il pouvait me servir."
ustensiles	"Ils avaient tous l'air en mauvais état, mais je les fouillais minutieusement."
volume	"Je feuilletais cet énorme livre, espérant trouver une information concrète."
yeux	"Une paire d'yeux incandescents nous surveillait. Était-il l'un de [italic type]ses[roman type] messagers ?"
étagères	"La bibliothèque était vaste, mais je la fouillais de fond en comble malgré tout."
Nevena	"Je me voyais serrant mon frère devant la fenêtre, alors qu'ils arrivaient. Je me voyais courant dans l'escalier, le tenant par la main et le forçant à me suivre. Je me voyais l'entraînant dans la forêt, alors qu'il était exténué. Je me voyais entrant avec lui dans la bâtisse, malgré son air peu rassurant. Je le voyais qui me suivait toujours et qui m'aimait, après tout ce que je lui avais fait endurer. Il fallait que je le sortisse de là, quoi qu'il en coûte."


Check praying for:
	if the location is not chapelle, say "Ce n[']était pas le bon endroit pour prier." instead.

[
Report hinting something:
	say "Nous nous agenouillâmes, fermâmes les yeux et nous mirent à prier. Il n'y avait plus beaucoup d'espoir…[line break]";
	if the noun is an Item listed in the Table of Hints
	begin;
		say "J'eus soudain une vision…[paragraph break]";
		wait for any key;
		say "[italic type][Hint entry][roman type][paragraph break]";
		wait for any key;
		say "La vision s'estompa. Encore secouée, je me relevai et tirai mon frère de sa prière. Il ne fallait pas s'attarder.";
		wait for any key;
	else;
		say "Nous restâmes ainsi quelques minutes, et nous nous relevâmes. Il ne fallait pas baisser les bras.";
	end if;
	spend 5 minutes.
]

Report praying for something:
	say "Nous nous agenouillâmes, fermâmes les yeux et nous mirent à prier. Il n'y avait plus beaucoup d'espoir…[line break]";
	if there is an item of the noun in the Table of Hints
	begin;
		wait for any key;
		say "J'eus soudain une vision…[paragraph break]";
		wait for any key;
		choose row with a item of the noun in the Table of Hints;
		say "[italic type][Hint entry][roman type][paragraph break]";
		wait for any key;
		say "La vision s'estompa. Encore secouée, je me relevai et tirai mon frère de sa prière. Il ne fallait pas s'attarder.";
		wait for any key;
	else;
		say "Nous restâmes ainsi quelques minutes, et nous nous relevâmes. Il ne fallait pas baisser les bras.";
	end if.

Part 5 - En connaître plus sur l'histoire

Table of French Final Question Options (continued)
final question wording	only if victorious	topic	final response rule	final response activity
"en savoir plus sur l'HISTOIRE"	true	"histoire"	know more about the story rule	--

[This is the know more about the story rule:
	say "Avez-vous prié dans la chapelle, comme indiqué dans les indices ? Si non, félicitations ! Cependant, sachez que certaines des visions reçues en priant contiennent des allusions au passé des protagonistes.";
	wait for any key;
	say "Avez-vous en outre réussi l[']énigme de la cave ? Si tel est le cas, <liste d'actions à faire sur la montre>. Lorsque la chose qui vous poursuit arrivera, le temps sera suspendu et vous pourrez continuer à explorer la demeure sans crainte de perdre, afin de découvrir tous ses secrets.[paragraph break]";
	wait for any key;
	say "Si tout cela a déjà été fait : merci d'avoir joué si longtemps ! J'espère que vous vous êtes bien amusé !";
	wait for any key;
	rule succeeds.]

This is the know more about the story rule:
	say "As-tu prié dans la chapelle, comme indiqué dans les indices ? Si non, félicitations ! Cependant, sache que certaines des visions reçues en priant contiennent des allusions au passé des protagonistes.";
	wait for any key;
	say "En outre, quand tu réussiras l[']énigme de la cave qui n'a pas encore été implémenté, tu pourras suspendre le temps lorsque la chose qui te poursuit arrivera, te permettant d'explorer la demeure sans crainte de perdre, afin de découvrir tous ses secrets.[paragraph break]";
	wait for any key;
	say "Si tout cela a déjà été fait : merci d'avoir joué si longtemps ! J'espère que tu t'es bien amusé !";
	wait for any key;
	rule succeeds.